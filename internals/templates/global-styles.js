import { createGlobalStyle } from 'styled-components';
import { fonts } from 'configs/styleVars';

const GlobalStyle = createGlobalStyle`
  html,
  body {
    height: 100%;
    width: 100%;
  }

  body {
    font-family: ${fonts.primaryFontFamily}, ${fonts.secondaryFontFamily};
  }

  body.fontLoaded {
    font-family: ${fonts.primaryFontFamily}, ${fonts.secondaryFontFamily};
  }

  #app {
    background-color: #fafafa;
    min-height: 100%;
    min-width: 100%;
  }

  p,
  label {
    font-family: ${fonts.primaryFontFamily}, ${fonts.secondaryFontFamily};
    line-height: 1.5em;
  }
`;

export default GlobalStyle;
