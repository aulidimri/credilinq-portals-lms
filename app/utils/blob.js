export const downloadBlob = (blob, sendingDataObject, fileType, fileName) => {
  if (blob.type == 'application/octet-stream') {
      blob = new Blob([blob], { type: 'application/pdf'});
  }
  console.log("DOWNLOADING...BLOB");

  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
    window.navigator.msSaveOrOpenBlob(newBlob);
    return;
  }

  const url = URL.createObjectURL(blob);
  //TODO: fix this hack
  var newtab = sendingDataObject.newTab;

  setTimeout(() => {
      newtab.location.href = url;
  }, 0);
};
