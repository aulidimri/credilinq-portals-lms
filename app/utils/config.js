import qs from 'query-string';

let obj = {};
if (process.env.NODE_ENV === 'development') {
    obj = {
        apiUrl: 'http://dev-los.getlend.in:8000/journey-0.0.1-SNAPSHOT',
        apiSubUrl: '/lending',
        apiPortalUrl: '/portal',
        apiCaseUrl: '/case'
    };
} else {
    obj = {
        apiUrl: 'http://uat-demo.getlend.in:8000/journey-0.0.1-SNAPSHOT',
        apiSubUrl: '/lending',
        apiPortalUrl: '/portal',
        apiCaseUrl: '/case'
    };
}


obj.apiPath = {
    login: () => `/auth/login`,
    products: (params) => `${obj.apiSubUrl}${obj.apiPortalUrl}/get-product-config/${params.roleName}`,
    assignedList: (params, queryParamString) => `${obj.apiSubUrl}${obj.apiPortalUrl}/assignedList/${params.roleName}/${params.groupId}/${params.productType}/${params.bucketKey}${queryParamString}`,
    tabConfig: (params, queryParamString) => `${obj.apiSubUrl}${obj.apiPortalUrl}/${params.caseInstanceId}/get-tab-config/${queryParamString}`,
    cardConfig: (params, queryParamString) => `${obj.apiSubUrl}${obj.apiPortalUrl}/${params.roleName}/${params.activeBucket}/${params.caseInstanceId}/${params.tabKey}/get-card-config${queryParamString}`,
    postComment: (params) => `${obj.apiSubUrl}${obj.apiPortalUrl}/${params.roleName}/${params.caseInstanceId}/${params.tabKey}/comment`,
    fetchCommentsByTab: (params) => `${obj.apiSubUrl}${obj.apiPortalUrl}/${params.caseInstanceId}/${params.tabKey}/comment`,
    fetchComments: (params) => `${obj.apiSubUrl}${obj.apiPortalUrl}/${params.caseInstanceId}/comment`,
    save: (params, queryParamString) => `${obj.apiSubUrl}${obj.apiCaseUrl}/submit-variables${queryParamString}`,
    configApi: (params, queryParamString) => `${params.subUrl}${queryParamString}`,
    download: (params, queryParamString) => `${params.subUrl}${queryParamString}`,
};

function encode(d, bool) {
    return bool ? encodeURIComponent(d) : d;
}
function parameterizeQuery(data, bool) {
   const ret = [];
   for (let d in data)
     ret.push(encode(d, bool) + '=' + encode(data[d], bool));
   return ret.join('&');
}

obj.getApiUrl = (apiType, params, queryParams, encode = true) => {
    let queryParamString = '';
    if (queryParams && Object.keys(queryParams).length) {
        //queryParamString = '?' + qs.stringify(queryParams);
        queryParamString = '?' + parameterizeQuery(queryParams, encode);

    }
    let apiPathUrl = obj.apiPath[apiType](params, queryParamString);
    if(apiType === "download") {
      console.log(apiPathUrl);
      return apiPathUrl;
    }
    return obj.apiUrl + apiPathUrl;
};

export const layoutConfig = {
  includeHeader: true,
  defaultColumnWidth: 6,
  // Each row is divided into 12 columns
  // In native the size is given in percentage
  // So each columns will have width of:
  defaultColumnSizeForMobile: (100 / 12),
  tableWidthUnit: 150,
};
export const config = obj;
