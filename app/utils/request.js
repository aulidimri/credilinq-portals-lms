import 'whatwg-fetch';
import config from 'utils/config';
import axios from 'axios';
import dottie from 'dottie';
import { downloadBlob } from 'utils/blob';
import { array } from 'prop-types';

/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON from the request
 */
function parseJSON(response) {
  return response.json();
}

function parseBlob(response) {
  const contentType = response.headers['content-type'];
  console.log("Content Type: ", contentType);
  let blob = response.data;
  console.log("Blob: ", blob);
  if (contentType.includes('application/vnd.ms-excel') || contentType.includes('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')) {
    //blob = new Blob([blob],{type: contentType});
    //console.log("Blob: ", blob);
  }

  response.data = blob;
  return response;
  //return { blob: new Blob([response.data], { type: "image/png" }), ...response };
  // new Blob([blob], { type: "application/pdf" })
}

let requestDataObject = {};

/**
 * Checks if a network request came back fine, and throws an error if not
 *
 * @param  {object} response   A response from a network request
 *
 * @return {object|undefined} Returns either the response, or throws an error
 */
function checkStatus(response) {
  console.log(response);
  if (!response) {
    console.log("Response is undefined!");
    return null;
  }

  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  const error = new Error(response.statusText);
  error.response = response;
  throw error;
}

function checkResponseType(response) {
  const contentType = response.headers['content-type'];

  //console.log(response);

  if (contentType) {
    if (contentType.includes('application/json')) {
      //return parseJSON(response);
      return response.data;
    } else if (
      contentType.includes('application/pdf') || contentType.includes('application/octet-stream') || contentType.includes('image/png') ||
      contentType.includes('application/vnd.ms-excel') || contentType.includes('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    ) {
      return parseBlob(response);
    }
    console.error(`Response from "${response.url}" has unexpected "content-type" ${contentType}`);
  } else {
    console.error(`Response from "${response.url}" has no "content-type"`);
  }
  return response;
}

function postParse(response, otherOptions) {

  // if (response.data instanceof ArrayBuffer) {
  //   let arrayBuffer = response.data;
  //   response.data = new Blob([arrayBuffer], {
  //     type: 'application/octet-stream'
  //   });
  // }

  // if (response.data instanceof Blob) {
  //   let blob = response.data;
  //   if (blob.type === 'application/octet-stream') {
  //     blob = new Blob([blob], { type: 'application/pdf' });
  //     response.data = blob;
  //   }
  // }
  return response;
}

export default function request(url, authorization, payload, sendingMethod, dataObject) {
  //console.log('url, authorization', payload);
  requestDataObject = dataObject;
  let options = {};
  // var data = new FormData();
  // data.append("json", JSON.stringify(payload));
  options = {
    headers: {
      'Content-Type': 'application/json',
    },
    credentials: 'same-origin',
  };


  if (payload) {
    let data;
    if (payload instanceof FormData || payload.requestBody instanceof FormData) {
      data = payload;
      options = {
        headers: {
        },
        credentials: 'same-origin',
      };
    } else {
      //console.log("DATA BEFORE STRINGIFY: ", payload);
      //data = JSON.stringify(payload);
      data = payload
      //console.log("DATA AFTER STRINGIFY: ", data);
      //console.log(typeof(data));
    }
    //options.body = data;
    options.data = data;
  }

  if (authorization) {
    const token = 'Bearer '.concat(authorization);
    options.headers.Authorization = token;
  }

  if (sendingMethod) {
    options.method = sendingMethod;
  }

  if (requestDataObject && requestDataObject.onUploadProgress) {
    options.onUploadProgress = requestDataObject.onUploadProgress;
  }

  if (requestDataObject && requestDataObject.onDownloadProgress) {
    options.onDownloadProgress = requestDataObject.onDownloadProgress;
  }

  if (requestDataObject && requestDataObject.responseType) {
    options.responseType = requestDataObject.responseType;
  }

  //console.log("URL: ", url, "OPTIONS: ", options);;
  return axios(url, options)
    .then(checkStatus)
    .then(checkResponseType)
    .then(postParse)
    .catch((error) => {
      console.error('API Error: ', error, dottie.get(error, 'response.data'));
      return dottie.get(error, 'response.data') || {};
    });
  // return fetch(url, options)
  //   .then(checkStatus)
  //   .then(checkResponseType)
  //   .then(postParse)
  //   .catch((error) => {
  //     //console.error('API Error: ', error);
  //     return { error };
  //   });


}

/**
 * Requests a URL, returning a promise
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 *
 * @return {object}           The response data
 */
function makeRequest(url, options) {
  return fetch(url, options)
    .then(checkStatus)
    .then(parseJSON);
}
