/* This is storage util file
contains:
  getters-setters in localStorage
*/

const localStorage = window && window.localStorage;
// Get an item from the localStorage
export const getlocalKey = (key) => {
    let value = localStorage && localStorage.getItem(key);
    value = JSON.parse(value);
    return value;
};

// Set any key with the value
export const setlocalKey = (key, value) => {
    value = JSON.stringify(value);
    localStorage && localStorage.setItem(key, value);
};

// Remove an item from the localStorage
export const deletelocalKey = key => {
    localStorage && localStorage.removeItem(key);
};
