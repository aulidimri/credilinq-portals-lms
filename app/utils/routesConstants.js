export const RESTART_ON_REMOUNT = '@@saga-injector/restart-on-remount';
export const DAEMON = '@@saga-injector/daemon';
export const ONCE_TILL_UNMOUNT = '@@saga-injector/once-till-unmount';
import _ from 'lodash';

const baseUrl = {
    listingPage: '/app/admin',
    products: '/adminProducts',
    organization: '/adminOrganizations',
    productMix: '/productsMix',
    charges: '/charges',
    floatingRate: '/floatingRates',
};

const ROUTES = {
    // login: () => { return { url: `/auth/login`, name: 'Login'}},
    // listingPage: () => {return {url: `${baseUrl.listingPage}`, name: 'Listing Page'}},

    adminOrganizations: () => { return { url: `${baseUrl.listingPage}${baseUrl.organization}`,name: 'Organization', categoryKey: 'admin', navigationKey: 'adminOrganizations'}},
    // orgMainPageCardsManageOffices: () => { return { url: `${baseUrl.listingPage}${baseUrl.organization}/manageOffers`, name: 'Manage Offices', categoryKey: 'admin', navigationKey: 'adminOrganizations'}},
    // orgMainPageCardsCurrencyConfiguration: () => { return { url: `${baseUrl.listingPage}${baseUrl.organization}/currencyConfiguration`, name: 'Currency Configuration', categoryKey: 'admin', navigationKey: 'adminOrganizations'}},
    orgMainPageCardsComponentTesting: () => { return { url: `${baseUrl.listingPage}${baseUrl.organization}/componentTesting`, name: 'Components Testing', categoryKey: 'admin', navigationKey: 'adminOrganizations'}},
    // orgMainPageCardsManageHolidays: () => { return { url: `${baseUrl.listingPage}${baseUrl.organization}/manageHolidays`, name: 'Manage Holidays', categoryKey: 'admin', navigationKey: 'adminOrganizations'}},
    // orgMainPageCardsManageFunds: () => { return { url: `${baseUrl.listingPage}${baseUrl.organization}/manageFunds`, name: 'Manage Funds', categoryKey: 'admin', navigationKey: 'adminOrganizations'}},
    // orgMainPageCardsManageEmployees: () => { return { url: `${baseUrl.listingPage}${baseUrl.organization}/manageEmployees`, name: 'Manage Employees', categoryKey: 'admin', navigationKey: 'adminOrganizations'}},
    // orgMainPageCardsBulkLoanReassignment: () => { return { url: `${baseUrl.listingPage}${baseUrl.organization}/bulkLoanReassignment`, name: 'Bulk Loan Reassignment', categoryKey: 'admin', navigationKey: 'adminOrganizations'}},
    // orgMainPageCardsStandingInstructionsHistory: () => { return { url: `${baseUrl.listingPage}${baseUrl.organization}/standingInstructionsHistory`, name: 'Standing Instructions History', categoryKey: 'admin', navigationKey: 'adminOrganizations'}},
    // orgMainPageCardsWorkingDays: () => { return { url: `${baseUrl.listingPage}${baseUrl.organization}/workingDays`, name: 'Working Days', categoryKey: 'admin', navigationKey: 'adminOrganizations'}},
    // orgMainPageCardsFundMapping: () => { return { url: `${baseUrl.listingPage}${baseUrl.organization}/fundMapping`, name: 'Fund Mapping', categoryKey: 'admin', navigationKey: 'adminOrganizations'}},
    // orgMainPageCardsPaymentType: () => { return { url: `${baseUrl.listingPage}${baseUrl.organization}/paymentType`, name: 'Payment Type', categoryKey: 'admin', navigationKey: 'adminOrganizations'}},
    // orgMainPageCardsPasswordPreferences: () => { return { url: `${baseUrl.listingPage}${baseUrl.organization}/passwordPreferences`, name: 'Password Preferences', categoryKey: 'admin', navigationKey: 'adminOrganizations'}},
    // orgMainPageCardsSMSCampaigns: () => { return { url: `${baseUrl.listingPage}${baseUrl.organization}/smsCampaigns`, name: 'SMS Campaigns', categoryKey: 'admin', navigationKey: 'adminOrganizations'}},
    // orgMainPageCardsProvisioningCriteria: () => { return { url: `${baseUrl.listingPage}${baseUrl.organization}/loanProvisioningCriteria`, name: 'Loan Provisioning Criteria', categoryKey: 'admin', navigationKey: 'adminOrganizations'}},
    // orgMainPageCardsAdHocQuery: () => { return { url: `${baseUrl.listingPage}${baseUrl.organization}/adHocQuery`, name: 'AdHocQuery', categoryKey: 'admin', navigationKey: 'adminOrganizations'}},
    // orgMainPageCardsEntityDataTableChecks: () => { return { url: `${baseUrl.listingPage}${baseUrl.organization}/entityDataTableChecks`, name: 'Entity Data Table Checks', categoryKey: 'admin', navigationKey: 'adminOrganizations'}},
    // orgMainPageCardsBulkimport: () => { return { url: `${baseUrl.listingPage}${baseUrl.organization}/bulkimport`, name: 'Bulk Import', categoryKey: 'admin', navigationKey: 'adminOrganizations'}},

    adminProducts: () => { return { url: `${baseUrl.listingPage}${baseUrl.products}`, name: 'Products', categoryKey:'admin', navigationKey: 'adminProducts'}},
    
    proLoanProducts: () => { return { url: `${baseUrl.listingPage}${baseUrl.products}/loanProducts`, name: `Loan Products`, categoryKey:'admin', navigationKey: 'adminProducts'}},
    // proLoanProductsAdd: (params) => { return { url: `${baseUrl.listingPage}${baseUrl.products}/loanProducts/add/${params && params.loanProductId && params.loanProductId.toString() || ''}`, name: `Create New Loan Product`, categoryKey:'admin', navigationKey: 'adminProducts'}},
    // proLoanProductsEdit: (params) => { return { url: `${baseUrl.listingPage}${baseUrl.products}/loanProducts/${params && params.loanProductId || ''}/edit`, name: `Create New Loan Product`, categoryKey:'admin', navigationKey: 'adminProducts'}},
    // proLoanProductsDetails: (params) => { return { url: `${baseUrl.listingPage}${baseUrl.products}/loanProducts/${params && params.productId || ''}`, name: `Create New Loan Product`, categoryKey:'admin', navigationKey: 'adminProducts'}},

    proCharges: () => { return { url: `${baseUrl.listingPage}${baseUrl.products}${baseUrl.charges}`, name: `Charges`, categoryKey:'admin', navigationKey: 'adminProducts'}},
    // proChargeDetails: (params) => { return { url: `${baseUrl.listingPage}${baseUrl.products}${baseUrl.charges}/${params && params.chargeId || ''}`, name: `Charges`, categoryKey:'admin', navigationKey: 'adminProducts'}},
    // proChargeDetailsEdit: (params) => { return { url: `${baseUrl.listingPage}${baseUrl.products}${baseUrl.charges}/${params && params.chargeId || ''}/edit`, name: `Edit Charges`, categoryKey:'admin', navigationKey: 'adminProducts'}},
    // proChargeAddNew: () => { return { url: `${baseUrl.listingPage}${baseUrl.products}${baseUrl.charges}/add`, name: `Add Charges`, categoryKey:'admin', navigationKey: 'adminProducts'}},

    proProductsMix: () => { return { url: `${baseUrl.listingPage}${baseUrl.products}${baseUrl.productMix}`, name: `Products Mix`, categoryKey:'admin', navigationKey: 'adminProducts'}},
    // proManageTaxConfigurations: () => { return { url: `${baseUrl.listingPage}${baseUrl.products}/manageTaxConfigurations`, name: `Manage Tax Configurations`, categoryKey:'admin', navigationKey: 'adminProducts'}},
    // proManageTaxComponents: () => { return { url: `${baseUrl.listingPage}${baseUrl.products}/manageTaxConfigurations/manageTaxComponents`, name: `Manage Tax Components`, categoryKey:'admin', navigationKey: 'adminProducts'}},
    // proManageTaxComponentAdd: () => { return { url: `${baseUrl.listingPage}${baseUrl.products}/manageTaxConfigurations/manageTaxComponents/add`, name: `Add Tax Component`, categoryKey:'admin', navigationKey: 'adminProducts'}},
    // proManageTaxComponentEdit: (params) => { return { url: `${baseUrl.listingPage}${baseUrl.products}/manageTaxConfigurations/manageTaxComponents/${params && params.taxId || ''}/edit`, name: `Edit Tax Component`, categoryKey:'admin', navigationKey: 'adminProducts'}},
    // proManageTaxComponentView: (params) => { return { url: `${baseUrl.listingPage}${baseUrl.products}/manageTaxConfigurations/manageTaxComponents/${params && params.taxId || ''}/view`, name: `View Tax Component`, categoryKey:'admin', navigationKey: 'adminProducts'}},

    // proManageTaxGroups: () => { return { url: `${baseUrl.listingPage}${baseUrl.products}/manageTaxConfigurations/manageTaxGroups`, name: `Manage Tax Groups`, categoryKey:'admin', navigationKey: 'adminProducts'}},
    // proManageTaxGroupAdd: () => { return { url: `${baseUrl.listingPage}${baseUrl.products}/manageTaxConfigurations/manageTaxGroups/add`, name: `Add Tax Group`, categoryKey:'admin', navigationKey: 'adminProducts'}},
    // proManageTaxGroupEdit: (params) => { return { url: `${baseUrl.listingPage}${baseUrl.products}/manageTaxConfigurations/manageTaxGroups/${params && params.taxId || ''}/edit`, name: `Edit Tax Group`, categoryKey:'admin', navigationKey: 'adminProducts'}},
    // proManageTaxGroupView: (params) => { return { url: `${baseUrl.listingPage}${baseUrl.products}/manageTaxConfigurations/manageTaxGroups/${params && params.taxId || ''}/view`, name: `View Tax Group`, categoryKey:'admin', navigationKey: 'adminProducts'}},

    proFloatingRates: () => { return { url: `${baseUrl.listingPage}${baseUrl.products}${baseUrl.floatingRate}`, name: `Floating Rates`, categoryKey:'admin', navigationKey: 'adminProducts'}},
    proFloatingRatesAdd: () => { return { url: `${baseUrl.listingPage}${baseUrl.products}${baseUrl.floatingRate}/add`, name: `Create Floating Rates`, categoryKey:'admin', navigationKey: 'adminProducts'}},
    proFloatingRatesDetails: (params) => { return { url: `${baseUrl.listingPage}${baseUrl.products}${baseUrl.floatingRate}/${params && params.floatingRateId || ''}`, name: `Floating Rates Details`, categoryKey:'admin', navigationKey: 'adminProducts'}},
    proFloatingRateDetailsEdit: (params) => { return { url: `${baseUrl.listingPage}${baseUrl.products}${baseUrl.floatingRate}/${params && params.floatingRateId || ''}/edit`, name: `Edit Floating Rates`, categoryKey:'admin', navigationKey: 'adminProducts' } },

    productMixDetails: (params) => { return { url: `${baseUrl.listingPage}${baseUrl.products}${baseUrl.productMix}/${params && params.productId || ''}`, name: `Product Mix Details`, categoryKey:'admin', navigationKey: 'adminProducts'}},
    productMixEdit: (params) => { return { url: `${baseUrl.listingPage}${baseUrl.products}${baseUrl.productMix}/${params && params.productId || ''}/edit`, name: `Edit Product Mix`, categoryKey:'admin', navigationKey: 'adminProducts'}},
    productMixAdd: () => { return { url: `${baseUrl.listingPage}${baseUrl.products}${baseUrl.productMix}/add`, name: `Add Product Mix`, categoryKey:'admin', navigationKey: 'adminProducts'}},
    
    // logout: () => { return { url: `/`, name: `Logout` } },
}

export const isRefreshTokenEnabled = false;
export const getRoute = (type, params) => {

    if (ROUTES[type]) {
        return ROUTES[type](params);
    }
    return null;
}
