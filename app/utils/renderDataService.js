/*
  This service mainly contains all the methods/functions/utilities to be used in rendering the step
  To build all the useful data objects to render all the relative components
*/

import { getFieldOrder } from './apiUtility';

const defaultFormData = {};
export const getDefaultFormData = (fieldID) => defaultFormData && defaultFormData[fieldID];

const isJson = (str) => {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
};

// Build the rendering data consisting the meta info for each field in the step
export const buildTabData = (tabData, tabKey) => {
  // being called to parse and update tabData to be able to allow display on selection.
  const fieldIds = Object.keys(tabData[tabKey]);
  const renderDataDummy = tabData[tabKey];
  fieldIds.forEach((key) => {
    // Check for 'display-on-selection' key
    // To set the isHidden flag to render or not render this field
    let { params: { meta } } = renderDataDummy[key];
    if (meta['display-on-selection']) {
      const displayConfig = meta['display-on-selection'];
      let unionFormIds = [];
      Object.keys(displayConfig).forEach(value => {
        const dependentFields = displayConfig[value];
        unionFormIds = unionFormIds.concat(dependentFields);
      });
      meta.dependentFields = unionFormIds;
    }
    // If the field is a dummy field ->
    // not being used anywhere but exist for reference of other fields (like in case of editable table)
    if (meta.dummyField) {
      renderDataDummy[key].isHidden = true;
    }
  });
  const newTabData = {
    [tabKey]: renderDataDummy,
    ...tabData,
  };
  return updateHiddenFields(newTabData, tabKey);
};

export const buildFormValues = (renderData) => {
  // curates and returns formValues

  const formValues = {};
  const fieldIds = Object.keys(renderData);
  fieldIds.forEach(fieldId => {
    formValues[fieldId] = renderData[fieldId].value || renderData[fieldId].params.meta.default;
  });
  return formValues;
}

export const updateHiddenFields = (tabData, tabKey) => {
  const fieldOrder = getFieldOrder(tabData[tabKey]);
  const formValues = buildFormValues(tabData[tabKey]);
  let updatedData = { renderData: { ...tabData[tabKey] } };
  fieldOrder.forEach((fieldId) => {
    updatedData = setHiddenOnSelection({ ...updatedData.renderData }, fieldId, formValues);
  });

  // returning only tabData, with updated renderData
  return { ...tabData, [tabKey]: updatedData.renderData };
};

// Build default form data to be submitted through the steps
//
// export const buildFormData = (renderData, fieldOrder) => {
//   const stepFormData = {};
//   if (renderData && fieldOrder) {
//     fieldOrder.forEach((fieldId) => {
//       let defaultValue = renderData[fieldId].value;
//       if (!defaultValue && defaultValue !== 0) {
//         defaultValue = renderData[fieldId].metaData && renderData[fieldId].metaData.default;
//       }
//       if (renderData[fieldId].value === false) {
//         defaultValue = false;
//       }
//       const enumConfig = renderData[fieldId].metaData.enumField;
//       if (renderData[fieldId].metaData.enumField && typeof (renderData[fieldId].metaData.enumField) === 'object') {
//         const targetFieldId = enumConfig.targetFieldId;
//         if (targetFieldId && renderData[targetFieldId]) {
//           const currentFieldValue = renderData[targetFieldId].value || (renderData[targetFieldId].metaData && renderData[targetFieldId].metaData.default);
//
//           const defaultValueObj = enumConfig.values && enumConfig.values[currentFieldValue];
//           if (defaultValueObj) {
//             defaultValue = getTranslation(defaultValueObj);
//           }
//         }
//       }
//
//       stepFormData[fieldId] = (defaultValue === false || defaultValue || defaultValue === 0) ? defaultValue : '';
//       if (renderData[fieldId].metaData.form_type === 'slider' && !renderData[fieldId].value) {
//         const meta = renderData[fieldId].metaData;
//         let isIrregular = meta.isStepIrregluar,
//           valArr;
//         if (isIrregular) {
//           valArr = meta.valArr;
//           defaultValue = defaultValue || valArr[0];
//         } else {
//           defaultValue = defaultValue ? Number(defaultValue) : meta.min * meta.step;
//         }
//         stepFormData[fieldId] = defaultValue;
//       }
//       if (renderData[fieldId].metaData.form_type === 'editableTable') {
//         if (typeof stepFormData[fieldId] === 'object') {
//           stepFormData[fieldId] = JSON.stringify(stepFormData[fieldId]);
//         }
//       }
//     });
//   }
//   defaultFormData = { ...stepFormData };
//   return stepFormData;
// };

// Update the hidden fields based on 'display-on-selection' fields

// Set hidden flag based on 'display-on-selection' object
export const setHiddenOnSelection = (renderData, elmId, formValues) => {
  const { params: { meta } } = renderData[elmId];
  const displayConfig = meta['display-on-selection'];
  if (displayConfig) {
    const unionFormIds = meta.dependentFields;
    unionFormIds.forEach((fieldId) => {
      if (renderData[fieldId]) {
        renderData[fieldId].isHidden = true;
        const defaultValue = getDefaultFormData(fieldId);
        formValues[fieldId] = defaultValue || '';
        const { params: { meta : dependentFieldMeta } } = renderData[elmId];
        // const dependentFieldMeta = renderData[fieldId].metaData;
        if (dependentFieldMeta['display-on-selection']) {
          const updatedData = setHiddenOnSelection(renderData, fieldId, formValues);
          renderData = { ...updatedData.renderData };
          formValues = { ...updatedData.formValues };
        }
      } else {
        console.error('Meta', `Missing Field ${fieldId} in ${elmId}`, renderData);
      }
    });

    if (formValues[elmId] !== undefined && formValues[elmId] !== '') {
      if (!displayConfig[formValues[elmId]]) { console.debug('displayConfig doesnt exist for', formValues[elmId]); }
      console.debug('what is inside isHidden', formValues[elmId], displayConfig[formValues[elmId].toString()]);
      // if (isJson(formValues[elmId]) && Array.isArray(JSON.parse(formValues[elmId]))) {
      //   const valArr = JSON.parse(formValues[elmId]);
      // TODO change below lines to accommodate for accepting signified array.
      if (Array.isArray(formValues[elmId])) {
        const valArr = formValues[elmId];
        valArr.forEach((value) => {
          if (displayConfig[value]) {
            displayConfig[value].forEach((fieldId) => {
              if (renderData[fieldId]) {
                renderData[fieldId].isHidden = false;
              } else {
                console.error('Meta', `Missing Field ${fieldId} in ${elmId}`, renderData);
              }
            });
          }
        });
      } else if (displayConfig[formValues[elmId]]) {
        displayConfig[formValues[elmId].toString()].forEach((fieldId) => {
          if (renderData[fieldId]) {
            renderData[fieldId].isHidden = false;
          } else {
            console.error('Meta', `Missing Field ${fieldId} in ${elmId}`, renderData);
          }
        });
      }
    }
  }
  return { renderData: { ...renderData }, formValues: { ...formValues } };
};
