export const getFieldOrder = (renderData) => {
  const fieldIds = Object.keys(renderData);
  fieldIds.sort((a, b) => {
    if (renderData[a].params.meta.order < renderData[b].params.meta.order) {
      return -1;
    }
    if (renderData[a].params.meta.order > renderData[b].params.meta.order) {
      return 1;
    }
    return 0;
  });

  return fieldIds;
};


export const getMutatedFormType = (formType, readMode) => {
  /*
  * Handle's formType change for readMode
  * */
  if (!readMode)
    return formType;

  switch (formType) {
    case 'text':
    case 'number':
    case 'long':
    case 'textarea':
    case 'dropdown':
    case "multiselect-dropdown":
    case 'datepicker':
    case 'rest-dropdown':
      return 'string';
    default:
      return formType;
  }
}
