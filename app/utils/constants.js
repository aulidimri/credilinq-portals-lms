import logo from 'images/logo.png';

export const RESTART_ON_REMOUNT = '@@saga-injector/restart-on-remount';
export const DAEMON = '@@saga-injector/daemon';
export const ONCE_TILL_UNMOUNT = '@@saga-injector/once-till-unmount';


export const brand = {
  logo
}

export const tableFixedColumnCountThreshold = 7;
