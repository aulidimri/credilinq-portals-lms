/*
 *
 * ListingPage constants
 *
 */

export const DEFAULT_ACTION = 'app/ListingPage/DEFAULT_ACTION';
export const SET_ALL_PRODUCTS_BY_ROLE = 'app/ListingPage/SET_ALL_PRODUCTS_BY_ROLE';
export const SET_ASSIGNED_LIST = 'app/ListingPage/SET_ASSIGNED_LIST';
export const SET_ACTIVE_PRODUCT_BY_ROLE = 'app/ListingPage/SET_ACTIVE_PRODUCT_BY_ROLE';
export const SET_BUCKET_KEY = 'app/ListingPage/SET_BUCKET_KEY';
export const RESET_PRODUCTS = 'app/ListingPage/RESET_PRODUCTS';
export const SET_ASSIGNED_LIST_CURRENT_PAGE = "app/ListingPage/SET_ASSIGNED_LIST_CURRENT_PAGE";
export const SET_ASSIGNED_LIST_PAGE_WIDTH = "app/ListingPage/SET_ASSIGNED_LIST_PAGE_WIDTH"

export const mapIconKeyToIconType = {
  DELETE: 'delete',
  CLAIM: 'check',
  EDIT: 'edit',
}

export const mapIconKeyToActionKey = {
  CLAIM: 'claim',
  DELETE: 'delete',
  HIDE: 'hide',
  ADD: 'add',
  EDIT: 'edit',
}

export const paginationSettings = {
  showSizeChanger: true,
  pageSizeOptions: ['5', '10', '15', '20'],
  showTotal: (total, range) => `showing ${range[0]}-${range[1]} from ${total}`
};
