import styled from 'styled-components';
import { colors, sizes } from 'configs/styleVars';

export const Wrapper = styled.div`
  flex-grow: 1;
  background-color: ${colors.primaryBodyBG};
  padding: ${sizes.paddingL};
  padding-top: 0;

  .productSelectBar {
    display: flex;
  }

  .productSelectLabel {
    color: ${colors.secondaryBodyColor};
    margin-right: ${sizes.marginS};
  }

  .productSelect {
    width: 200px;
  }

`;
