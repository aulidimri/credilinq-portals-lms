/*
 *
 * ListingPage actions
 *
 */

import {
  DEFAULT_ACTION,
  SET_ALL_PRODUCTS_BY_ROLE,
  SET_ACTIVE_PRODUCT_BY_ROLE,
  SET_ASSIGNED_LIST,
  SET_BUCKET_KEY,
  RESET_PRODUCTS,
  SET_ASSIGNED_LIST_CURRENT_PAGE,
  SET_ASSIGNED_LIST_PAGE_WIDTH,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function setAllProductsByRole(allProducts) {
  return {
    type: SET_ALL_PRODUCTS_BY_ROLE,
    allProducts,
  };
}

export function setAssignedList(list) {
  return {
    type: SET_ASSIGNED_LIST,
    list,
  };
}

export function setActiveProductByRole(product) {
  return {
    type: SET_ACTIVE_PRODUCT_BY_ROLE,
    product,
  };
}

export function setBucketByActiveProductByRole(bucketKey) {
  return {
    type: SET_BUCKET_KEY,
    bucketKey,
  };
}

export function setAssignedListCurrentPage(currentPage) {
  return {
    type: SET_ASSIGNED_LIST_CURRENT_PAGE,
    currentPage,
  }
}

export function setAssignedListPageWidth(pageWidth) {
  return {
    type: SET_ASSIGNED_LIST_PAGE_WIDTH,
    pageWidth,
  }
}

export function resetProductsPage() {
  return {
    type: RESET_PRODUCTS,
  };
}



