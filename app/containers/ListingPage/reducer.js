/*
 *
 * ListingPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  SET_ALL_PRODUCTS_BY_ROLE,
  SET_ASSIGNED_LIST,
  SET_ACTIVE_PRODUCT_BY_ROLE,
  SET_BUCKET_KEY,
  SET_ASSIGNED_LIST_CURRENT_PAGE,
  SET_ASSIGNED_LIST_PAGE_WIDTH,
  RESET_PRODUCTS
} from './constants';

const initialState = fromJS({
  allProductsByRole: null,
  assignedList: null,
  activeProduct: null,
  activeBucket: null,
  assignedListCurrentPage: 1,
  assignedListPageWidth: 10,
});

function listingPageReducer(state = initialState, action) {
  switch (action.type) {
    case SET_ALL_PRODUCTS_BY_ROLE:
      // TODO: Candidate for backend
      //console.log("SET_ALL_PRODUCTS_BY_ROLE", action.allProducts);
      action.allProducts.forEach(product => {
        let buckets = {}
        product.bucketList.forEach(bucket => {
          buckets[bucket.id] = bucket
        })
        product.buckets = buckets;
      })
      return state
        .set('allProductsByRole', action.allProducts);

    case SET_ASSIGNED_LIST:
      console.log(action.list);
      return state
        .set('assignedList', action.list);

    case SET_ACTIVE_PRODUCT_BY_ROLE:
      return state
        .set('activeProduct', action.product);

    case SET_BUCKET_KEY:
      return state
        .set('activeBucket', action.bucketKey);

    case SET_ASSIGNED_LIST_CURRENT_PAGE:
      return state
        .set("assignedListCurrentPage", action.currentPage);

    case SET_ASSIGNED_LIST_PAGE_WIDTH:
      return state
        .set("assignedListPageWidth", action.pageWidth);

    case RESET_PRODUCTS:
      return state
        .set('allProductsByRole', null)
        .set('assignedList', null)
        .set('activeProduct', null)
        .set('activeBucket', null)
        .set('assignedListCurrentPage', 1)
        .set('assignedListPageWidth', 10)

    default:
      return state;
  }
}

export default listingPageReducer;
