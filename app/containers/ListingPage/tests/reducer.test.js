
import { fromJS } from 'immutable';
import listingPageReducer from '../reducer';

describe('listingPageReducer', () => {
  it('returns the initial state', () => {
    expect(listingPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
