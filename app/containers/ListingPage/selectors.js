import { createSelector } from 'reselect';

/**
 * Direct selector to the dashboardPage state domain
 */
const selectListingPageDomain = (state) => state.get('listingPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ListingPage
 */

const makeSelectListingPage = () => createSelector(
  selectListingPageDomain,
  (substate) => substate.toJS()
);

// const makeSelectAllProductsByRole = () => createSelector(
//   selectListingPageDomain,
//   (substate) => substate.get('allProductsByRole')
// );

const makeSelectAssignedList = () => createSelector(
  selectListingPageDomain,
  (substate) => substate.get('assignedList')
);

const makeSelectAssignedListCurrentPage = () => createSelector(
  selectListingPageDomain,
  (substate) => substate.get('assignedListCurrentPage')
);

const makeSelectAssignedListPageWidth = () => createSelector(
  selectListingPageDomain,
  (substate) => substate.get('assignedListPageWidth')
);

export {
  makeSelectListingPage,
  selectListingPageDomain,
  //makeSelectAllProductsByRole,
  makeSelectAssignedList,
  makeSelectAssignedListCurrentPage,
  makeSelectAssignedListPageWidth,
};
