/**
 *
 * ListingPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import injectReducer from 'utils/injectReducer';
import Table from 'components/Table';
import Row from 'components/Row';
import Col from 'components/Col';
import { Wrapper } from './Styles';
import { makeSelectListingPage, makeSelectAllProductsByRole, makeSelectAssignedList, makeSelectAssignedListCurrentPage, makeSelectAssignedListPageWidth } from './selectors';
import reducer from './reducer';
import { makeSelectRoleSelected } from 'containers/App/selectors';
import { networkAction, setRecord, refreshDetailsPage } from 'containers/App/actions';
import { setAllProductsByRole, setActiveProductByRole, setAssignedList, setAssignedListCurrentPage, setAssignedListPageWidth, setBucketByActiveProductByRole } from './actions';
import { mapIconKeyToIconType, mapIconKeyToActionKey, paginationSettings } from './constants';

import Tabs from 'components/Tabs';
import Select from 'components/Select';
import uuid from 'uuid/v4';
import dottie from 'dottie';


//TODO: Add comments on this entire page for all methods
//TODO: Improve code quality

/**
 * Page containing the Loan Applications List
 */
export class ListingPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  state = {
    activeTab: '',
    activeProduct: '',
    selectedRowKeys: [],
  }

  //TODO: Use componentDidUpdate or getDerivedStateFromProps instead cuz these are guaranteed to fire only once per update and safer to use according to the docs
  componentDidUpdate(prevProps, prevState) {
    if (this.props.activeProduct !== prevProps.activeProduct) {
      this.onProductChange(this.props.activeProduct)
    }
  }

  componentDidMount() {
    const { roleSelected, listingPage } = this.props;
    console.log('props in listinPage', listingPage);
    this.props.refreshDetailsPage();
    const reqData = {
      api: 'products',
      method: 'GET',
      params: { roleName: roleSelected.key },
      successAction: this.props.setAllProductsByRole,
      successCallback: (res) => {
        console.log("RES: ", res);
        let defaultProduct = listingPage.activeProduct || (res.data && res.data.length && res.data[0]);
        const { productType, bucketList } = defaultProduct;
        const bucketKey = listingPage.activeBucket || (bucketList && bucketList.length && bucketList[0].id);
        if (!productType || !bucketKey)
          return;

        this.props.setActiveProductByRole(defaultProduct);
        const queryParams = {
          offset: this.props.assignedListCurrentPage-1,
          pageWidth: this.props.assignedListPageWidth,
        }

        // Need to make call for assigned list
        this.fetchAssignedListForBucket(productType, bucketKey, queryParams, () => {
          this.setState({
            activeTab: bucketKey,
            activeProduct: defaultProduct
          })
        });
      }
    };

    //console.log("Component did mount: ");
    this.props.networkAction(reqData);
  }



  onSelectChange = (selectedRowKeys) => {
    this.setState({ selectedRowKeys });
  };

  //TODO: Remove the need to pass productType and bucketKey as args, fetch them directly from state or props
  //TODO: Discuss whether to put product in the app store
  //TODO: set filter value and filter key in the state on search
  //TODO: Query params should also be built directly from the state for better UX such as not required to set the search key everytime the user changes tab or product
  fetchAssignedListForBucket = (productType, bucketKey, queryParams, callBack) => {

    const { roleSelected } = this.props;
    const assignedListData = {
      api: 'assignedList',
      method: 'GET',
      params: { roleName: roleSelected.key, groupId: roleSelected.id, productType, bucketKey },
      queryParams,
      loader: true,
      successAction: this.props.setAssignedList,
      successCallback: (response) => {
        this.props.setBucketByActiveProductByRole(bucketKey);
        this.setState({
          selectedRowKeys: [],
        })
        console.log(`assigned list for ${bucketKey} bucket and ${productType} product`, response.data);
        if (callBack)
          callBack();
      }
    }
    this.props.networkAction(assignedListData);
  }

  handleAction(e, key, record) {
    e.stopPropagation();

    //TODO Fetch action key types as variables from the contants file ( But before this, discuss whether action is specified by iconKey or id in the api)
    console.log("handleActions");
    switch (key) {
      case 'delete':
        console.log(record, "DELETED!!");
        break;
      case 'edit':
        console.log(record, "EDITTED!!");
        break;
      case 'claim':
        console.log(record, "CLAIMED!!");
        break;
      default:
        console.log(record, "default action");
    }
  }

  handleBulkAction = (e, key, records) => {

    //TODO: Fetch action key types as variables from the contants file
    switch (key) {
      case 'delete':
        console.log(records, "DELETED!!");
        break;
      case 'edit':
        console.log(records, "EDITTED!!");
        break;
      case 'claim':
        console.log(records, "CLAIMED!!");
        break;
      default:
        console.log(records, "default bulk action");
    }
  }

  fetchTableData = () => {
    const { activeTab } = this.state;
    const { assignedList, activeProduct } = this.props.listingPage;

    const dataSource = assignedList[activeTab] && assignedList[activeTab].length && assignedList[activeTab].map(row => ({
      key: row.caseInstanceId,
      ...row,
    })) || [];

    const columns = [];
    let actions;
    const variables = dottie.get(activeProduct, `buckets.${activeTab}.variables`)
    Array.isArray(variables) && variables.forEach(variable => {
      if (variable.type === 'listOfAction') {
        actions = {
          label: variable.label || "",
          key: variable.id || "",
          list: Array.isArray(variable.actionConfigurationList) && variable.actionConfigurationList.map(({ iconKey, label, disabled }) => ({
            key: mapIconKeyToActionKey[iconKey],
            icon: mapIconKeyToIconType[iconKey],
            label: label,
            //disabled: disabled??, //First check the api for support
          })) || [],
        }
      }
      else {
        columns.push({
          title: variable.label,
          dataIndex: variable.id,
        })
      }
    });

    const bucketActions = dottie.get(activeProduct, `buckets.${activeTab}.bucketActions`);
    const bulkActions = Array.isArray(bucketActions) && bucketActions.map(({ iconKey, label }) => ({
      key: mapIconKeyToActionKey[iconKey],
      label: label,
    })) || [];
    console.log('tabledata', dataSource, columns);
    return {
      rows: dataSource,
      headings: columns,
      actions: actions,
      bulkActions: bulkActions,
    }
  }

  selectedRow = record => {
    console.log('record', record);
    const { caseInstanceId, assignee } = record;
    this.props.setRecord({ caseInstanceId, assignee });
    if (record.bucket === 'claim') return;
    this.props.history.push('/app/details');
  };

  tabChange = (key) => {
    //console.log('in tab change', key);
    const { activeProduct } = this.props.listingPage;
    const queryParams = {
      offset: 0,
      pageWidth: this.props.assignedListPageWidth,
    }

    // Need to make api call to fetch assigned list here
    this.fetchAssignedListForBucket(activeProduct.productType, key, queryParams, () => {
      this.setState({
        activeTab: key,
      })
      this.props.setAssignedListCurrentPage(1);
    });
  }

  onProductChange = (value) => {
    const { allProductsByRole } = this.props.listingPage;
    const activeProduct = allProductsByRole.filter(product => product.productLabel === value)[0];

    const { productType, bucketList } = activeProduct;
    const bucketKey = bucketList && bucketList.length && bucketList[0].id;
    this.props.setActiveProductByRole(activeProduct);
    const queryParams = {
      offset: 0,
      pageWidth: this.props.assignedListPageWidth,
    }
    this.fetchAssignedListForBucket(productType, bucketKey, queryParams, () => {
      this.setState({
        activeTab: bucketKey,
        //activeProduct: activeProduct
      })
      this.props.setAssignedListCurrentPage(1);
    });
  }

  onSearchHandler = (value, filterKey) => {
    const { activeProduct } = this.props.listingPage;
    const { productType, bucketList } = this.state.activeProduct;
    const bucketKey = this.state.activeTab;
    const queryParams = {
      offset: 0,
      pageWidth: this.props.assignedListPageWidth,
      [filterKey]: value,                              //THE ORDER OF QUERY PARAMS IS IMPORTANT...
    }
    console.log(queryParams);

    this.fetchAssignedListForBucket(
      productType,
      bucketKey,
      queryParams,
      () => {
        this.props.setAssignedListCurrentPage(1);
      }
    );
  }

  onPageChangeHandler = (page, pageSize) => {
    const { activeProduct } = this.props.listingPage;
    const { productType, bucketList } = activeProduct;
    const bucketKey = this.state.activeTab;
    const queryParams = {
      offset: page-1,
      pageWidth: this.props.assignedListPageWidth,
    }
    console.log(queryParams);

    this.fetchAssignedListForBucket(
      productType,
      bucketKey,
      queryParams,
      () => {
        this.props.setAssignedListCurrentPage(page);
      }
    );
  }

  onPageSizeChangeHandler = (current, size) => {
    const { activeProduct } = this.props.listingPage;
    const { productType, bucketList } = activeProduct;
    const bucketKey = this.state.activeTab;
    const queryParams = {
      offset: 0,
      pageWidth: size,
    }
    console.log(queryParams);

    this.fetchAssignedListForBucket(
      productType,
      bucketKey,
      queryParams,
      () => {
        this.props.setAssignedListPageWidth(size);
        this.props.setAssignedListCurrentPage(1);
      }
    );
  }

  configurePagination = () => {
    const { assignedList, assignedListCurrentPage, assignedListPageWidth } = this.props;

    //Setting pagination to false, hides pagination and displays all the contents of the table at once
    const showPagination = assignedList.meta.showPagination;
    if(!showPagination)
      return false;

    const totalCount = assignedList.meta.totalCount;
    const pageWidth = assignedListPageWidth;
    const currentPage = assignedListCurrentPage;
    const pagination = {
      ...paginationSettings,
      onChange: this.onPageChangeHandler,
      onShowSizeChange: this.onPageSizeChangeHandler,
      pageSize: pageWidth,
      current: currentPage,
      total: totalCount,
    };
    return pagination;
  }

  render() {
    //console.log('in listing render', this.state);
    const { assignedList, assignedListCurrentPage, assignedListPageWidth } = this.props;
    const { allProductsByRole, activeProduct } = this.props.listingPage;
    const { activeTab } = this.state;

    if (!allProductsByRole || !activeTab || !activeProduct) return null;


    let tabs = [];
    let productOptions = [];
    allProductsByRole.forEach(product => {
      productOptions.push({
        label: product.productLabel,
        ...product
      })
    })

    tabs = activeProduct.bucketList.map(bucket => {
      return {
        key: bucket.id,
        heading: bucket.label,
        children: '',
      }
    })
    const { rows, headings, actions, bulkActions } = this.fetchTableData();
    const pagination = this.configurePagination();


    const productSelect = (
      <div className="productSelectBar">
        <div className="productSelectLabel">Product: </div>
        <Select className="productSelect"
          componentProps={{
            defaultValue: activeProduct.productLabel,
            options: productOptions,
          }}
          onChangeHandler={this.onProductChange}
        />
      </div>
    );

    const selection = true;
    const { selectedRowKeys } = this.state;
    let rowSelection;
    if (selection) {
      rowSelection = {
        selectedRowKeys,
        onChange: this.onSelectChange,
      };
    }


    return (
      <Wrapper>
        <Row>
          <Col componentProps={{ span: 24 }}>
            <Tabs
              componentProps={{ animated: false, /*tabBarExtraContent: productSelect,*/ activeKey: activeTab }}
              tabs={tabs}
              callBack={this.tabChange}
            />
            <Table
              componentProps={{
                className: "loanApplicationsList",
                onRow: (record, rowIndex) => ({
                  onClick: event => {
                    this.selectedRow(record);
                  },
                }),
                rows,
                headings,
                actions,
                bulkActions,
                iconsToDisplay: 2,
                isSearch: true,
                rowSelection: rowSelection,
                pagination,
              }}
              handleAction={this.handleAction}
              handleBulkAction={this.handleBulkAction}
              onSearchHandler={this.onSearchHandler}
            />
          </Col>
        </Row>
      </Wrapper>
    )
  }
}

ListingPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  match: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  listingPage: makeSelectListingPage(),
  roleSelected: makeSelectRoleSelected(),
  assignedList: makeSelectAssignedList(),
  assignedListCurrentPage: makeSelectAssignedListCurrentPage(),
  assignedListPageWidth: makeSelectAssignedListPageWidth(),
});

function mapDispatchToProps(dispatch) {
  return {
    networkAction: (networkData) => dispatch(networkAction(networkData)),
    setAllProductsByRole: (data) => dispatch(setAllProductsByRole(data)),
    setActiveProductByRole: (data) => dispatch(setActiveProductByRole(data)),
    setBucketByActiveProductByRole: (bucketKey) => dispatch(setBucketByActiveProductByRole(bucketKey)),
    setAssignedList: (data) => dispatch(setAssignedList(data)),
    setAssignedListCurrentPage: (currentPage) => dispatch(setAssignedListCurrentPage(currentPage)),
    setAssignedListPageWidth: (pageWidth) => dispatch(setAssignedListPageWidth(pageWidth)),
    setRecord: (data) => dispatch(setRecord(data)),
    refreshDetailsPage: () => dispatch(refreshDetailsPage()),
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'listingPage', reducer });

export default compose(
  withReducer,
  withConnect,
)(ListingPage);
