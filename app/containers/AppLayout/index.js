/**
 *
 * AppLayout
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { Layout } from 'antd';
import { withRouter } from 'react-router';
import { ConnectedRouter } from 'connected-react-router/immutable';
import { Route, Switch, Link, Redirect } from 'react-router-dom';
//import { setAllProductsByRole, setActiveProductByRole, setAssignedList, setBucketByActiveProductByRole } from './actions';


import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { makeSelectAppLayout } from './selectors';
import { makeSelectApp } from 'containers/App/selectors';
import { makeSelectListingPage } from 'containers/ListingPage/selectors';
import reducer from './reducer';
import saga from './saga';
import DetailsPage from 'containers/DetailsPage/Loadable';
import SelectionPage from 'containers/SelectionPage/Loadable';
import ListingPage from 'containers/ListingPage/Loadable';
import ErrorPage from 'containers/ErrorPage/Loadable';
import { ERR_NOT_FOUND } from 'containers/ErrorPage/constants';

import LoadingIndicator from 'components/LoadingIndicator';
import { Wrapper } from './Styles';


import UserInfo from 'components/UserInfo';
import Icon from 'components/Icon';
import Menu from 'components/Menu';
import MenuItem from 'components/MenuItem';
import Dropdown from 'components/Dropdown';
import { brand } from 'utils/constants';
import uuid from 'uuid';

const { Content, Header } = Layout;

export class AppLayout extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props)
    this.state = {
      activeProduct: '',
      editMode: true, //Change this to disable changing of product
    }
  }


  onProductChange = (e, activeProduct) => {
    this.setState({
      activeProduct,
    });
  }



  onClickHandler = (e, key) => {
    // console.log('clicked ', key, this);
    switch (key) {
      case 'role':
        this.props.history.push('/app/selection')
        break;

      default:
        break;
    }
  };

  render() {
    const { match, location, app, listingPage: { allProductsByRole, activeProduct } } = this.props;
    //console.log('in app render', this.props, allProductsByRole, activeProduct);
    const NotFoundPage = (props) => (
      <ErrorPage error={ERR_NOT_FOUND} {...props}></ErrorPage>
    );

    //TODO: Check if there is a better way to pass userOptions as props without checking pathname
    const userOptions = location.pathname === '/app/selection' ? [] : app.userOptions;
    const reactUserOptions = (
      userOptions && userOptions.length &&
      userOptions.map((option, index) => (
        <div className="app-header-option" key={uuid()}>
          <Icon
            componentProps={{ type: option.icon, label: option.value, prefix: true }}
            onClickHandler={this.onClickHandler}
            onClickParams={[option.key]}
          />
        </div>
      ))
    ) || [];

    /******************************************************** */
    //const productList = ["DP1", "DP2", "DP3", "Dummy Product Teessst"];
    const productData = allProductsByRole && allProductsByRole.map(product => ({
      label: product.productLabel,
      onClickParams: [product.productLabel,]
    })) || [];


    const productDropdownDisabled = location.pathname !== '/app/listing';
    const productDropdown = productData && (
      <Dropdown
        onClickHandler={this.onProductChange}
        componentProps={{
          data: productData,
          placement: 'bottomRight',
          trigger: ['click'],
          disabled: productDropdownDisabled
        }}
      >
        {
          productDropdownDisabled ? <div>{activeProduct && activeProduct.productLabel}</div> : (<Icon
            componentProps={{
              label: activeProduct && activeProduct.productLabel,
              prefix: true,
              type: 'caret-down',
            }}
          />)
        }
      </Dropdown>
    )
    /*********************************************************************************8 */

    return (
      <Wrapper>
        {
          this.props.applayout.loaderCount ? (
            <LoadingIndicator />
          ) : null
        }
        <ConnectedRouter history={this.props.history}>
          <Layout style={{ height: "100%" }}>

            <div className="app-header">
              <Header>
                <div className="app-header-info">
                  <img id="app-header-logo" src={brand.logo} alt="logo" />
                  {productDropdown}
                </div>
                <div className="app-header-extras">
                  {reactUserOptions}
                  <UserInfo className="app-header-option" userInfo={app.userInfo} />
                </div>
              </Header>
            </div>

            <Content style={{ display: 'flex', flexDirection: 'column' }}>
              <Switch>
                <Route
                  exact
                  path="/app"
                  render={() => (<Redirect to={{ pathname: `${match.path}/selection` }} />)} />
                <Route path={`${match.path}/details`} component={DetailsPage}></Route>
                <Route path={`${match.path}/selection`} exact component={SelectionPage}></Route>
                {/*<Route path={`${match.path}/listing`} exact component={ListingPage}></Route>*/}
                <Route path={`${match.path}/listing`} exact render={() => (<ListingPage {...this.props} activeProduct={this.state.activeProduct} />)}></Route>
                <Route component={NotFoundPage} />
              </Switch>
            </Content>
          </Layout>
        </ConnectedRouter>
      </Wrapper>
    );
  }
}

AppLayout.propTypes = {
  dispatch: PropTypes.func.isRequired,
  applayout: PropTypes.object,
  history: PropTypes.object,
  match: PropTypes.object,
  location: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  applayout: makeSelectAppLayout(),
  app: makeSelectApp(),
  listingPage: makeSelectListingPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    //setActiveProductByRole: (data) => dispatch(setActiveProductByRole(data)),

  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'appLayout', reducer });
const withSaga = injectSaga({ key: 'appLayout', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
  withRouter
)(AppLayout);

/*
*/
