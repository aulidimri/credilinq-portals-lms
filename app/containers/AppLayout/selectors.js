import { createSelector } from 'reselect';

/**
 * Direct selector to the appLayout state domain
 */
const selectAppLayoutDomain = (state) => state.get('appLayout');

/**
 * Other specific selectors
 */


/**
 * Default selector used by AppLayout
 */

const makeSelectAppLayout = () => createSelector(
  selectAppLayoutDomain,
  (substate) => substate.toJS()
);

export {
  makeSelectAppLayout,
  selectAppLayoutDomain,
};
