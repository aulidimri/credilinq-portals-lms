/*
 *
 * AppLayout constants
 *
 */
export const DEFAULT_ACTION = 'app/AppLayout/DEFAULT_ACTION';
export const TOGGLE_SIDEBAR_COLLAPSE = 'app/AppLayout/TOGGLE_SIDEBAR_COLLAPSE';
export const INCREMENT_LOADER = 'app/AppLayout/INCREMENT_LOADER';
export const DECRMENT_LOADER = 'app/AppLayout/DECRMENT_LOADER';




export const SET_ACTIVE_PRODUCT_BY_ROLE = 'app/ListingPage/SET_ACTIVE_PRODUCT_BY_ROLE';
