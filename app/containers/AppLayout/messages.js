/*
 * AppLayout Messages
 *
 * This contains all the text for the AppLayout component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.AppLayout.header',
    defaultMessage: 'This is AppLayout container !',
  },
});
