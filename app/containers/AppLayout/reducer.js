/*
 *
 * AppLayout reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  TOGGLE_SIDEBAR_COLLAPSE,
  INCREMENT_LOADER,
  DECRMENT_LOADER,
  SET_ACTIVE_PRODUCT_BY_ROLE
} from './constants';

const initialState = fromJS({
  sidebar: {
    collapse: false,
  },
  loaderCount: 0,
});

function appLayoutReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case TOGGLE_SIDEBAR_COLLAPSE:
      return state.setIn(['sidebar', 'collapse'], action.collapse);

    case INCREMENT_LOADER:
      //TODO need to check why it returns NaN after 1st iteration
      return state
        .set('loaderCount', (state.get('loaderCount') || 0) + 1);

    case DECRMENT_LOADER:
      return state
        .set('loaderCount', state.get('loaderCount') - 1);









    case SET_ACTIVE_PRODUCT_BY_ROLE:
      return state
        .set('activeProduct', action.product);

    default:
      return state;
  }
}

export default appLayoutReducer;
