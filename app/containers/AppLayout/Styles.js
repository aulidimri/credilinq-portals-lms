import styled from 'styled-components';
import { colors, fonts, sizes } from 'configs/styleVars';

export const Wrapper = styled.div`
  height: 100%;


  .app-header-product-select {
  }

  .app-header-info {
    display: flex;
    align-items: flex-end;
  }

.app-header {
      position: relative;
    &::before {
      content: '';
      box-shadow: rgb(220, 220, 220) 0px 1px 5px;
      width: 100%;
      position: absolute;
      top: 0;
      bottom: 0;
      pointer-events: none;
    }

    .ant-layout-header {
      background-color: ${colors.primaryBodyBG};
      display: flex;
      justify-content: space-between;
      align-items: center;
      line-height: inherit;
      font-size: ${fonts.fontSizeS};
      color: ${colors.primaryBodyColor};
      height: 55px;
    }

    #app-header-logo {
      padding: 0 ${sizes.paddingM} 0 0;
      height: 30px;
    }

    .dividerStyle {
      height: ${fonts.fontSizeM};
      margin-left: ${sizes.marginM};
    }

    .app-header-extras {
      display: flex;
      align-items: center;
      color: ${colors.primaryBodyColor};
    }

    .app-header-option {
      padding: 0 ${sizes.paddingS};
      align-self: stretch;
      &:not(:last-child) {
        border-right: 1px solid ${colors.secondaryBodyColor};
      }
    }
  }




`
