/*
 *
 * AppLayout actions
 *
 */

import {
  DEFAULT_ACTION,
  TOGGLE_SIDEBAR_COLLAPSE,
  INCREMENT_LOADER,
  DECRMENT_LOADER,
  SET_ACTIVE_PRODUCT_BY_ROLE
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}
export function setSidebarCollapse(collapse) {
  return {
    type: TOGGLE_SIDEBAR_COLLAPSE,
    collapse,
  };
}

export function incrementLoader() {
  return {
    type: INCREMENT_LOADER,
  };
}

export function decrementLoader() {
  return {
    type: DECRMENT_LOADER,
  };
}




export function setActiveProductByRole(product) {
  return {
    type: SET_ACTIVE_PRODUCT_BY_ROLE,
    product,
  };
}
