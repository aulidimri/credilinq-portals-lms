
import { fromJS } from 'immutable';
import authenticatedRouteReducer from '../reducer';

describe('authenticatedRouteReducer', () => {
  it('returns the initial state', () => {
    expect(authenticatedRouteReducer(undefined, {})).toEqual(fromJS({}));
  });
});
