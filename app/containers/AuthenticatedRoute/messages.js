/*
 * AuthenticatedRoute Messages
 *
 * This contains all the text for the AuthenticatedRoute component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.AuthenticatedRoute.header',
    defaultMessage: 'This is AuthenticatedRoute container !',
  },
});
