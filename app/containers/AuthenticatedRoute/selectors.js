import { createSelector } from 'reselect';

/**
 * Direct selector to the authenticatedRoute state domain
 */
const selectAuthenticatedRouteDomain = (state) => state.get('authenticatedRoute');


/**
 * Default selector used by AuthenticatedRoute
 */

const makeSelectAuthenticatedRoute = () => createSelector(
  selectAuthenticatedRouteDomain,
  (substate) => substate.toJS()
);

export {
  makeSelectAuthenticatedRoute,
  selectAuthenticatedRouteDomain,
};
