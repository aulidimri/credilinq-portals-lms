/**
 *
 * AuthenticatedRoute
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Route, Redirect } from 'react-router-dom';
import moment from 'moment';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { makeSelectAuthenticatedRoute } from './selectors';
import { makeSelectAuth } from 'containers/App/selectors';
import reducer from './reducer';
import saga from './saga';
// import messages from './messages';

export class AuthenticatedRoute extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { component: Component, ...rest } = this.props;
    return (
      (this.props.auth.token || true) ?
      (
        <Route component={Component} {...rest} />
      ) :
      (
        <Redirect to={{ pathname: '/forbidden', state: { from: this.props.location } }} />
      )
    );
  }
}

AuthenticatedRoute.propTypes = {
  dispatch: PropTypes.func.isRequired,
  component: PropTypes.any.isRequired,
  auth: PropTypes.object,
  location: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  authenticatedRoute: makeSelectAuthenticatedRoute(),
  auth: makeSelectAuth(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'authenticatedRoute', reducer });
const withSaga = injectSaga({ key: 'authenticatedRoute', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(AuthenticatedRoute);
