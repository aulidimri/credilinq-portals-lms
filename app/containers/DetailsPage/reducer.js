/*
 *
 * DetailsPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  TOGGLE_SIDEBAR_COLLAPSE,
  SET_TAB_CONFIG,
  SET_TAB_DATA,
  SET_ACTIVE_TAB,
  SET_SCREEN_INFO,
//  SET_COMMENTS,
} from './constants';

const initialState = fromJS({
  sidebar: {
    collapse: false,
  },
  tabs: {
    tabConfig: null,
    tabData: null,
    comment: [],
    activeTab: null
  },
});

function detailsPageReducer(state = initialState, action) {
  //console.debug('what action being fired', action);
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;

    case TOGGLE_SIDEBAR_COLLAPSE:
      return state.setIn(['sidebar', 'collapse'], action.collapse);

    //TODO: This entire operation to be removed once backend starts sending icons
    case SET_TAB_CONFIG:
      if(action.data && action.data.length) {
        action.data.forEach(tab => {
          //tab.icon = 'user';
          //TODO: Need to remove, tab.path = tab.id
          //tab.path = tab.id;
          if(tab.children && tab.children.length) {
            tab.children.forEach(child => {
              child.icon = 'caret-right';
            })
          }
        })
      }
      return state.setIn(['tabs', 'tabConfig'], action.data);

    case  SET_TAB_DATA:
      return state
        .setIn(['tabs', 'tabData'], action.data)
        //.setIn(['tabs', 'comment'], action.data && (action.data.comment || []))

    case SET_ACTIVE_TAB:
      return state
        .setIn(['tabs', 'activeTab'], action.key)

    case SET_SCREEN_INFO:
      return state
        .setIn(['tabs', 'screenInfo'], action.key)

    // case SET_COMMENTS:
    //   //console.log('in reducer for SET_COMMENTS', action.comment);
    //   return state
    //     .updateIn(['tabs', 'comment'], arr => arr.concat(action.comment))


    default:
      return state;
  }
}

export default detailsPageReducer;
