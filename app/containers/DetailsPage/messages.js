/*
 * DetailsPage Messages
 *
 * This contains all the text for the DetailsPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.DetailsPage.header',
    defaultMessage: 'This is DetailsPage container !',
  },
});
