import React from 'react';
import { Route, Switch, Link, Redirect } from 'react-router-dom';
//import 'antd/lib/breadcrumb/style/index.less';
import { Icon, Menu } from 'antd';
import ApplicationDetailsPage from 'containers/ApplicationDetailsPage/Loadable';
import ErrorPage from 'containers/ErrorPage/Loadable';
import { ERR_NOT_FOUND } from 'containers/ErrorPage/constants';
/* eslint-disable object-property-newline */
import { routeItemStyle, labelItemStyle } from './Styles';

//TODO: Need to remove this
const routes = [];

const NotFoundPage = (props) => (
  <ErrorPage error={ERR_NOT_FOUND} {...props}></ErrorPage>
);

function joinPath(base, path) {
  let result = `${base}/${path}`;
  if (!(path && path.length > 0)) {
    result = `${base}`;
  }
  return result;
}

function AppBreadcrumbRecursive(basePath, r = routes) {
  return (
    <Switch>
      {r.map((route) => (
        <Route
          strict
          exact={!(route.children && route.children.length > 0)}
          key={joinPath(basePath, route.path)}
          path={joinPath(basePath, route.path)}
        >
          <span>
            {route.title && <span className="ant-breadcrumb-separator">/</span>}
            <Link
              className="ant-breadcrumb-link"
              to={joinPath(basePath, route.path)}
            >
              {route.title}
            </Link>
            {route.children && route.children.length > 0 &&
              AppBreadcrumbRecursive(joinPath(basePath, route.path), route.children)}
          </span>
        </Route>
      ))}
    </Switch>);
}

function AppPathRecursive(r = routes, p = '') {
  //console.log('routes', r, p);
  //TODO to return array of paths;
  return Array.isArray(r) ? r.map(route => {
    return route.children && route.children.length > 0 && AppPathRecursive(route.children);
  }) : r.label

}

export function AppPath(r = routes) {
  return AppPathRecursive(r);
}
// function handleMenuClick(args) {
//   console.log('clicked args', args, );
// }
function AppMenuRecursive(basePath, r = routes, portalVisitedTabs, handleMenuClick, linkPath) {
  return r.map((route) => {

    const isVisited = portalVisitedTabs.indexOf(route.id) > -1;
    const isVisitedIcon = isVisited ? <Icon type='check' style={{color: 'green'}} /> : null;
    const getRouteIcon = icon => icon ? <span><Icon type={icon}/></span> : null;
    const routePath = [...linkPath];
    const getRouteItem = route => {
      return (
        <span style={routeItemStyle}>
          <span style={labelItemStyle}>{getRouteIcon(route.icon)} {route.label}</span>
          <span> {isVisitedIcon}</span>
        </span>
      );
    };

    routePath.push(route.id);
    let view = (
      <Menu.Item
        onClick={(args) => handleMenuClick(args, route.id,  routePath.join("/"))} key={route.id}>
        {getRouteItem(route)}
      </Menu.Item>
    );
    if (route.children && route.children.length > 0) {
      view = (
        <Menu.SubMenu
          title={
            <span style={routeItemStyle}>
              <span> {getRouteIcon(route.icon)} {route.label}</span>
            </span>
          }
          key={route.id}
        >
          {AppMenuRecursive(joinPath(basePath, route.id), route.children, portalVisitedTabs , handleMenuClick, routePath)}
        </Menu.SubMenu>
      );
    }
    return view;
  });
}

export function AppBreadcrumb(basePath, r = routes) {
  return (
    <div style={{ marginBottom: '16px' }}>
      <Link className="ant-breadcrumb-link" to={`${basePath}`}>
        <Icon type="home" style={{ fontSize: 16 }}></Icon>
      </Link>
      {AppBreadcrumbRecursive(basePath, r)}
    </div>
  );
}



//TODO: Add comments on why are we doing this
export function AppRouter(basePath, r = routes, self) {
  //console.log('basePath and routes', basePath, r, self);
  let initialRoute = r[0];
  let initialPath = initialRoute.childPresent ? joinPath(initialRoute.id, initialRoute.children[0].id) : initialRoute.id
  let defaultRoute = joinPath(basePath, initialPath);
  //console.log('initial path', initialPath, defaultRoute);

  return (
    <Switch>
      <Route
        exact
        path="/app/details"
        render={() => (<Redirect to={{ pathname: defaultRoute }} />)}/>
      {/*<Route path={'/app/details/appDetails'} component={ApplicationDetailsPage}></Route>*/}
      {r.map((route) => (
        <Route
          exact={!(route.id && route.id.length) || false}
          key={joinPath(basePath, route.id)}
          path={joinPath(basePath, route.id)}
          render={() =>(<ApplicationDetailsPage fireRestAction={self.fireRestAction.bind(self)}/>)}
        >
          {route.children && route.children.length > 0 &&
            AppRouter(joinPath(basePath, route.id), route.children, self)}
        </Route>
      ))}
      <Route component={NotFoundPage} />
    </Switch>
  );
}
export function AppMenu(basePath, r = routes, portalVisitedTabs, handleMenuClick, linkPath) {
  return AppMenuRecursive(basePath, r, portalVisitedTabs, handleMenuClick, linkPath);
}
export default routes;
