/*
 *
 * DetailsPage constants
 *
 */

export const DEFAULT_ACTION = 'app/DetailsPage/DEFAULT_ACTION';
export const TOGGLE_SIDEBAR_COLLAPSE = 'app/DetailsPage/TOGGLE_SIDEBAR_COLLAPSE';
export const SET_TAB_CONFIG = 'app/DetailsPage/SET_TAB_CONFIG';
export const SET_TAB_DATA = 'app/DetailsPage/SET_TAB_DATA';
export const SET_ACTIVE_TAB = 'app/DetailsPage/SET_ACTIVE_TAB';
export const SET_SCREEN_INFO = 'app/DetailsPage/SET_SCREEN_INFO';

export const returnMappedAction = {
  "message": "showMessage",
  "autoPopulate": "autoPopulateFields",
  "partialSave": "partialSave",
  "route": "changeRoute",
}
export const routeMap = {
  "LIST": '/app/listing',
  "SELECTION": '/app/selection',
}

