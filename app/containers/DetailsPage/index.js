/**
 *
 * DetailsPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { makeSelectApp, makeSelectPersistentDetailsPage } from 'containers/App/selectors';
import { makeSelectListingPage } from 'containers/ListingPage/selectors';
import { makeSelectDetailsPage } from './selectors';
import reducer from './reducer';
import saga from './saga';
import { networkAction, setOpenSubMenus, setSelectedTabs } from 'containers/App/actions';

import { Menu } from 'antd';
import { AppRouter, AppMenu } from './routes';
import { setSidebarCollapse, setTabConfig, setTabData, setActiveTab, setScreenInfo } from './actions';
import { Wrapper } from './Styles';

import GlobalDetailsViewer from 'components/GlobalDetailsViewer';

import { Layout } from 'antd';
import { openNotification } from 'components/Notification';
import { showInfoMessage } from 'components/Message';
import Modal from 'components/Modal';
import _ from 'lodash';
import { returnMappedAction, routeMap } from './constants'

import UserComments from 'components/UserComments'
import * as renderService from '../../utils/renderDataService';
import dottie from 'dottie';
import { select } from 'redux-saga/effects';

//import ApplicationDetails from 'containers/ApplicationDetails/Loadable';


const { Header, Sider, Content } = Layout;
export class DetailsPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  state = {
    messageModal: {
      componentProps: {
        title: 'Title',
        visible: false,
        closable: false,
      },
      children: '',
      openSubMenus: [],
      selectedTabs: [],
    },
  }
  componentDidMount() {
    const { record: { caseInstanceId, assignee }, roleSelected } = this.props.app;
    const { listingPage: { activeProduct }, location } = this.props;

    const reqData = {
      api: 'tabConfig',
      method: 'GET',
      params: { caseInstanceId },
      queryParams: { assignee },
      encode: false,
      successAction: this.props.setTabConfig,
      successCallback: (res) => {

        const tab = res.data[0];
        let tabKey = this.props.persistentDetailsPage.selectedTabs[0];
        if (!tabKey) {
          if (tab.childPresent) {
            this.props.setOpenSubMenus([tab.id]);
            tabKey = tab.children[0].id;
          } else {
            tabKey = tab.id;
          }
        }
        console.log("tabKey", tabKey);
        this.props.setSelectedTabs([tabKey])
        this.fetchCardData(tabKey);
      }
    };
    this.props.networkAction(reqData);
  }

  handleCollapse = (collapsed) => {
    this.props.dispatch(setSidebarCollapse(collapsed));
  }

  fetchCardData = (tabKey, callBack) => {
    const { record: { caseInstanceId, assignee }, roleSelected } = this.props.app;
    const { activeProduct, activeBucket } = this.props.listingPage;

    const cardData = {
      api: 'cardConfig',
      method: 'GET',
      params: { roleName: roleSelected.key, activeBucket, caseInstanceId, tabKey },
      queryParams: { assignee },
      encode: false,
      successCallback: (response) => {
        console.log(`SUCCESS: card data for ${caseInstanceId} ${tabKey}`, response.data);
        const tabData = renderService.buildTabData(response.data, tabKey);
        const screenInfo = response.screenInfo.split('-')[1];
        this.props.setScreenInfo(screenInfo);
        this.props.setTabData(tabData);
        this.props.setActiveTab(tabKey);
        if (callBack) callBack();
      },
      failCallback: (err) => {
        console.log("ERROR: cannot fetch card data", err);
        this.props.setTabData(null);
        this.props.setActiveTab(tabKey);
      }
    }
    this.props.networkAction(cardData);
  }

  handleMenuClick = (args, tabKey, route) => {
    console.log('menu clicked', args, route);
    this.props.history.push(route);
    this.fetchCardData(tabKey)
  }

  fireRestAction(args, formValues, cb, updateFormValues) {
    console.log('captured here', args, formValues);
    const { record: { caseInstanceId }, roleSelected: { key: roleName }, } = this.props.app;
    const { tabs: { activeTab: tabKey } } = this.props.detailsPage;
    const { id: elmId, params: { meta } } = args;
    const { restApi: { method, subUrl, queryParams, requestPayload, defaultComment, successActions = [], errorActions = [] } } = meta;

    let keys = { caseInstanceId, tabKey, roleName }, qParams = {}, data = {};
    Object.keys(requestPayload).forEach(key => {
      let nestedObj = {};
      Object.keys(requestPayload[key]).length && Object.keys(requestPayload[key]).forEach(i => {
        //console.log('=========', i, formValues[requestPayload[key][i]], keys[requestPayload[key][i]])
        nestedObj[i] = formValues[requestPayload[key][i]] || keys[requestPayload[key][i]] || requestPayload[key][i];
      })
      data[key] = nestedObj;
    })
    data.message = defaultComment;
    data.key = elmId;
    queryParams.forEach(param => {
      qParams[param] = keys[param]
    });

    const formData = {
      api: 'configApi',
      method: method || 'POST',
      params: { subUrl },
      queryParams: qParams,
      data,
      successCallback: (res) => {
        this.handleRestApiResponseActions(res.data, 'success', successActions, elmId, cb, updateFormValues, formValues)
      },
      failCallback: (err) => {
        this.handleRestApiResponseActions(err, 'error', errorActions, elmId, cb, updateFormValues, formValues)
      }
    };
    this.props.networkAction(formData);
  }

  handleRestApiResponseActions(response, type, actions, elmId, cb, updateFormValues, formValues) {
    cb && cb();

    let seqActions = actions.reduce((promiseChain, action) => {
      return promiseChain.then(() => new Promise((resolve) => {
        this.mapActions(response, action, type, resolve, updateFormValues, formValues);
      }));
    }, Promise.resolve());
  }

  showMessage(response, action, type, cb, updateFormValues, formValues) {
    const message = type === 'success' ? "Success" : "Error";
    const content = action.content;
    let args = {
      description: content,
      message,
      duration: 2.5,
      type,
      onClose: () => {
        cb();
      }
    }
    if (action.formType === 'toastr') {
      openNotification(args);
    } else {
      let messageModal = _.extend({}, this.state.messageModal);
      messageModal = {
        componentProps: {
          visible: true,
          title: message,
        },
        children: content,
        onOkHandler: () => {
          this.closeModal(cb)
        }
      }
      this.setState({ messageModal })
    }
  }

  autoPopulateFields(response, action, type, cb, updateFormValues, formValues) {
    let newForm = {}
    Object.keys(action.mapping).forEach(key => {
      newForm[key] = dottie.get(response, action.mapping[key])
    })

    let args = {
      content: 'Autopopulating process variables',
      type: 'info',
      onClose: () => {
        cb();
        updateFormValues && updateFormValues(newForm);
      }
    }
    showInfoMessage(args)
  }

  partialSave(response, action, type, cb, updateFormValues, formValues) {

    const { record: { caseInstanceId, assignee } } = this.props.app;
    let form = {}
    Object.keys(action.mapping).forEach(key => {
      if (key === 'processVariables') {
        let pVList = action.mapping[key];
        Array.isArray(pVList) && pVList.length && pVList.forEach(k => {
          form[k] = formValues[k]
        })
      } else {
        form[key] = dottie.get(response, action.mapping[key])
      }
    })
    let args = {
      content: 'Saving partial data',
      type: 'loading',
      duration: 2,
      onClose: () => {
        cb();
      }
    }
    showInfoMessage(args);

    //console.log('response and action', response, action, form);

    const formData = {
      api: 'save',
      method: 'POST',
      queryParams: { caseInstanceId },
      data: form,
      successCallback: (res) => {
        //console.log('saving card data', res);
        updateFormValues && updateFormValues(form);
      }
    };
    this.props.networkAction(formData);
  }

  changeRoute(response, action, type, cb) {
    //console.log('need to change route here', action);
    this.props.history.push(routeMap[action.page]);
    cb();
  }

  closeModal = (cb) => {
    let messageModal = _.extend({}, this.state.messageModal);
    messageModal = {
      componentProps: {
        visible: false,
        title: ""
      },
      children: "",
    }
    this.setState({ messageModal }, () => {
      cb();
    });
  }

  mapActions = (response, action, actionType, cb, updateFormValues, formValues) => {
    const { type } = action;
    let method = returnMappedAction[type];
    this[method] && this[method](response, action, actionType, cb, updateFormValues, formValues);
  };

  onSubMenuOpenChange = (openKeys) => {
    this.props.setOpenSubMenus(openKeys);
  }

  onMenuItemSelectChange = ({ item, key, keyPath, selectedKeys, domEvent }) => {
    this.props.setSelectedTabs(selectedKeys);
  }


  render() {
    //const currentPath = this.props.location.pathname;
    let { tabs: { tabConfig, tabData, activeTab, }, selectedTabs, openSubMenus } = this.props.detailsPage;
    const { record: { caseInstanceId, assignee }, roleSelected: { key: roleName }, userInfo: { username: userName } } = this.props.app;
    if (!tabConfig || !tabConfig.length || !tabData) return null;
    const portalVisitedTabs = tabData && tabData.portalVisitedTabs ? tabData.portalVisitedTabs : [];
    const accessCode = tabData.commentConfiguration;
    console.log("current path:", tabConfig);
    const siderMenu = (<Menu
      inlineIndent={0}
      theme="light"
      mode="inline"
      openKeys={this.props.persistentDetailsPage.openSubMenus}
      selectedKeys={this.props.persistentDetailsPage.selectedTabs}
      onOpenChange={this.onSubMenuOpenChange}
      onSelect={this.onMenuItemSelectChange}
    >
      {AppMenu(this.props.match.url, tabConfig, portalVisitedTabs, this.handleMenuClick, [this.props.match.url])}
    </Menu>)


    return (
      <Wrapper>
        <Layout style={{ height: "100%" }}>
          <Header>
            <GlobalDetailsViewer {...this.props} data={tabData && tabData["gloablVariables"]} fireRestAction={this.fireRestAction.bind(this)} />
          </Header>

          <Layout style={{ height: "100%" }}>
            <Sider
              breakpoint="sm"
              // collapsible
              collapsed={this.props.detailsPage.sidebar.collapse}
              onCollapse={this.handleCollapse}
            >
              {siderMenu}
            </Sider>
            <Content style={{ padding: "16px", height: "100%" }}>
              {/*AppBreadcrumb(this.props.match.url)*/}
              <Modal {...this.state.messageModal}></Modal>
              {AppRouter(this.props.match.url, tabConfig, this)}
            </Content>
          </Layout>
          <UserComments
            componentProps={{
              accessCode,
              userName,
              roleName
            }}
            fetchComments={{
              params: { caseInstanceId }
            }}
            postComment={{
              params: { roleName, caseInstanceId, tabKey: activeTab }
            }}
            tabKey={activeTab}
          />
        </Layout>
      </Wrapper>
    );
  }
}

DetailsPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  match: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  app: makeSelectApp(),
  persistentDetailsPage: makeSelectPersistentDetailsPage(),
  listingPage: makeSelectListingPage(),
  detailsPage: makeSelectDetailsPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    networkAction: (networkData) => dispatch(networkAction(networkData)),
    setTabData: (data) => dispatch(setTabData(data)),
    setScreenInfo: (data) => dispatch(setScreenInfo(data)),
    setTabConfig: (data) => dispatch(setTabConfig(data)),
    setActiveTab: (key) => dispatch(setActiveTab(key)),
    setSelectedTabs: (keys) => dispatch(setSelectedTabs(keys)),
    setOpenSubMenus: (keys) => dispatch(setOpenSubMenus(keys)),
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'detailsPage', reducer });
const withSaga = injectSaga({ key: 'detailsPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(DetailsPage);
