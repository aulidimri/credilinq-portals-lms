/*
 *
 * DetailsPage actions
 *
 */

import {
  DEFAULT_ACTION,
  TOGGLE_SIDEBAR_COLLAPSE,
  SET_TAB_CONFIG,
  SET_TAB_DATA,
  SET_ACTIVE_TAB,
  SET_SCREEN_INFO,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function setSidebarCollapse(collapse) {
  return {
    type: TOGGLE_SIDEBAR_COLLAPSE,
    collapse,
  };
}

export function setTabConfig(data) {
  return {
    type: SET_TAB_CONFIG,
    data,
  };
}

export function setTabData(data) {
  return {
    type: SET_TAB_DATA,
    data,
  };
}

export function setActiveTab(key) {
  return {
    type: SET_ACTIVE_TAB,
    key,
  };
}

export function setScreenInfo(key) {
  return {
    type: SET_SCREEN_INFO,
    key,
  }
}

// export function setComments(comment) {
//   return {
//     type: SET_COMMENTS,
//     comment,
//   };
// }


