import { createSelector } from 'reselect';

/**
 * Direct selector to the dashboardPage state domain
 */
const selectDetailsPageDomain = (state) => state.get('detailsPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by DetailsPage
 */

const makeSelectDetailsPage = () => createSelector(
  selectDetailsPageDomain,
  (substate) => substate.toJS()
);

export {
  makeSelectDetailsPage,
  selectDetailsPageDomain,
};
