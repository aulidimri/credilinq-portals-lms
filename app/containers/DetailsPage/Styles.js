import styled from 'styled-components';
import { globalColors, colors, fonts, sizes } from 'configs/styleVars';

export const Wrapper = styled.div`
  height: 100%;

  .ant-layout-header {
    height: auto;
    padding: 0;
    line-height: inherit;
    position: relative;
    border-bottom: 1px solid ${colors.secondaryBorderColor};
    &::before {
      content: '';
      box-shadow: rgb(220,220,220) 0px 1px 5px;
      width: 100%;
      position: absolute;
      top: 0;
      bottom: 0;
      pointer-events: none;
      }
  }

  .ant-layout-sider {
    background: ${globalColors.white};
    .ant-menu-inline .ant-menu-item {
      margin-top: 0;
    }
  }

`;

export const routeItemStyle = {
  display: 'flex',
  justifyContent: 'space-between',
  paddingLeft: 15,
};

export const labelItemStyle = {
  width: 'calc(100% - 10px)',
  overflow: 'hidden',
  textOverflow: 'ellipsis',
  whiteSpace: 'nowrap'
}
