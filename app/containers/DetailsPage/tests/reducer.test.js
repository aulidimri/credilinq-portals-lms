
import { fromJS } from 'immutable';
import detailsPageReducer from '../reducer';

describe('detailsPageReducer', () => {
  it('returns the initial state', () => {
    expect(detailsPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
