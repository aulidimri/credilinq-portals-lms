import moment from 'moment';
import { call, put, /* select, */ takeLatest, takeEvery, select } from 'redux-saga/effects';
import { push } from 'connected-react-router/immutable';
import { LOGIN_REQUEST, LOGOUT, LOGIN_PATH, LOCATION_CHANGE, NETWORK_SAGA } from './constants';
import { config } from 'utils/config';
import request from 'utils/request';
import { makeSelectAuth } from './selectors';
// import * as appActions from './actions';
import { loginSuccess, loginFail } from './actions';
import { incrementLoader, decrementLoader } from 'containers/AppLayout/actions';
import qs from 'query-string';
import { openNotification } from 'components/Notification';
import dottie from 'dottie';


// Individual exports for testing
export function* login(loginData) {
  const { data, api, params, queryParams, method } = loginData.data;
  //console.log('in login', data, api, method);
  try {
    const result = yield call(request, config.getApiUrl(api, null, queryParams), null, data, method);
    if(result.status === 200){
      yield put(loginSuccess(result.data));
    } else {
      //result.error.response.data
      //result.data.error_description
      // TODO: BE to return generic responses on api failure
      let error = dottie.get(result, 'data.error_description') || dottie.get(result, 'message');
      console.log('result is', result, error);
      yield put(loginFail(error));
    }
  } catch (error) {
    console.log(error);
    yield put(loginFail(error));
  }
}
export function* logoutRedirect() {
  yield put(push(LOGIN_PATH));
}

export function* navigationSaga(action) {
  let params = '';
  if (action.routingData && action.routingData.params && Object.keys(action.routingData.params).length) {
    params += '?' + qs.stringify(action.routingData.params);
  }

  yield put(push({
    pathname: action.routingData.url,
    search: params,
  }));

  // yield put(appActions.setAppDetails(action.routingData));
}

export function* networkSaga(action) {
  //console.log("In network saga");
  const { data, successAction, successCallback, failCallback, failAction, api, params, queryParams, loader, method, encode } = action.dataObject

  if (loader) {
    yield put(incrementLoader());
  }

  let response = {};

  const sendingMethod = method || (data ? 'POST' : 'GET');

  const authSelector = yield select(makeSelectAuth());

  const authorization = authSelector.token;
  // if(api === 'configApi'){
  //   console.log('url, payload, method, dataObj', config.getApiUrl(api, params || null, queryParams || null, encode), data, sendingMethod, action.dataObject);
  //   //return;
  // }
  response = yield call(request, config.getApiUrl(api, params || null, queryParams || null, encode), authorization, data, sendingMethod, action.dataObject);
  //console.log('response from saga', response);
  if (response && response.status === 200) {   //  TODO - better handeling of document API
      if (successAction) {
          yield put(successAction(response.data));
      }
      //console.log("ACTION DONE");
      if (successCallback) {
          yield call(successCallback, response);
      }
  } else {
      console.log('API FAIL : ', response);
      if (response.status === 401) {
          //Take care of refresh token and issue same call again

          const refresh_token = authSelector.refresh_token;
          if (!refresh_token) {
            yield put(push('/auth'));
            return
          }
          let refreshTokenResponse = {};
          const refreshTokenBody = { refresh_token };
          //console.log('SENDING REFRESH TOKEN DATA : ', refreshTokenBody);
          refreshTokenResponse = yield call(request, config.getApiUrl('refreshToken'), null, refreshTokenBody, 'POST');
          //console.log('RESPONSE REFRESH TOKEN DATA : ', refreshTokenResponse);
          if (refreshTokenResponse && refreshTokenResponse.status === 200) {
            yield put(loginSuccess(refreshTokenResponse.data));
            yield * networkSaga(action);
          } else {
              console.log('REFRESH TOKEN ERROR');
          }
      } else {
          if (failCallback) {
              //console.log('INSIDE FAIL CALL BACK');
              yield call(failCallback, response);
          }
          if (failAction) {
              //console.log('failAction getting called from failAction');
              yield put(failAction(response.data));
          }
          // TODO: Need to call only when there is no failCallback defined for api call
          let args = {
            description: response.message || 'Something went wrong',
            message: 'Error',
            duration: 4.5,
            type: 'error',
          }
          openNotification(args);
      }
      //Need to integrate notification here;
  }

  if (loader) {
    yield put(decrementLoader());
  }
}

export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(LOGIN_REQUEST, login);
  yield takeLatest(LOGOUT, logoutRedirect);
  yield takeLatest(LOCATION_CHANGE, navigationSaga);
  yield takeEvery(NETWORK_SAGA, networkSaga);
}
