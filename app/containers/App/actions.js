/*
 *
 * App actions
 *
 */

import {
  DEFAULT_ACTION,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT,
  LOGOUT_SUCCESS,
  LOGOUT_FAIL,
  LOCATION_CHANGE,
  NETWORK_SAGA,
  SELECT_ROLE,
  SET_RECORD,
  SET_OPEN_SUB_MENUS,
  SET_SELECTED_TABS,
  REFRESH_DETAILS_PAGE,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}
export function login(data) {
  return {
    type: LOGIN_REQUEST,
    data,
  };
}
export function loginSuccess(data) {
  return {
    type: LOGIN_SUCCESS,
    data,
  };
}
export function loginFail(error) {
  return {
    type: LOGIN_FAIL,
    error,
  };
}

export function logout() {
  return {
    type: LOGOUT,
  };
}
export function logoutSuccess() {
  return {
    type: LOGOUT_SUCCESS,
  };
}

export function logoutFail() {
  return {
    type: LOGOUT_FAIL,
  };
}

export function networkAction(dataObject) {
  return {
    type: NETWORK_SAGA,
    dataObject,
  };
}

export function routeChange(routingData) {
  return {
    type: LOCATION_CHANGE,
    routingData
  };
}

export function selectRole(role) {
  return {
    type: SELECT_ROLE,
    role,
  };
}

export function setRecord(record) {
  return {
    type: SET_RECORD,
    record,
  };
}

export function setSelectedTabs(keys) {
  return {
    type: SET_SELECTED_TABS,
    keys,
  }
}

export function setOpenSubMenus(keys) {
  return {
    type: SET_OPEN_SUB_MENUS,
    keys,
  }
}

export function refreshDetailsPage() {
  return {
    type: REFRESH_DETAILS_PAGE,
  }
}
