import { createSelector } from 'reselect';

/**
 * Direct selector to the app state domain
 */
const selectAppDomain = (state) => state.get('app');

/**
 * Other specific selectors
 */


/**
 * Default selector used by App
 */

const makeSelectApp = () => createSelector(
  selectAppDomain,
  (substate) => substate.toJS()
);

const makeSelectAuth = () => createSelector(
  selectAppDomain,
  (substate) => substate.get('auth').toJS()
);

const makeSelectRoles = () => createSelector(
  selectAppDomain,
  (substate) => substate.get('roles')
);

const makeSelectRoleSelected = () => createSelector(
  selectAppDomain,
  (substate) => substate.get('roleSelected')
);

const makeSelectPersistentDetailsPage = () => createSelector(
  selectAppDomain,
  (substate) => substate.get('detailsPage').toJS()
);

export {
  makeSelectApp,
  selectAppDomain,
  makeSelectAuth,
  makeSelectRoles,
  makeSelectRoleSelected,
  makeSelectPersistentDetailsPage,
};
