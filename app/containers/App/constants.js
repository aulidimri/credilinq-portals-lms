/*
 *
 * App constants
 *
 */

export const DEFAULT_LOCALE = 'en';
export const DEFAULT_ACTION = 'app/App/DEFAULT_ACTION';
export const DEFAULT_REDIRECT_PATH = '/app';
export const LOGIN_PATH = '/login';

export const LOGIN_REQUEST = 'app/App/LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'app/App/LOGIN_SUCCESS';
export const LOGIN_FAIL = 'app/App/LOGIN_FAIL';

export const LOGOUT = 'app/App/LOGOUT';

export const LOCATION_CHANGE = 'app/App/LOCATION_CHANGE';
export const LOGOUT_REQUEST = 'app/App/LOGOUT_REQUEST';
export const LOGOUT_SUCCESS = 'app/App/LOGOUT_SUCCESS';
export const LOGOUT_FAIL = 'app/App/LOGOUT_FAIL';

export const NETWORK_SAGA = 'app/App/NETWORK_SAGA';

export const SELECT_ROLE = 'app/App/SELECT_ROLE';
export const SET_RECORD = 'app/App/SET_RECORD';

export const SET_OPEN_SUB_MENUS = 'app/DetailsPage/SET_OPEN_SUB_MENUS';
export const SET_SELECTED_TABS = 'app/DetailsPage/SET_SELECTED_TABS';
export const REFRESH_DETAILS_PAGE = 'app/DetailsPage/REFRESH_DETAILS_PAGE';
