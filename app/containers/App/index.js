/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
// import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Switch, Route, withRouter, Redirect } from 'react-router-dom';
import { AnimatedSwitch } from 'react-router-transition';
import { ConnectedRouter } from 'connected-react-router/immutable';

import AuthenticatedRoute from 'containers/AuthenticatedRoute/Loadable';
// import HomeLayout from 'containers/HomeLayout/Loadable';
import AppLayout from 'containers/AppLayout/Loadable';
import LoginPage from 'containers/LoginPage/Loadable';
import ErrorPage from 'containers/ErrorPage/Loadable';
import { LOGIN_PATH } from 'containers/App/constants';
import { ERR_NOT_FOUND, ERR_FORBIDDEN } from 'containers/ErrorPage/constants';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { makeSelectApp } from './selectors';
import reducer from './reducer';
import saga from './saga';
import { Wrapper } from './Styles';
// import messages from './messages';

const ForbiddenPage = (props) => (
  <ErrorPage error={ERR_FORBIDDEN} {...props}></ErrorPage>
);
const NotFoundPage = (props) => (
  <ErrorPage error={ERR_NOT_FOUND} {...props}></ErrorPage>
);

export class App extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  /* constructor (props) {
    super(props)
  } */

  render() {
    return (
      <Wrapper>
        <Helmet>
          <title>Lend.in | Portals</title>
          <meta name="lms" content="Lend.in | Portals" />
        </Helmet>
        <ConnectedRouter history={this.props.history}>
          <Switch
            atEnter={{ opacity: 0 }}
            atLeave={{ opacity: 0 }}
            atActive={{ opacity: 1 }}
            className="switch-wrapper">
            <AuthenticatedRoute path="/app" component={AppLayout}></AuthenticatedRoute>
            <Route
              exact
              path="/"
              render={() => (<Redirect to={{ pathname: LOGIN_PATH }} />)}
            />
            <Route exact path={LOGIN_PATH} component={LoginPage}></Route>
            <Route exact path="/forbidden" component={ForbiddenPage}></Route>
            <Route component={NotFoundPage}></Route>
          </Switch>
        </ConnectedRouter>
      </Wrapper>
    );
  }
}

App.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  history: PropTypes.object,
  // location: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  app: makeSelectApp(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'app', reducer });
const withSaga = injectSaga({ key: 'app', saga });
export default compose(
  withReducer,
  withConnect,
  withSaga,
  withRouter,
)(App);
