/*
 *
 * App reducer
 *
 */

import { fromJS } from 'immutable';
import { LOCATION_CHANGE } from 'connected-react-router/immutable';
import {
  LOGIN_REQUEST,
  LOGIN_FAIL,
  LOGIN_SUCCESS,
  LOGOUT,
  SELECT_ROLE,
  SET_RECORD,
  SET_OPEN_SUB_MENUS,
  SET_SELECTED_TABS,
  REFRESH_DETAILS_PAGE,
} from './constants';
const initialState = fromJS({
  auth: {
    token: false,
    expires: false,
    loading: false,
    error: false,
    refresh_token: false,
  },
  roles: [],
  roleSelected: null,
  record: null,
  userInfo: {
    username: null,
  },
  userOptions: [
    {
      icon: 'bell',
      key: 'notification',
      value: '',
    },
    {
      icon: 'edit',
      key: 'role',
      value: '',
    },
  ],
  detailsPage: {
    selectedTabs: [],
    openSubMenus: [],
  }
  //productsByRole : null,
});

function appReducer(state = initialState, action) {
  switch (action.type) {

    case LOCATION_CHANGE:
      return state.setIn(['auth', 'error'], false).setIn(['auth', 'loading'], false);

    case LOGIN_REQUEST:
      return state.setIn(['auth', 'error'], false).setIn(['auth', 'loading'], true);

    case LOGIN_SUCCESS:
      let roles = action.data.groups.map(group => {
        return { key: group['realmRoles'][0], label: group['name'], active: false, id: group['id'] };
      });
      return state
        .setIn(['auth', 'loading'], false)
        .setIn(['auth', 'token'], action.data.access_token)
        .setIn(['auth', 'refresh_token'], action.data.refresh_token)
        .setIn(['auth', 'expires'], action.data.expires_in)
        .setIn(['auth', 'error'], false)
        .set('roles', roles)
        .setIn(['userInfo', 'username'], action.data.username)
    // Will set expire once backend implements auth
    //.setIn(['auth', 'expire'], action.result.expire);

    case LOGIN_FAIL:
      return state
        .setIn(['auth', 'loading'], false)
        .setIn(['auth', 'error'], action.error)
        .set('roles', [])
        .set('roleSelected', null)

    case LOGOUT:
      return state
        .setIn(['auth', 'token'], false)
        .setIn(['auth', 'expires'], false)
        .setIn(['auth', 'refresh_token'], false)
        .setIn(['auth', 'error'], false)
        .set('roles', [])
        .set('roleSelected', null);

    case SELECT_ROLE:
      roles = state.toJS().roles;
      let userOptions = state.toJS().userOptions;
      userOptions[1].value = action.role.label;

      roles.forEach(role => {
        role.active = role.key === action.role.key
      })
      return state
        .set('roles', roles)
        .set('roleSelected', action.role)
        .set('userOptions', userOptions)

    case SET_RECORD:
      return state
        .set('record', action.record);

    case SET_SELECTED_TABS:
      return state
        .setIn(['detailsPage', 'selectedTabs'], action.keys)

    case SET_OPEN_SUB_MENUS:
      return state
        .setIn(['detailsPage', 'openSubMenus'], action.keys);

    case REFRESH_DETAILS_PAGE:
      return state
        .setIn(['detailsPage', 'selectedTabs'], [])
        .setIn(['detailsPage', 'openSubMenus'], [])

    default:
      return state;
  }
}

export default appReducer;
