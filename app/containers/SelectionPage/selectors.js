import { createSelector } from 'reselect';

/**
 * Direct selector to the dashboardPage state domain
 */
const selectSelectionPageDomain = (state) => state.get('selectionPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by SelectionPage
 */

const makeSelectSelectionPage = () => createSelector(
  selectSelectionPageDomain,
  (substate) => substate.toJS()
);

export {
  makeSelectSelectionPage,
  selectSelectionPageDomain,
};
