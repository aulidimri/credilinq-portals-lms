
import { fromJS } from 'immutable';
import dashboardPageReducer from '../reducer';

describe('selectionPageReducer', () => {
  it('returns the initial state', () => {
    expect(selectionPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
