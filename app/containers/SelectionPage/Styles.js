import styled from 'styled-components';
import { globalColors, colors, sizes } from 'configs/styleVars';

export const Wrapper = styled.div`
  height: 100%;
  background-color: ${colors.primaryBodyBG};

  .ant-list-item.roleListItem {
    margin: 15px 0;
    padding: ${sizes.paddingS};
    border: 1px solid ${colors.primaryBorderColor};
    background-color: ${colors.secondaryRowColor};
    cursor: pointer;

    //TODO Lets use border-color instead of this
    &:hover {
      color: ${colors.primaryHoverBorderColor};
      border-color: ${colors.primaryHoverBorderColor};

    }

    &.active {
      color: ${globalColors.white};
      background-color: ${globalColors.primaryColor};
      border-color: ${colors.primaryActiveBorderColor};
    }
  }

  #logo {
    max-width: 200px;
  }

  img {
    margin: 40px 0;
  }
`
