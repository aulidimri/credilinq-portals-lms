/**
 *
 * SelectionPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import injectReducer from 'utils/injectReducer';

import { makeSelectRoles, makeSelectRoleSelected } from 'containers/App/selectors';

import Icon from 'components/Icon';
import { brand } from 'utils/constants';
import Button from 'components/Button';
import { selectRole } from 'containers/App/actions';
import { resetProductsPage } from 'containers/ListingPage/actions';

import Row from 'components/Row';
import Col from 'components/Col';
import List from 'components/List';
import ListItem from 'components/ListItem';
import { Wrapper } from './Styles';
import reducer from './reducer';
import { makeSelectSelectionPage } from './selectors';

export class SelectionPage extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {
      active: ''
    }
    this.onSelect = this.onSelect.bind(this);
  }

  componentDidMount() {
    this.props.resetProductsPage();
  }

  onSelect = item => {
    //console.log('item is', item);
    this.props.selectRole(item);
    this.props.history.push('/app/listing');
  };

  // onRoleSubmit = () => {
  //   console.log('Selected Role', this.props.roleSelected);
  //   this.props.history.push('/app/listing');
  // }

  render() {
    const { roles, roleSelected } = this.props;
    //console.log('groups in api response', roles, roleSelected);
    if(!roles.length) return <div>Roles List Empty</div>
    const renderListItem = item => (
      <ListItem
        componentProps={{
          onClick: () => this.onSelect(item),
          className: `roleListItem ${item.active ? 'active' : ''}`
        }}
      >
        <Icon
          componentProps={{
            type: 'caret-right',
            label: item.label,
          }}
        />
      </ListItem>
    );

    return (
      <Wrapper>
        <Row>
          <Col
            componentProps={{
              xs: { span: 16, offset: 4 },
              md: { span: 8, offset: 8 },
              sm: { span: 12, offset: 6 },
            }}
          >
            <img src={brand.logo} alt="logo" id="logo" />
            {/*<h1>Hi Maninder,</h1>*/}
            <h2>Please select your role</h2>
            <List
              componentProps={{
                size: 'large',
                dataSource: roles,
                renderItem: renderListItem,
              }}
            />
            {/*<Button
              componentProps={{label: 'Continue', type: 'primary', disabled: !roleSelected}}
              onClickHandler={this.onRoleSubmit}
            />*/}
          </Col>
        </Row>
      </Wrapper>
    );
  }
}

SelectionPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  match: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  selectionPage: makeSelectSelectionPage(),
  roles: makeSelectRoles(),
  roleSelected: makeSelectRoleSelected(),
});

function mapDispatchToProps(dispatch) {
  return {
    selectRole: role => dispatch(selectRole(role)),
    resetProductsPage: () => dispatch(resetProductsPage()),
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);
const withReducer = injectReducer({ key: 'selectionPage', reducer });

export default compose(
  withReducer,
  withConnect,
)(SelectionPage);
