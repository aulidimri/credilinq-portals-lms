import styled from 'styled-components';

//TODO Got to use styleVars in this file

export const Wrapper = styled.div`
  section {
    width: 100%;
    display: flex;
    flex-direction: column;

    &.login-bg {
      justify-content: space-around;
      display: flex;
      align-items: center;

      div {
        width: 60%;
        img {
          width: 100%;
        }
      }
    }
  }

  h1 {
    font-size: 25px;
    font-weight: 500;
    margin: 2rem 0;
  }

  .subContainer {
    display: flex;
    height: 100vh;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    padding: 2rem;
  }

  #logo {
    max-width: 200px;
  }

  .ant-form {
    width: 60%;
  }

  .ant-form-item:last-of-type .ant-form-item-control {
    display: flex;
    justify-content: flex-end;
  }

  //TODO Lets not restrict the width of the button
  .ant-form-item:last-of-type .ant-btn {
    width: 100px;
  }

  p,
  label {
    font-family: inherit;
  }

  //TODO Lets use size api in ant to handle this
  .ant-input {
    height: 45px;
  }

  .alert-box {
    margin-bottom: 2rem;
    padding: 0.8rem 1.5rem;

    .ant-alert-close-icon {
      top: 8px;
    }
  }
`;
