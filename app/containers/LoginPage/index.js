/**
 *
 * LoginPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

//TODO Use our own components instead of using ant's
import { Form, Input, Button, Alert, Row, Col } from 'antd';
import { Redirect } from 'react-router-dom';

import { login } from 'containers/App/actions';
import { DEFAULT_REDIRECT_PATH } from 'containers/App/constants';
import { makeSelectAuth } from 'containers/App/selectors';
import coverImage from 'images/bg.png';
import { brand } from 'utils/constants';

import { Wrapper } from './Styles';

const FormItem = Form.Item;
export class LoginPage extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const loginData = {
          api: 'login',
          method: 'POST',
          data: values,
        };
        this.props.login(loginData);
      }
    });
  }

  render() {
    const { from } = this.props.location.state || {
      from: { pathname: DEFAULT_REDIRECT_PATH },
    };
    if (this.props.auth.token) {
      // is authenticated
      // return <Redirect to={from} />;
      return <Redirect to={'/app/selection'} />;

    }

    return (
      <Wrapper>
        <Row>
          <Col span={16} className="subContainer">
            <section className="login-bg">
              <div>
                <img id="coverImage" alt="coverImage" src={coverImage} />
              </div>
            </section>
          </Col>
          <Col span={8} className="subContainer">
            <section>
              <img id="logo" alt="logo" src={brand.logo} />
              <h1>Login to your account</h1>
              <Form
                onSubmit={this.handleSubmit}
                hideRequiredMark
                colon={false}
                layout="vertical"
              >
                {this.props.auth.error && (
                    <Alert
                      className='alert-box'
                      description={this.props.auth.error}
                      type="error"
                      closable
                    />
                  )}
                <FormItem label="User Name or Email Address">
                  {this.props.form.getFieldDecorator('email', {
                    rules: [
                      { required: true, message: 'Please input your email!' },
                    ],
                  })(<Input placeholder="User Name or Email Address" />)}
                </FormItem>
                <FormItem label="Password">
                  {this.props.form.getFieldDecorator('password', {
                    rules: [
                      {
                        required: true,
                        message: 'Please input your Password!',
                      },
                    ],
                  })(<Input type="password" placeholder="Password" />)}
                </FormItem>
                <FormItem>
                  <Button
                    type="primary"
                    htmlType="submit"
                    loading={this.props.auth.loading}
                  >
                    Login
                  </Button>
                </FormItem>
              </Form>
            </section>
          </Col>
        </Row>
      </Wrapper>
    );
  }
}

LoginPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  form: PropTypes.object,
  auth: PropTypes.object,
  location: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  auth: makeSelectAuth(),
});

function mapDispatchToProps(dispatch) {
  return {
    login: loginData => dispatch(login(loginData)),
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  Form.create(),
)(LoginPage);
