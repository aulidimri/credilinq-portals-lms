/**
 *
 * ApplicationDetailsPage
 *
 */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectReducer from 'utils/injectReducer';
import makeSelectApplicationDetailsPage from './selectors';
import { makeSelectDetailsPage } from 'containers/DetailsPage/selectors';
import { makeSelectApp } from 'containers/App/selectors';

import reducer from './reducer';

import propsMap from 'configs/propsMapByFormType';
import componentMap from 'configs/componentMapByFormType';
import FormItem from 'components/FormItem';
import Form from 'components/Form';
import { layoutConfig } from 'utils/config';
import Button from 'components/Button';
//import UserComments from 'components/UserComments';
import Row from 'components/Row';
import Col from 'components/Col';
import { Wrapper } from './Styles';
import { networkAction } from 'containers/App/actions';

import { getMutatedFormType, getFieldOrder } from 'utils/apiUtility';
import * as renderService from '../../utils/renderDataService';
import { setTabData } from '../DetailsPage/actions';
import uuid from 'uuid';
import dottie from 'dottie';
/* eslint-disable react/prefer-stateless-function */

//BUG: This component is getting mounted everytime on active tab change!! but not on subtab change
export class ApplicationDetailsPage extends PureComponent {

  state = {
    caseInstanceId: '',
    fieldMap: {},
    editMode: {},
    formValues: {},
    activeTab: "",
    submitState: {

    }
  }

  static propTypes = {
    dispatch: PropTypes.func.isRequired,
  };

  componentDidMount() {
    //console.log("Mounted!!!!");
  }

  //Runs before render, use this instead of componentWillReceiveProps
  static getDerivedStateFromProps(nextProps, prevState) {
    const { app: { record: { caseInstanceId } }, detailsPage: { tabs: { activeTab, tabData } } } = nextProps;

    //console.log('************', prevState, caseInstanceId)
    if (!activeTab || !tabData || !Object.keys(tabData).length || Object.keys(tabData).indexOf(activeTab) < 0 || (activeTab === prevState.activeTab && tabData.caseInstanceId === prevState.caseInstanceId))
      return null;
    //console.log('&&&&&&&does it get fired');
    //Tab changed!! clear the previous active tab form fields in the state and fill it with the new ones
    const renderData = tabData[activeTab];
    const newFieldIds = Object.keys(renderData);
    const res = {};
    newFieldIds.forEach((newFieldId) => {
      let value = dottie.get(renderData, `${newFieldId}.value`) || dottie.get(renderData, `${newFieldId}.params.meta.default`);
      let formType = dottie.get(renderData, `${newFieldId}.params.meta.form_type`)
      if (formType === "upload") {  //TODO: Evaluate the need to make a api value parser for each component
        value = typeof (value) === "string" ? JSON.parse(value) : value;
        value = Array.isArray(value) && value.map(file => {
          return {
            uid: uuid(),
            name: file.label,
            response: file,
            status: "done",
            //url:
            //type:
          }
        }) || [];
      }
      res[newFieldId] = value;
    });
    return { activeTab: activeTab, formValues: res, caseInstanceId: tabData.caseInstanceId }
  }

  onChangeHandler = (value, elmId, renderData) => {
    console.log('on change in ', value, elmId, renderData);
    const formValues = this.updateFormValues(value, elmId);

    this.setState({
      formValues: {
        ...formValues,
        [elmId]: value,
      }
    });
  }

  updateMultipleField = (valueSet) => {
    /*
    * Enable's the update of multiple fields in the state at once.
    * */
    this.setState({
      formValues: {
        ...this.state.formValues,
        ...valueSet,
      },
    });
  };


  onBlurHandler = (value, elmId, renderData) => {
    const { params: { meta } } = renderData;
    const { formValues, editMode } = this.state;

    if (meta.restApi && Object.keys(meta.restApi).length) {
      this.props.fireRestAction(renderData, formValues, null, (updatedFormValues) => {
        this.setState({
          formValues: { ...formValues, ...updatedFormValues }
        })
      })
      return;
    }
  }

  /*
    This function updates formValues in state and tabData,

  */
  updateFormValues = (value, elmId) => {

    const { detailsPage: { tabs: { activeTab, tabData } } } = this.props;
    const tabFieldMap = tabData[activeTab];

    const formValues = this.state.formValues;
    let updatedFormValues;
    let updatedData;
    const { params: { meta } } = tabFieldMap[elmId];
    // const meta = tabFieldMap[elmId] && tabFieldMap[elmId].params.meta;
    if (meta && meta['display-on-selection']) {
      updatedData = renderService.setHiddenOnSelection(tabFieldMap, elmId, { ...formValues, [elmId]: value });
      this.props.setTabData({ ...tabData, [activeTab]: updatedData.renderData });
      updatedFormValues = { ...updatedData.formValues, [elmId]: value };
    } else {
      // send renderData and formData
      updatedFormValues = { ...formValues, [elmId]: value };
    }
    return updatedFormValues;
  };


  editCard = (cardID) => {
    this.setState({
      editMode: {
        ...this.state.editMode,
        [cardID]: true,
      },
    })
  }

  onClickHandler = (args, formType) => {
    console.log('onCLick in ', args, formType, this.state.formValues);
    const { formValues } = this.state;
    const { id: elmId, params: { meta } } = args;

    if (meta.restApi && Object.keys(meta.restApi).length) {

      this.setState({
        submitState: {
          ...this.state.submitState,
          [elmId]: true,
        }
      });
      this.props.fireRestAction(args, formValues, () => {
        this.setState({
          submitState: {
            ...this.state.submitState,
            [elmId]: false,
          }
        });
      }, (updatedFormValues) => {
        this.setState({
          formValues: { ...formValues, ...updatedFormValues }
        })
      })

    } else {
      this.onChangeHandler('triggered', elmId, null);
    }
  }

  saveCard = (cardID, fieldIds) => {
    console.log(fieldIds);
    //Logic to send data to API using values corresponding to the fieldIds in the state
    const { record: { caseInstanceId, assignee } } = this.props.app;
    const { formValues, editMode } = this.state;
    const { tabs: { activeTab, tabData } } = this.props.detailsPage;
    const renderData = tabData[activeTab];

    let form = {}
    fieldIds.forEach(fieldId => {
      let value = formValues[fieldId];
      let formType = dottie.get(renderData, `${fieldId}.params.meta.form_type`);
      let type = dottie.get(renderData, `${fieldId}.params.meta.type`)
      if (formType === "upload") {
        value = value.filter(file => file.status === "done");
        value = JSON.stringify(value.map(file => file.response));
      }
      console.log('typetype',type)
      if( type === 'action'){
        value = '';
      }
      console.log(fieldId, value);
      form[fieldId] = value;
    })
    console.log('save card', formValues, form);

    const formData = {
      api: 'save',
      method: 'POST',
      queryParams: { caseInstanceId },
      data: form,
      successCallback: (res) => {
        //console.log('saving card data', res);
        this.setState({
          editMode: {
            ...editMode,
            [cardID]: false,
          }
        })
      }
    };
    this.props.networkAction(formData);
  }

  getLabels = (str) => {
    return str
      // insert a space before all caps
      .replace(/([A-Z])/g, ' $1')
      // uppercase the first character
      .replace(/^./, (str) => str.toUpperCase())
  }

  getExtraProps = (elmId, renderData, formType, tabData, childrenBlock, isExpression) => {
    const { record: { caseInstanceId }, roleSelected: { key: roleName }, } = this.props.app;
    const { tabs: { activeTab: tabKey } } = this.props.detailsPage;
    const meta = renderData && renderData.params.meta;
    let extraProps = {}
    switch (formType) {
      case "card":
        let { children } = renderData.params.meta;
        extraProps = {
          extra: (
            <div className={"cardActions"}>
              {
                this.state.editMode[elmId] ?
                  <Button componentProps={{ label: "Save", size: "small" }} onClickHandler={e => {
                    e.stopPropagation();
                    this.saveCard(elmId, meta.children)
                  }} />
                  : <Button componentProps={{ label: "Edit", size: "small" }} onClickHandler={e => {
                    e.stopPropagation();
                    this.editCard(elmId)
                  }} />
              }
            </div>
          ),
          fullWidth: Array.isArray(children) && children.length === 1 && tabData[children[0]].params.meta.form_type === 'editableTable'
        };
        break;
      case "rest-dropdown":
        extraProps = {
          keys: { caseInstanceId, roleName, tabKey }
        }
        break;

      case "readOnlyTable":
        // let dependentFields;
        let { dependentFields } = meta;
        let headers = [], headings = [];
        if (dependentFields) {
          headers = tabData[dependentFields[0]].value;
          headings = (headers || []).map(field => {
            return {
              title: field.label,
              dataIndex: field.key,
            }

          })
        } else {
          headers = renderData.value[0];
          headings = (Object.keys(headers) || []).map(key => {
            return {
              title: this.getLabels(key),
              dataIndex: key
            }
          })
        }
        extraProps = {
          headings,
          pagination: false,
        }
        break;

      case "dropdown":
      case "multiselect-dropdown":
        extraProps = {
          options: Array.isArray(renderData.enumValues) && renderData.enumValues.map(({ id, name }) => ({ id: id, label: name, })) || [],
        }
        break;

      case "upload":
        extraProps = {
          keys: { caseInstanceId, tabKey, roleName }
        }
        break;

      case "string":
        extraProps = {
          className: "ant-form-text",
        }
        break;

      case 'editableTable':
        extraProps = {
          tabData,
        }
        break;

      case 'droplist':
      case 'rest-droplist':
        const createOptions = option => {
          if (Array.isArray(option.enumValues)) {
            return {
              data: option.enumValues.map(createOptions),
              label: option.name,
            };
          }
          return {
            label: option.name,
            onClickParams: [option.id,],
            onClickHandler: this.onClickHandler,
          }
        };
        extraProps = {
          data: Array.isArray(renderData.enumValues) && renderData.enumValues.map(createOptions) || [],
          keys: { caseInstanceId, roleName, tabKey },
        }
        break;
      case 'modal':
        extraProps = {
          formValues: this.state.formValues,
          tabData,
          updateMultipleField: this.updateMultipleField,
          value: this.state.formValues[elmId],
        };
        break;
      default:
        extraProps = {};
    }
    return {
      ...extraProps,
      ...(isExpression ? { formValues: this.state.formValues } : {}),
    };
  }

  renderComponent = (fieldOrder, containerID, parentConfig) => {
    //const renderData = { ...this.props.renderData },
    const { formValues } = this.state;
    const { detailsPage: { tabs: { activeTab, tabData, screenInfo } } } = this.props;
    const renderData = tabData[activeTab], renderRows = [], hiddenFields = [], actionFields = [];
    let currentColWidth = 0, renderElms = [];

    //If form field in a container, check if its in editMode and if not contained in container, automatically change form-field to non-editable
    const readMode = !containerID || containerID && !this.state.editMode[containerID] && parentConfig.readMode;

    for (let index = 0; index < fieldOrder.length; ++index) {
      const elmId = fieldOrder[index]
      if (!renderData[elmId] || this.fieldMap[elmId])
        continue;

      this.fieldMap[elmId] = true;
      const fieldRenderData = renderData[elmId];
      const { params: { meta } } = fieldRenderData;

      // const meta = renderData[elmId].params && renderData[elmId].params.meta;
      const formType = getMutatedFormType(meta.form_type, readMode);

      const isHidden = fieldRenderData.isHidden === undefined ? false || formType === 'hidden' : fieldRenderData.isHidden;
      const isAction = meta.type === 'action';
      const hasChildren = meta.children && meta.children.length;
      const childrenBlock = hasChildren && this.renderComponent(
        meta.children,
        elmId,
        {
          readMode,
        },
      );
      const value = formValues[elmId];


      let isExpression = !!meta.expression;
      let extraProps = this.getExtraProps(elmId, renderData[elmId], formType, renderData, childrenBlock, isExpression);
      let compProps = propsMap[formType] && propsMap[formType](renderData[elmId], elmId, value, readMode) || {};
      if (containerID && renderData[containerID].params.meta.form_type === 'tab-actions') {
        compProps.disabled = false;
      }
      if (Array.isArray(meta.dependentKeys) && meta.dependentKeys.length) {
        compProps.disabled = readMode ? compProps.disabled : meta.dependentKeys.some(key => !formValues[key])
      }


      if (meta.restApi && formType === 'button') {
        compProps.loading = this.state.submitState[elmId] || false;
      }
      let UnitComponent = componentMap[isExpression ? 'expression' : formType];

      if (!UnitComponent)
        continue;

      // if( meta.dummyField  || isHidden ) {
      //   // console.log("Skipping field %c"+elmId+"%c for outer container as its a dummyField.","color: #f8b017;","");
      //   continue;
      // }

      const rowsBlock = childrenBlock && childrenBlock.renderRows ? childrenBlock.renderRows : null;
      const hiddenBlock = childrenBlock && childrenBlock.hiddenFields ? childrenBlock.hiddenFields : null;
      const unitComponent = (<UnitComponent
        componentProps={{ ...compProps, ...extraProps }}
        key={elmId}
        elmId={elmId}
        renderData={renderData[elmId]}
        onChangeHandler={(value) => { this.onChangeHandler(value, elmId, renderData[elmId]); }}
        onClickHandler={(e, params) => { this.onClickHandler(renderData[elmId], formType); }}
        onBlurHandler={(value) => { this.onBlurHandler(value, elmId, renderData[elmId]) }}
      >
        {rowsBlock}
      </UnitComponent>)



      let colWidth = (layoutConfig.defaultColumnWidth || meta.colWidth) * 2;
      const offset = meta.offset || '';
      //let colSize = colWidth * layoutConfig.defaultColumnSizeForMobile * 0.5;
      const exclusiveTypes = ['card', 'editableTable', 'tab-actions', 'readOnlyTable', 'heading', 'modal'];

      if (hasChildren || exclusiveTypes.indexOf(formType) > -1) {
        colWidth = 24;
        //colSize = 90;
      }
      const formItemLayout = {
        labelCol: { span: 12 },
        wrapperCol: { span: 10 },
      }
      let componentSection = (exclusiveTypes.indexOf(formType) < 0 && !isAction) ? (
        <FormItem
          label={renderData[elmId].name}
          tip={meta.helpText}
          key={elmId}
          componentProps={{ required: meta.required, help: this.state.editMode[containerID] && meta.hintText, ...formItemLayout }}
        >
          {unitComponent}
        </FormItem>
      ) : unitComponent;

      const compWrapper = !isAction ?
        (<Col componentProps={{ sm: { span: colWidth }, xs: { span: 24 } }} key={'compwrapper-' + elmId}>{componentSection}</Col>)
        : (<Col key={'compwrapper-' + elmId}>{componentSection}</Col>);
      //console.log('compWrapper', compWrapper, `currentColWidth=${currentColWidth}`, `newRow=${meta.newRow}`, `hasChildren=${hasChildren}`, `index=${index}`, `fieldOrderLength=${fieldOrder.length}`);

      if (containerID && screenInfo === "document" && !isHidden && !isAction) {
        componentSection = (exclusiveTypes.indexOf(formType) < 0 && !isAction) ? (
          <FormItem
            label={renderData[elmId].name}
            tip={meta.helpText}
            key={elmId}
            componentProps={{ required: meta.required, help: this.state.editMode[containerID] && meta.hintText }}
          >
            {unitComponent}
          </FormItem>
        ) : unitComponent;
        renderRows.push(componentSection);
      } else if (!isHidden && !isAction) {
        currentColWidth += colWidth;
        if (currentColWidth > 24 || meta.newRow) {
          if (renderElms.length) {
            renderRows.push(<Row componentProps={{ gutter: 16 }} key={`${elmId}-${index}`}>{[...renderElms]}</Row>); // `
          }
          renderElms = [];
          currentColWidth = meta.newRow ? 0 : colWidth;
        }
        renderElms.push(compWrapper);

        if (hasChildren && (index !== (fieldOrder.length - 1))) {
          if (renderElms.length) {
            renderRows.push(<Row componentProps={{ gutter: 16 }} key={`${elmId}-${index}${1}`}>{[...renderElms]}</Row>); // `
          }
          renderElms.length = 0;
          currentColWidth = 0;
        }
      } else if (isHidden) {
        hiddenFields.push(componentSection);
      } else if (isAction) {
        actionFields.push(compWrapper);
      }

      // If its the last elem
      if (index === (fieldOrder.length - 1)) {
        if (renderElms.length) {
          renderRows.push(<Row componentProps={{ gutter: 16 }} key={`${elmId}-${index}${2}`}>{[...renderElms]}</Row>); // `
        }
        if (actionFields.length) {
          renderRows.push(<Row componentProps={{ gutter: 8, type: 'flex', justify: 'end' }} key={`${elmId}-${index}${3}`}>{[...actionFields]}</Row>); // `
        }
      }


    }
    return { renderRows, hiddenFields };
  }

  clearFieldMap = () => {
    this.fieldMap = {};
  }

  setFormValues = (formValues) => {
    this.setState({
      formValues,
      formBuilt: true
    })
  }


  render() {
    const { tabs: { activeTab, tabData, screenInfo } } = this.props.detailsPage;

    //console.log('....', activeTab, tabData, Object.keys(tabData).indexOf(activeTab) );
    if (!activeTab || !tabData || !Object.keys(tabData).length
      || Object.keys(tabData).indexOf(activeTab) < 0) return null;

    const fieldOrder = getFieldOrder(tabData[activeTab]);
    this.clearFieldMap();

    const renderElms = this.renderComponent(fieldOrder);
    const renderRows = renderElms && renderElms.renderRows;
    if (!renderRows || !renderRows.length)
      return null;

    let res = null;
    if (screenInfo === "document") {
      res = (
        <Form componentProps={{ layout: "vertical" }}>
          {renderRows}
        </Form>
      )
    } else {
      res = (
        <Form componentProps={{ layout: "horizontal" }}>
          {renderRows}
        </Form>
      )
    }




    // const formItemLayout = {
    //   labelCol: { span: 12 },
    //   wrapperCol: { span: 10 },
    // }

    //console.log("in application details page render", this.state, this.props);
    return (
      <Wrapper>
        {res}
      </Wrapper>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  app: makeSelectApp(),
  applicationDetailsPage: makeSelectApplicationDetailsPage(),
  detailsPage: makeSelectDetailsPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    networkAction: (networkData) => dispatch(networkAction(networkData)),
    //setComments: (comments) => dispatch(setComments(comments)),
    setTabData: (data) => dispatch(setTabData(data)),
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'applicationDetailsPage', reducer });

export default compose(
  withReducer,
  withConnect,
)(ApplicationDetailsPage);
