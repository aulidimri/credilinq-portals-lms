import styled from 'styled-components';
import { colors, sizes } from 'configs/styleVars';

export const Wrapper = styled.div`

  .cardActions {
    display: flex;
    & > * {
      margin: ${sizes.marginS};
      margin-left: 0;
    }
  }

  .ant-form-item {
    margin-bottom: ${sizes.marginL};
  }
`;
