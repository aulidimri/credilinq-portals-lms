import { fromJS } from 'immutable';
import applicationDetailsPageReducer from '../reducer';

describe('applicationDetailsPageReducer', () => {
  it('returns the initial state', () => {
    expect(applicationDetailsPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
