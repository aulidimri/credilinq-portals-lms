/*
 * ApplicationDetailsPage Messages
 *
 * This contains all the text for the ApplicationDetailsPage container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.ApplicationDetailsPage';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the ApplicationDetailsPage container!',
  },
});
