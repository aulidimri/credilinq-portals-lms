/**
 *
 * Asynchronously loads the component for ApplicationDetailsPage
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));
