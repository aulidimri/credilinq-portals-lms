import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the applicationDetailsPage state domain
 */

const selectApplicationDetailsPageDomain = state =>
  state.get('applicationDetailsPage', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by ApplicationDetailsPage
 */

const makeSelectApplicationDetailsPage = () =>
  createSelector(selectApplicationDetailsPageDomain, substate =>
    substate.toJS(),
  );

export default makeSelectApplicationDetailsPage;
export { selectApplicationDetailsPageDomain };
