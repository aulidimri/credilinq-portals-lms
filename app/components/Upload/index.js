import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Upload as AntUpload } from 'antd';
import { DefaultStyle } from './Styles';

import { connect } from 'react-redux';
import { compose } from 'redux';
import { networkAction } from 'containers/App/actions';

import Button from 'components/Button';
import Icon from 'components/Icon';

import dottie from 'dottie';

const { Dragger: AntDragger } = AntUpload;
class Upload extends PureComponent {

  /**
   * CustomRequest to upload the file
   * onProgress has data for how much of the file was uploaded to the server
   * onSuccess & onError are used by ant to determine the status of the upload
   * file is the file being uploaded
   */
  customRequest = ({ onSuccess, onError, onProgress, file }) => {
    const { meta, keys } = this.props.componentProps;
    const { api: { method, subUrl, queryParams, requestPayload } } = meta;

    const qParams = {};
    queryParams.forEach(param => {
      qParams[param] = keys[param]
    });

    const fileData = new FormData();
    fileData.append('file', file);
    fileData.append('document_type', requestPayload.requestBody.document_type);
    const data = fileData;
    const fileUploadRequest = {
      api: 'configApi',
      method: method || 'POST',
      params: { subUrl },
      data,
      queryParams: qParams,
      onUploadProgress: (e) => {
        if (e.total && e.loaded) {
          const res = { percent: Math.round((e.loaded * 100) / e.total) };
          onProgress(res)
        }
      },
      successCallback: (res) => {
        console.log("SUCCESS UPLOADING", res.data.response);
        onSuccess(res, data);
      },
      failCallback: (err) => {
        console.log("ERROR UPLOADING", err);
        onError(err, data);
      }
    };
    this.props.networkAction(fileUploadRequest);
  }

  /**
   *
   * @param {*} file local file object which will refer to the generated object url of the downloaded file
   * @param {*} url base url from where to download the file from
   * @param {*} document_id document id of the uploaded file
   */
  downloadFile(file, url, document_id) {
    const newTab = window.open();
    const { meta, keys } = this.props.componentProps;
    //console.log(meta);
    const { api: { method, subUrl, queryParams, requestPayload } } = meta;

    const qParams = {};
    queryParams.forEach(param => {
      qParams[param] = keys[param]
    });

    const fileURLRequest = {
      api: 'download',
      method: 'GET',
      params: { subUrl: url },
      queryParams: { document_id },
      responseType: "blob",
      loader: true,
      successCallback: (res) => {
        console.log("SUCCESS GETTING FILE DATA");
        const url = URL.createObjectURL(res.data);
        file.url = url;
        if (!newTab) {
          console.log("Error: Please allow popups to be opened by the application");
        } else {
          newTab.location.href = url;
        }
      },
      failCallback: (err) => {
        console.log("ERROR GETTING FILE DATA", err);
      }
    };
    this.props.networkAction(fileURLRequest);
  }

  /**
   * @param url url of the file object to which the new tab should refer to
   */
  openTab = (url) => {
    const newTab = window.open();
    if (!newTab) {
      console.log("Error: Please allow popups to be opened by the application");
    } else {
      newTab.location.href = url;
    }
  }

  onPreview = (file) => {
    if (file.url) {
      this.openTab(file.url);
    } else {
      if (file.originFileObj) {
        const url = URL.createObjectURL(file.originFileObj);
        file.url = url;
        this.openTab(url);
      } else {
        const { meta, keys } = this.props.componentProps;
        const { api: { method, subUrl, queryParams, requestPayload, url } } = meta;
        this.downloadFile(file, url, file.response.id);
      }
    }
  }

  /**
   * Fires when the file.status changes
   */
  onChangeHandler = ({ file, fileList, event }) => {
    //file.status = uploading/done/error/removed
    fileList = fileList.map(file => {
      const response = dottie.get(file, `response.data.response`) || file.response;
      return {
        uid: file.uid,
        status: file.status,
        percent: file.percent,
        name: file.name,
        response: response,
        originFileObj: file.originFileObj,
      }
    })
    this.props.onChangeHandler && this.props.onChangeHandler(fileList);
  }

  //It is used to customize the thumbnail of the file
  previewFile = (file) => {
    return new Promise(resolve => { resolve("https://www.google.com") });
  }



  render() {

    const { componentProps = {} } = this.props;
    const { meta, keys, disabled, fileList, ...baseComponentProps } = componentProps;  //files will be a stringified array of file's responses
    const fileTypesAccepted = meta.filetypes; // || "application/pdf,image/png,image/jpeg";

    let disableInput = false;
    if (meta.isMultiple === false && fileList.length) {
      disableInput = true;
    }

    return (
      <DefaultStyle isDisabled={disabled}>
        <AntDragger fileList={fileList} disabled={disableInput} listType="picture" onPreview={this.onPreview} accept={fileTypesAccepted} customRequest={this.customRequest} {...baseComponentProps} onChange={this.onChangeHandler} className="upload-list-inline">
          <div className="upload-component-link"><a disabled={disableInput}><Icon componentProps={{ disabled: disableInput, type: "link" }} />Add file</a>or drop files here</div>
        </AntDragger>
      </DefaultStyle>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return {
    networkAction: (networkData) => dispatch(networkAction(networkData)),
    dispatch,
  };
}

const withConnect = connect(
  null,
  mapDispatchToProps,
);

export default compose(
  withConnect,
)(Upload);
