import styled from 'styled-components';
import { sizes } from 'configs/styleVars'

export const DefaultStyle = styled.div`

.upload-list-inline .ant-upload-list-item {
  float: left;
  width: 200px;
  margin-right: 8px;
}
.upload-list-inline .ant-upload-animate-enter {
  animation-name: uploadAnimateInlineIn;
}
.upload-list-inline .ant-upload-animate-leave {
  animation-name: uploadAnimateInlineOut;
}

.upload-component-link, .upload-component-link > * {
  display: flex;
  padding: 0 5px;
}

.upload-component-link .anticon {
  padding-right: 5px;
}

.ant-upload.ant-upload-drag{
  width: auto;
  display: ${props => props.isDisabled ? "none" : "inline-block"};
}

.ant-upload.ant-upload-drag .ant-upload {
  padding: 5px;
}
`;
