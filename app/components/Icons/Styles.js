import styled from 'styled-components';
import { colors, fonts, sizes } from 'configs/styleVars';

export const DefaultStyle = styled.div`
  display: flex;
  align-items: center;
  .anticon {
    padding: 0 ${sizes.paddingXS};
  }
`;
