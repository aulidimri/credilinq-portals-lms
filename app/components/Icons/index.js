import React, { PureComponent } from 'react';
import Icon from 'components/Icon';
import Dropdown from 'components/Dropdown';
import PropTypes from 'prop-types';
import { DefaultStyle } from './Styles';

import uuid from 'uuid/v4';

class Icons extends PureComponent {
  static propTypes = {
    componentProps: PropTypes.object,
    children: PropTypes.node,
  };

  renderIcons = (icons, iconsToDisplay) => {
    const maxIconsToDisplay = 3;

    iconsToDisplay = Math.min(iconsToDisplay || icons.length, maxIconsToDisplay);
    if (icons.length > maxIconsToDisplay)
      iconsToDisplay = maxIconsToDisplay - 1;
    const iconsToDisplayAsIcons = icons.slice(0, iconsToDisplay);
    const iconsToDisplayAsDropdown = icons.slice(iconsToDisplay);

    //Render as icons
    const res = iconsToDisplayAsIcons.map(icon => (
      <Icon
        key={uuid()}
        onClickHandler={this.props.onClickHandler}
        onClickParams={icon.onClickParams}
        componentProps={{ type: icon.icon, disabled: icon.disabled }}
      />
    ));
    //Render the rest as dropdown options
    if (iconsToDisplayAsDropdown.length) {
      res.push(
        <Dropdown
          key={uuid()}
          onClickHandler={this.props.onClickHandler}
          componentProps={{
            data: iconsToDisplayAsDropdown,
            placement: 'bottomRight',
            trigger: ['click'],
          }}
        >
          <Icon
            componentProps={{
              type: 'more',
            }}
          />
        </Dropdown>
      );
    }

    return res;
  }

  render() {
    const { componentProps, onClickHandler, data } = this.props;
    const { iconsToDisplay, ...baseComponentProps } = componentProps;

    return (
      <DefaultStyle>
        {this.renderIcons(data, iconsToDisplay)}
      </DefaultStyle>
    );
  }
}

export default Icons;
