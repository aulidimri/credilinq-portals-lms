import styled from 'styled-components';
import { colors, sizes } from 'configs/styleVars';

export const DefaultStyle = styled.div`
  width: 100%;

  .ant-table-thead > tr > th {
    background-color: ${colors.primaryComponentBG};
    color: ${colors.primaryComponentColor};
    padding: ${sizes.paddingXS};
  }

  .ant-table-tbody > tr > td {
    padding: ${sizes.paddingXS};
    cursor: pointer;
  }


  .ant-pagination {
    float: none;
    display: flex;
    justify-content: start;
  }

  .ant-pagination > li{
    &:first-child {
      order: 2;
      margin-right: auto;
    }

    &:last-child {
      order: 1;
      margin-left: 0;
    }

    order: 3;
  }

  .searchBar {
    display: flex;
    & > * {
      margin: ${sizes.marginS};
      margin-left: 0;
    }
  }

  .filterSelect {
    width: 200px;
  }

  // .table-component-actions-container {
  //   display: flex;
  //   justify-content: center;
  // }

  .table-component-actions-bar {
    display: flex;
    justify-content: space-between;
  }

`;
