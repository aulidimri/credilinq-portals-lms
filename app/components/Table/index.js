import React, { Component } from 'react';
import { Table as AntTable } from 'antd';
import Icons from 'components/Icons';
import Actions from 'components/Actions';
import Input from 'components/Input';
import Select from 'components/Select';
import PropTypes from 'prop-types';
import { DefaultStyle } from './Styles';
import { tableFixedColumnCountThreshold } from '../../utils/constants';

/**
 * Table Component
 */
class Table extends Component {

  state = {
    /** If selection is true, contains the row entry keys that are selected */
    filterKey: "",
  }

  static propTypes = {
    /** Whether to allow selection or rows or not */
    selection: PropTypes.bool,
    /** Data to render inside the table */
    renderData: PropTypes.object,
    /** Actions allowed on the table data entries */
    actions: PropTypes.object,
    componentProps: PropTypes.object,
    children: PropTypes.node,
  };

  static defaultProps = {
    componentProps: {},
  }

  renderActions = (record) => {
    const { componentProps, handleAction } = this.props;
    const { actions, iconsToDisplay } = componentProps;
    const data = Array.isArray(actions.list) && actions.list.map((action) => ({
      ...action,
      //onClickHandler: handleAction,
      onClickParams: [action.key, record],
    })) || [];

    return <div className="table-component-actions-container"><Icons onClickHandler={handleAction} componentProps={{ iconsToDisplay: iconsToDisplay }} data={data} /></div>
  }

  renderBulkActions = (records) => {
    const { componentProps, handleBulkAction } = this.props;
    const { bulkActions, disableBulkActions } = componentProps;

    const data = Array.isArray(bulkActions) && bulkActions.map((bulkAction) => ({
      ...bulkAction,
      onClickParams: [bulkAction.key, records],
      disabled: !records.length,
    })) || [];

    return <Actions data={data} onClickHandler={handleBulkAction} componentProps={{ actionsToDisplay: 2 }} />
  }

  renderSearch = (columns) => {

    const filterOptions = Array.isArray(columns) && columns.map(colHeader => ({
      value: colHeader.dataIndex,   //dataIndex is the id of the column header variable
      label: colHeader.title,
    })) || [];


    return (
      <div className="searchBar">
        <Input onSearchHandler={this.searchHandler} componentProps={{ type: "search" }} />
        <Select onChangeHandler={this.onFilterChange} componentProps={{ className: "filterSelect", placeholder: "Filter By", options: filterOptions, value: this.state.filterKey, }} />
      </div>
    );

  }

  searchHandler = (value) => {
    const { filterKey } = this.state;
    console.log(value, filterKey);
    this.props.onSearchHandler && this.props.onSearchHandler(value, filterKey);
  }

  onFilterChange = (value) => {
    this.setState({
      filterKey: value,
    })
  }


  static getDerivedStateFromProps(nextProps, prevState) {
    const { headings } = nextProps.componentProps
    if (Array.isArray(headings) && headings[0] && headings[0].dataIndex && !prevState.filterKey) {
      return {
        filterKey: headings[0].dataIndex,
      }
    }
    return null;
  }

  renderTableActions = (isSearch, headings, rowSelection) => {

    if(!isSearch && !rowSelection) {
      return null;
    }

    return (
      <div className="table-component-actions-bar">
        {isSearch && this.renderSearch(headings)}
        {rowSelection && this.renderBulkActions(rowSelection.selectedRowKeys)}
      </div>
    )
  }


  render() {
    const { componentProps, handleBulkAction, handleAction } = this.props;
    const { bulkActions, actions, headings, rows, iconsToDisplay, selection, isSearch, onSearchHandler, rowSelection, ...baseComponentProps } = componentProps
    const columns = headings.map(col => ({ ...col }));

    let scrollProps = {};
    let selectionColProps = {
      width: 100,
      fixed: 'left',
    };
    let firstColProps = {
      width: 150,
      fixed: 'left',
    };
    let actionColProps = {
      fixed: 'right',
      width: 100,
    };
    console.log('props in table', this.props);

    // uncomment below code to mock table columns
    // headings= [...headings,
    //   ...(headings.map((h)=>({...h,key:uuid()}))),
    //   ...(headings.map((h)=>({...h,key:uuid()}))),
    //   ...(headings.map((h)=>({...h,key:uuid()}))),
    //   ...(headings.map((h)=>({...h,key:uuid()}))),
    // ]

    let isTableScrollable = headings.length > tableFixedColumnCountThreshold;
    if (actions && Array.isArray(actions.list)) {
      // Concatted cuz we shouldn't modify props, props are read only
      columns.push(
        {
          title: actions.label,
          key: actions.key,
          render: (text, record) => this.renderActions(record),
          ...(isTableScrollable ? {} : actionColProps ),
        },
      );
    }
    if (isTableScrollable && columns.length) {
      columns[0] = {
        ...columns[0],
        ...(isTableScrollable ? {} : firstColProps),
      }
    }
    scrollProps = isTableScrollable ? { scroll: { x: columns.length * 150 } } : {};

    //Capitalize Table Headers
    columns.forEach(column => column.title = column.title.toUpperCase());


    return (
      <DefaultStyle>
        {
          this.renderTableActions(isSearch, headings, rowSelection)
        }
        <AntTable
          {...baseComponentProps}
          {...scrollProps}
          dataSource={rows}
          columns={columns}
          rowSelection={rowSelection}
        />
      </DefaultStyle>
    );
  }
}

export default Table;
