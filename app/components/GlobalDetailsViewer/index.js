import React, { PureComponent } from 'react';
import { DefaultStyle } from './Styles';
import { Link } from 'react-router-dom';

import Col from 'components/Col';
import FormItem from 'components/FormItem';
import Actions from 'components/Actions';
import Icon from 'components/Icon';
import { withRouter } from 'react-router';
import { compose } from 'redux';

import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { makeSelectDetailsPage } from 'containers/DetailsPage/selectors';
import { makeSelectApp } from 'containers/App/selectors';


import propsMap from 'configs/propsMapByFormType';
import componentMap from 'configs/componentMapByFormType';

import dottie from 'dottie';
import { getMutatedFormType, getFieldOrder } from 'utils/apiUtility';

class GlobalDetailsViewer extends PureComponent {

  state = {
    maximized: true,
    submitState: {

    },
    formValues: {

    }
  }

  fireAction = (args) => {
    const { id: elmId, params: { meta } } = args;
    const { formValues } = this.state;

    if (meta.restApi) {
      this.setState({
        submitState: {
          ...this.state.submitState,
          [elmId]: true,
        }
      });
      this.props.fireRestAction(args, formValues, () => {
        this.setState({
          submitState: {
            ...this.state.submitState,
            [elmId]: false,
          }
        });
      }, (updatedFormValues) => {
        this.setState({
          formValues: { ...formValues, ...updatedFormValues }
        })
      })
    }
  }

  /**
   * name: The menu-item value that gets selected by the user
   */
  onClickHandler = (e, args, name) => {


    const { id: elmId } = args;
    if (name) {
      this.setState({
        formValues: {
          ...this.state.formValues,
          [elmId]: name
        }
      }, () => {
        this.fireAction(args)
      });
    } else {
      this.fireAction(args)
    }
  }

  getExtraProps = (formType, renderData) => {
    const { record: { caseInstanceId }, roleSelected: { key: roleName }, } = this.props.app;
    const { tabs: { activeTab: tabKey } } = this.props.detailsPage;
    const meta = renderData && renderData.params.meta;
    let extraProps = {};
    switch (formType) {
      case 'droplist':
      case 'rest-droplist':
        const createOptions = option => {
          return {
            label: option.name,
            onClickParams: [renderData, option.id],
          }
        };
        extraProps = {
          data: Array.isArray(renderData.enumValues) && renderData.enumValues.map(createOptions) || [],
          keys: { caseInstanceId, roleName, tabKey },
        }
        break;
      case 'checkbox':
        extraProps = {
          label: renderData.name,
        }
        break;
      default:
        extraProps = {};
    }
    return extraProps;
  }

  //TODO: Candidate for constants file
  getUnitComponent(elmId, renderData) {
    if (elmId === "cmmnCreditAnalystPOCAdminStatus")  //Status removed for now, later convert this to tag
      return null;

    const meta = renderData.params && renderData.params.meta;
    const readMode = true;
    const formType = getMutatedFormType(meta.form_type, readMode);
    const value = renderData.value || meta.default;
    const extraProps = this.getExtraProps(formType, renderData);
    const compProps = propsMap[formType] && propsMap[formType](renderData, elmId, value, readMode);
    //console.log('renderData & elmId', renderData, elmId);
    const isHidden = formType === 'hidden';
    if (meta.restApi && formType === 'button') {
      compProps.loading = this.state.submitState[elmId] || false;
    }
    const UnitComponent = componentMap[formType]
    if (!UnitComponent || meta.dummyField || isHidden) {
      return null;
    }

    let unitComponent = (<UnitComponent
      componentProps={{ ...compProps, ...extraProps }}
      key={elmId}
      elmId={elmId}
      renderData={renderData}
    />)

    const formItemExclusionSet = ["heading", "button", "droplist", "rest-droplist", "checkbox", "radio"];
    if (formItemExclusionSet.indexOf(formType) < 0) {
      const formItemLayout = {
        labelCol: { span: 12 },
        wrapperCol: { span: 12 },
      }

      unitComponent = (
        <FormItem
          label={renderData.name}
          key={elmId}
          componentProps={{ ...formItemLayout }}
        >
          {unitComponent}
        </FormItem>
      )
    }

    const colSpan = meta.section === 'variables' ? 12 : 24;
    return <Col key={elmId} componentProps={{ span: colSpan }}>{unitComponent}</Col>;
  }

  renderSections(renderRowsBySection) {
    const sectionNames = Object.keys(renderRowsBySection);
    const renderSections = [];
    for (let index = 0; index < sectionNames.length; ++index) {
      const sectionName = sectionNames[index];
      const renderSection = (
        <div className={"global-section global-section-" + sectionName} key={"global-section-" + sectionName}>
          {renderRowsBySection[sectionName]}
        </div>
      )
      renderSections.push(renderSection);
    }
    return renderSections;
  }

  renderComponent(orderedFieldIds, renderData) {

    const renderRowsBySection = {};


    const actionsInclusionSet = ["button", "droplist", "rest-droplist"];
    let unitComponent;
    for (let index = 0; index < orderedFieldIds.length; ++index) {
      const elmId = orderedFieldIds[index];
      const meta = dottie.get(renderData[elmId], 'params.meta');
      const formType = meta.form_type;
      const section = meta.section;

      //TODO: Refactor the code
      //Groups all the actions in the action section into the Action Component
      //The actions should appear consecutively in the API response data
      if (meta.type === "action") {
        const actionsData = [];
        for (let i = index; i < orderedFieldIds.length; ++i) {
          const elmId = orderedFieldIds[i];
          const meta = dottie.get(renderData[elmId], 'params.meta');
          const formType = meta.form_type;
          const section = meta.section;
          //If the next item is not an action, update the index and move on
          if (meta.type !== "action") {
            index = i - 1;
            break;
          }

          if (formType === "droplist" || formType === "rest-droplist") {

            const extraProps = this.getExtraProps(formType, renderData[elmId]);
            const data = extraProps.data;
            const keys = extraProps.keys;
            actionsData.push({
              renderData: renderData[elmId],
              elmId,
              onClickHandler: this.onClickHandler,
              label: renderData[elmId].name,
              data,
              keys,
            })
          } else {
            actionsData.push({
              label: renderData[elmId].name,
              //disabled: buttonRenderData.disabled??
              onClickParams: [renderData[elmId]]
            })
          }
        }
        unitComponent = <Actions onClickHandler={this.onClickHandler} key={`${section}-actions`} data={actionsData} componentProps={{ actionsToDisplay: 2, }} />
      } else {
        unitComponent = this.getUnitComponent(elmId, renderData[elmId]);
      }

      if (!unitComponent) {
        continue;
      }

      renderRowsBySection[section] = renderRowsBySection[section] || [];
      renderRowsBySection[section].push(unitComponent);
    }


    const renderSectionsWithDivider = this.renderSections(renderRowsBySection);

    return { renderSectionsWithDivider };
  }

  toggleSize = () => {
    this.setState({
      maximized: !this.state.maximized,
    })
  }


  render() {
    const { componentProps, data } = this.props;
    const { ...baseComponentProps } = componentProps;

    if (!data)
      return null;

    //console.log("Global Variables", data);

    const orderedFieldIds = getFieldOrder(data);

    const { renderSectionsWithDivider } = this.renderComponent(orderedFieldIds, data);

    const listPath = [
      <li key="Listing" ><Link to="/app/listing">Listing</Link></li>
    ];
    if (!this.state.maximized) {
      const res = Object.values(data).find(item => item.params.meta.applicationId);
      const applicationId = "#" + (res.value || res.params.meta.default);
      listPath.push(<li key="applicationId"><span>{applicationId}</span></li>)
    }

    return (
      <DefaultStyle>
        <div className="global-variables-header">
          <ul className="global-variables-path-list">
            {listPath}
          </ul>
          <a onClick={this.toggleSize}><Icon componentProps={this.state.maximized ? { label: "Minimize", type: "up" } : { label: "Maximize", type: "down" }} /></a>
        </div>

        <div id="global-sections" className={this.state.maximized ? "display-flex" : "display-none"}>
          {renderSectionsWithDivider}
        </div>
      </DefaultStyle>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  app: makeSelectApp(),
  detailsPage: makeSelectDetailsPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    networkAction: (networkData) => dispatch(networkAction(networkData)),
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withRouter,
  withConnect,
)(GlobalDetailsViewer);
