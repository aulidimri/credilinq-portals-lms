import styled from 'styled-components';
import { colors, fonts, sizes } from 'configs/styleVars';

export const DefaultStyle = styled.div`
background-color: ${colors.primaryBodyBG};
padding: ${sizes.paddingS} ${sizes.paddingM};

.ant-form-item {
  margin-bottom: ${sizes.marginS};
}

.global-variables-header {
  display: flex;
  justify-content: space-between;
  align-items: center;
}

.global-variables-path-list {
  display: flex;
  list-style: none;
  padding: 0;
  margin: 0;
}

.global-variables-path-list > li:not(:last-child) {
  &::after {
    content: '>';
    margin: 0 ${sizes.marginXS};
  }
}

.global-variables-path-list-caret {
  font-size: 0.8rem;
}

.global-section {
  padding: 0 ${sizes.paddingM};
  border-right: 1px solid ${colors.primaryBorderColor};

  &:first-child {
    padding-left: 0;
  }

  &:last-child {
    border-right: none;
  }
}


.global-section-info {
  width: 350px;
}

.global-section-variables {
  flex-grow: 1;
  .ant-form-item-control {
    font-weight: bold;
  }
}

.global-section-actions {
  width: 350px;
}

}
`;
