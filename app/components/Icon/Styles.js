import styled from 'styled-components';
import { sizes, colors } from 'configs/styleVars'

export const DefaultStyle = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  color: ${props => props.isDisabled ? colors.disabledBodyColor : "inherit"};

  span {
    order: ${props => (props.isPrefix ? -1 : 1)};
  }

  .icon-label {
    padding: 0 ${sizes.paddingS};
  }
`;
