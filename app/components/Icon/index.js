import { Icon as AntIcon } from 'antd';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { DefaultStyle } from './Styles';

/**
 * Icon Component
 */
class Icon extends PureComponent {
  static propTypes = {
    componentProps: PropTypes.object,
    /** function to be called when the component is clicked */
    onClickHandler: PropTypes.func,
    /** arguments to be passed to the onClickHandler function */
    onClickParams: PropTypes.array,
    /** Label to display next to the icon */
    label: PropTypes.string,
  };

  static defaultProps = {
    componentProps: {},
  };

  onClickHandler = (e) => {
    const { onClickHandler, onClickParams, onClick: antOnClick, componentProps } = this.props;
    const { disabled } = componentProps;

    if(disabled) {
      e.stopPropagation();
      return;
    }

    if (onClickHandler) {
      if (onClickParams && onClickParams.length)
        onClickHandler(e, ...onClickParams);
      else
        onClickHandler(e);
    }
    if( antOnClick ) {
      //console.log(e);
      antOnClick(e);
    }
  };

  //Method to accomodate for custom svg icons in icon component
  // getMutatedComponent =(type) => {
  //   let component;
  //   switch(type){
  //     case 'bell':
  //       component = MessageSvg;
  //       break;

  //     default:
  //       break;
  //   }

  //   return component;
  // }

  render() {
    const { componentProps, onClickHandler, onClickParams, onClick: antOnClick, ...antProps } = this.props;
    const { label, prefix, disabled, ...baseComponentProps } = componentProps;
    //const iconComponent = this.getMutatedComponent(baseComponentProps.type);

    return (
      <DefaultStyle isPrefix={prefix} isDisabled={disabled} >
        <AntIcon {...antProps} {...baseComponentProps} onClick={this.onClickHandler}/>
        {label && <span className="icon-label">{label}</span>}
      </DefaultStyle>
    );
  }
}

export default Icon;
