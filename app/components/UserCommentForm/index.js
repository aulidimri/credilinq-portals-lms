import React, { PureComponent } from 'react';
import Input from 'components/Input';
import Button from 'components/Button';
import Row from 'components/Row';
import Col from 'components/Col';
import { DefaultStyle } from './Styles';

import { networkAction } from 'containers/App/actions';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { makeSelectApp } from 'containers/App/selectors';


//TODO make it very general to use as comment
class UserCommentForm extends PureComponent {

  state = {
    value: '',
    submitting: false,
  }

  onSubmitHandler = () => {
    let { value } = this.state;
    value = value.trim();
    if (!value) {
      return;
    }

    this.setState({
      submitting: true,
    });

    const { params } = this.props;
    const postCommentRequest = {
      api: 'postComment',
      method: 'POST',
      params: params,
      data: { message: value },
      // successAction: this.props.setComments,
      successCallback: (res) => {
        //console.log('comment post', res);
        this.setState({
          submitting: false,
          value: '',
        })
        this.props.onSubmitHandler && this.props.onSubmitHandler(res.data);
      },
      failCallback: (err) => {
        this.setState({
          submitting: false,
        });
      }
    };
    this.props.networkAction(postCommentRequest);
  }

  onChangeHandler = (value) => {
    this.setState({
      value,
    })
  }

  render() {
    const { componentProps } = this.props;
    const { rows } = componentProps;
    const { value, submitting } = this.state;


    return (
      <DefaultStyle>
        <Row componentProps={{ type: "flex", span: 24 }}>
          <Col componentProps={{
            xs: { span: 16 },
            lg: { span: 18 },
            xl: { span: 20 },
          }}>
            <Input
              componentProps={{
                type: "textarea",
                rows: rows,
                value: value,
              }}
              onChangeHandler={this.onChangeHandler}
            />
          </Col>
          <Col componentProps={{
            id: "formActions",
            xs: { span: 8 },
            lg: { span: 6 },
            xl: { span: 4 },
          }}>
            <Button
              componentProps={{
                id: "submit",
                loading: submitting,
                type: "primary",
                label: "Submit",
              }}
              onClickHandler={this.onSubmitHandler}
            />
          </Col>
        </Row>
      </DefaultStyle>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  app: makeSelectApp(),
});

function mapDispatchToProps(dispatch) {
  return {
    networkAction: (networkData) => dispatch(networkAction(networkData)),
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
)(UserCommentForm);
