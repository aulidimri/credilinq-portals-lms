import styled from 'styled-components';

export const DefaultStyle = styled.div`

  #formActions {
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
  }

  #formActions > *{
    margin-left: 10px;
  }
`;


