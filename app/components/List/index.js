import React, { PureComponent } from 'react';
import { List as AntList } from 'antd';
import PropTypes from 'prop-types';
import { DefaultStyle } from './Styles';

/**
 * List Component
 */
class List extends PureComponent {
  static propTypes = {
    componentProps: PropTypes.object,
    children: PropTypes.node,
  };

  render() {
    const { componentProps } = this.props;
    return (
      <DefaultStyle>
        <AntList {...componentProps}>{this.props.children}</AntList>
      </DefaultStyle>
    );
  }
}

export default List;
