/**
*
* TabCard
*
*/

import React from 'react';
import { DefaultStyle } from './Styles';

import PropTypes from 'prop-types';

class TabCard extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function

  static propTypes = {
    children: PropTypes.node.isRequired,
  }

  render() {
    const { componentProps, children } = this.props;
    const {fullWidth, title, ...baseComponetProps} = componentProps
    //console.log('card props', this.props);

    return (
      <DefaultStyle>
        <div className="ant-title">{title}</div>
        {children}
      </DefaultStyle>
    );
  }
}


export default TabCard;
