import styled from 'styled-components'
import { colors, sizes, fonts } from 'configs/styleVars';

export const viewWithoutIconStyles = {
  marginLeft: 0,
  paddingBottom: 20,
  marginLeft: 38,
};

export const DefaultStyle = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  align-items: center;
  font-size: ${fonts.fontSizeM};
  margin-bottom: ${sizes.marginS};
  .ant-title {
    + div {
      width: auto;
    }
  }
`;
