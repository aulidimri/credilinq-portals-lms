import React, { PureComponent } from 'react';
import { DefaultStyle } from './Styles';

class Heading extends PureComponent {

  render() {
    const { componentProps } = this.props;
    const { type, label, value, note } = componentProps;

    return (
      <DefaultStyle type={type} note={note}>
        {label ? (<span className="heading-component-label">{label}</span>) : null}
        {value}
      </DefaultStyle>
    )
  }
}

export default Heading;
