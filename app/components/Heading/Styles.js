import styled from 'styled-components';
import { colors, fonts, sizes } from 'configs/styleVars';

const fontSizeByType = {
  h1: fonts.fontSizeXL,
  h2: fonts.fontSizeL,
  h3: fonts.fontSizeM,
  h4: fonts.fontSizeS,
  label: fonts.fontSizeXS,
}

export const DefaultStyle = styled.div`
  .heading-component-label {
    color: ${props => props.note ? colors.errorBodyColor : "inherit"};
  }
  font-size: ${props => fontSizeByType[props.type]};
`;
