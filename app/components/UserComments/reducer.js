import {
  SET_COMMENTS
} from './constants';

const initialState = fromJS({
  sidebar: {
    collapse: false,
  },
  tabs: {
    comment: [],
  }
});
