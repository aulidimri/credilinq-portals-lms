import styled from 'styled-components';

export const DefaultStyle = styled.div`

  #openCommentModalButton {
    position: fixed;
    bottom: 0;
    right: 50px;
  }
`;
