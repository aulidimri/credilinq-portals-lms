import {
  SET_COMMENTS
} from './constants';

export function setComments(comment) {
  return {
    type: SET_COMMENTS,
    comment,
  };
}
