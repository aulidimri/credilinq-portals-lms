import React, { PureComponent } from 'react';
import Modal from 'components/Modal';
import Comment from 'components/Comment';
import Button from 'components/Button';
import List from 'components/List';
import Icon from 'components/Icon';
import UserCommentForm from 'components/UserCommentForm';
import PropTypes from 'prop-types';
import { DefaultStyle } from './Styles';

import { networkAction } from 'containers/App/actions';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { makeSelectApp } from 'containers/App/selectors';

class UserComments extends PureComponent {

  static propTypes = {
    componentProps: PropTypes.shape({
      comments: PropTypes.array,
    }),
  }

  static defaultProps = {
  }

  state = {
    modalVisible: false,
    data: [],
    tabKey: "",
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.tabKey !== prevState.tabKey) {
      const { fetchComments: { params }, tabKey } = this.props;
      const fetchCommentsRequest = {
        api: 'fetchComments',
        method: 'GET',
        params: params,
        encode: false,
        successCallback: (response) => {
          this.setState({
            data: response.data,
            tabKey: this.props.tabKey,
          })
        }
      }
      this.props.networkAction(fetchCommentsRequest);
    }
  }

  openModal = () => {
    this.setState({
      modalVisible: true,
    })
  }

  closeModal = () => {
    this.setState({
      modalVisible: false,
    })
  }

  onSubmitHandler = (data) => {
    this.setState({
      data: [
        ...this.state.data,
        data
      ]
    })
  }

  render() {
    const { componentProps, fetchComments, postComment } = this.props;
    const { accessCode, userName, roleName } = componentProps;
    const { modalVisible, data } = this.state;

    const comments = Array.isArray(data) && data.map(com => ({
      author: `${com.userId} (${com.roleName})`,
      content: com.message,
      time: com.createdTime,
      tab: com.taskKey,
    })
    ).sort((a, b) => new Date(b.time) - new Date(a.time)) || [];   //TODO: Sorting not working on firefox

    const userAccess = ["No Access", "Read Access", "Write Access"]
    let commentInputHide;
    switch (userAccess[accessCode]) {
      case "No Access":
        return null;
      case "Read Access":
        commentInputHide = true;
        break;
      case "Write Access":
        commentInputHide = false;
    }

    const formContainingComment = {
      author: `${userName} (${roleName})`,
      content: <UserCommentForm componentProps={{ rows: 3 }} onSubmitHandler={this.onSubmitHandler} params={postComment.params} />,
    }

    return (
      <DefaultStyle>
        <Button
          componentProps={{
            id: "openCommentModalButton",
            type: "primary",
            label: `Comments (${comments.length})`,
          }}
          onClickHandler={this.openModal}
        />

        <Modal
          //inline styles must be passed to title, cant figure out another way to style modal title
          componentProps={{
            title: (
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div>{`Comments (${comments.length})`}</div>
                <div style={{ display: "flex" }}>
                  <Icon componentProps={{ type: "close" }} onClickHandler={this.closeModal} />
                </div>
              </div>
            ),
            width: '70%',
            bodyStyle: { height: "550px", overflow: "auto", },
            visible: modalVisible,
            footer: null,
            closable: false,
          }}
          onOkHandler={this.closeModal}
          onCancelHandler={this.closeModal}
        >
          <div>
            {
              commentInputHide ? null : (
                <Comment
                  componentProps={{
                    comment: formContainingComment,
                  }} />
              )
            }
            <List
              componentProps={{
                dataSource: comments,
                itemLayout: "horizontal",
                renderItem: item => <Comment componentProps={{ comment: item }} />,
              }} />
          </div>
        </Modal>
      </DefaultStyle>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  app: makeSelectApp(),
});

function mapDispatchToProps(dispatch) {
  return {
    networkAction: (networkData) => dispatch(networkAction(networkData)),
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
)(UserComments);
