import styled from 'styled-components'
import { colors, sizes } from 'configs/styleVars';

// export const labelStyles = {
//   ...fonts.getFontFamilyWeight('bold'),
// };

export const viewWithoutIconStyles = {
  marginLeft: 0,
  paddingBottom: 20,
  marginLeft: 38,
};

// export const viewWithIconStyles = {
//   ...viewWithoutIconStyles,
//   marginLeft: 38,
// };

export const DefaultStyle = styled.div`

  margin-bottom: ${sizes.marginM};
  .ant-card-body {
    padding: ${({fullWidth}) => (!fullWidth ? '24px' : 0)};
  }
  .ant-card-head {
    background-color: ${colors.primaryComponentBG};
    color: ${colors.primaryComponentColor};
    min-height: auto;
  }

  .ant-card-head-title {
    padding: 0;
  }

  .ant-card-extra {
    padding: 0;
  }

`;
