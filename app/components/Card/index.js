/**
*
* Card
*
*/

import React from 'react';
import { Card as AntCard} from 'antd';
import { DefaultStyle } from './Styles';

import PropTypes from 'prop-types';

class Card extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function

  static propTypes = {
    children: PropTypes.node.isRequired,
  }

  render() {
    const { componentProps, children } = this.props;
    const {fullWidth, ...baseComponetProps} = componentProps
    //console.log('card props', children, this.props);


    return (
      <DefaultStyle fullWidth={fullWidth}>
        <AntCard
          {...baseComponetProps}>
          {this.props.children}
        </AntCard>
      </DefaultStyle>
    );
  }
}


export default Card;
