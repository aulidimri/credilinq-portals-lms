import React, { PureComponent } from 'react';
import { Menu as AntMenu } from 'antd';
import PropTypes from 'prop-types';
import { DefaultStyle } from './Styles';

import MenuItem from 'components/MenuItem';
import SubMenu from '../SubMenu';
import uuid from 'uuid';


/**
 * Menu Component
 */
class Menu extends PureComponent {
  static propTypes = {
    componentProps: PropTypes.object,
    children: PropTypes.node,
  };

  render() {
    // antProps contain the unidentified props that are passed by the Ant Design Framework by parent components like Dropdown, Select, etc.
    const { data = [], componentProps, onClickHandler, ...antProps } = this.props;
    const { ...baseComponentProps } = componentProps;
    return (
      <DefaultStyle>
        <AntMenu  {...antProps} {...baseComponentProps} >
          {
            data.map(option => {
              if (option.data && !option.data.length || (option.renderData && option.keys)) {
                return (
                  <SubMenu
                    renderData={option.renderData}
                    elmId={option.elmId}
                    key={uuid()}
                    onClickHandler={option.onClickHandler}
                    componentProps={
                      {
                        label: option.label,
                        disabled: option.disabled,
                        data: option.data,
                        keys: option.keys,
                      }
                    }
                  />
                )
              }
              else {
                return (
                  <MenuItem
                    key={uuid()}
                    onClickHandler={onClickHandler}
                    onClickParams={option.onClickParams}
                    componentProps={{
                      disabled: option.disabled,
                    }}
                  >
                    {option.label}
                  </MenuItem>
                )
              }
            })
          }
        </AntMenu>
      </DefaultStyle>
    );
  }
}

export default Menu;
