import React, { PureComponent } from 'react';
import { DefaultStyle } from './Styles';
import PropTypes from 'prop-types';
import { Radio as AntRadio } from 'antd';

class Radio extends PureComponent {

  static propTypes = {
    componentProps: PropTypes.object,
  };

  handleChange = (e) => {
    this.props.onChangeHandler && this.props.onChangeHandler(e.target.checked);
  }

  render() {
    const { componentProps, onChangeHandler, elmId, renderData } = this.props;
    const { label, value, options, ...baseComponentProps } = componentProps
    if (!options || !options.length)
      return null;

    return (
      <DefaultStyle>
        <AntRadio {...baseComponentProps} options={options} value={value} onChange={this.handleChange}>{label}</AntRadio>
      </DefaultStyle>
    );
  }
}

export default Radio;
