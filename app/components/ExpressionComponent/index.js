import React, { PureComponent } from 'react';
import expr from 'expr-eval';
import componentMap from 'configs/componentMapByFormType';
import { getMutatedFormType } from 'utils/apiUtility';
import PropTypes from 'prop-types';

/**
 * Expression Component
 */

class ExpressionComponent extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      value: null,
    };
  }

  static propTypes = {
    componentProps: PropTypes.object,
    children: PropTypes.node,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    /*
    * Evaluates the expression and returns the state is result is updated
    * */
    const { componentProps: { formValues }, renderData: {params: {meta: { expression, dependsOn }}} } = nextProps;
    const Parser = expr.Parser;
    let expressionDict = {};
    dependsOn.forEach(field => {
      expressionDict[field] = formValues[field] || 0;
    });
    const parsedValue = Parser.evaluate(expression, expressionDict).toString();
    if( isFinite(parsedValue) && (parsedValue !== prevState.value)) {
      return { value: parsedValue };
    }
    return null;
  }

  componentDidUpdate(prevProps, prevState) {
    /*
    * Updates the new value for the parent if there is a change in state value
    * */
    if (prevState.value !== this.state.value) {
      this.props.onChangeHandler && this.props.onChangeHandler(this.state.value);
    }
  }

  render() {
    /*
    * Renders the previously hijacked unitComponent,
    * passes the updated value to it too.
    * */
    const { componentProps: { readMode }, renderData, componentProps, ...otherProps  } = this.props;
    const { params: { meta :{ form_type: formType } } } =  renderData;
    const mutatedFormType = getMutatedFormType(formType, readMode);
    const UnitComponent = componentMap[mutatedFormType];
    return UnitComponent ? (
      <UnitComponent
        renderData={renderData}
        {...otherProps}
        componentProps={{ ...componentProps, value: this.state.value }}
      />
    ) : null;
  }
}

export default ExpressionComponent;
