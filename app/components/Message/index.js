import { message } from 'antd';

export const showInfoMessage = (props = {}) => {
  const { content, type = 'open', duration = 1.5, onClose } = props;
  //console.log('in showInfoMessage', content, type, duration);
  message[type](content, duration, onClose && onClose());
};