import React, { PureComponent } from 'react';
import { Form as AntForm } from 'antd';
import { DefaultStyle } from './Styles';

class Form extends PureComponent {
  // eslint-disable-line react/prefer-stateless-function

  render() {
    const { componentProps } = this.props;
    const { ...baseComponentProps } = componentProps;
    return (
      <DefaultStyle>
        <AntForm {...baseComponentProps}>
          {this.props.children}
        </AntForm>
      </DefaultStyle>
    );
  }
}

export default Form;
