import React, { PureComponent } from 'react';
import { Row as AntRow } from 'antd';
import PropTypes from 'prop-types';
import { DefaultStyle } from './Styles';

/**
 * Row Component, Ant's Grid Layout
 */
class Row extends PureComponent {
  static propTypes = {
    componentProps: PropTypes.object,
    children: PropTypes.node,
  };

  render() {
    const { componentProps } = this.props;
    const { ...baseComponentProps } = componentProps;
    return (
        <AntRow {...baseComponentProps}>{this.props.children}</AntRow>
    );
  }
}

export default Row;
