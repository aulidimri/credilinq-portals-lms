import React, { PureComponent } from 'react';
import { Col as AntCol } from 'antd';
import PropTypes from 'prop-types';
import { DefaultStyle } from './Styles';

/**
 * Grid Column
 */
class Col extends PureComponent {
  static propTypes = {
    children: PropTypes.node,
  };

  render() {
    const { componentProps } = this.props;
    const { ...baseComponentProps } = componentProps;
    return (
        <AntCol {...baseComponentProps}>{this.props.children}</AntCol>
    );
  }
}

export default Col;
