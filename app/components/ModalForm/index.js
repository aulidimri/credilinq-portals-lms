/*
* Modal form, triggered by triggerConfig
* */

import React, { PureComponent } from 'react';
import Modal from 'components/Modal';

class ModalForm extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      relatedFieldIdsToCheck: [],
      isDataSame: false,
    };
  }

  componentDidMount() {
    /*
    * Add relatedFieldIdsToCheck to state,
    * Initially, Check popup showing conditions and update accordingly.
    * */
    const {
      renderData: {
        params: {
          meta: { triggerConfig },
        },
      },
    } = this.props;
    const relatedFieldIdsToCheck = Object.keys(triggerConfig);
    this.setState({ relatedFieldIdsToCheck });
    window.setTimeout(() => {
      this.checkFieldUpdateValidation(this.props);
    });
  }

  isFormDataSame = (oldData, newData) => {
    /*
    * Returns isDataSame as a performance guard condition,
    * works well when only non-actions fields are involved,
    * as action fields are always reset after modal close.
    * */
    const { relatedFieldIdsToCheck } = this.state;
    let isDataSame = !!relatedFieldIdsToCheck.length;
    const {
      componentProps: { formValues: newFormValues },
    } = newData;
    const {
      componentProps: { formValues: oldFormValues },
    } = oldData;

    if (this.areFormIdsPresent(newData)) {
      this.state.relatedFieldIdsToCheck.some(fieldId => {
        if (
          !oldFormValues ||
          newFormValues[fieldId] !== oldFormValues[fieldId]
        ) {
          isDataSame = false;
        }
      });
    }
    return isDataSame;
  };

  areFormIdsPresent = newProps => {
    /*
    * Checks if all the form ids are present or not.
    * */
    const newFormData = newProps.componentProps.formValues;
    let arePresent = false;
    const formIds = newFormData && Object.keys(newFormData);
    const { relatedFieldIdsToCheck } = this.state;

    if (formIds && formIds.length) {
      relatedFieldIdsToCheck.some(id => {
        if (formIds.indexOf(id) !== -1) {
          arePresent = true;
        }
      });
    }

    return arePresent;
  };

  componentDidUpdate(prevProps) {
    /*
     Check modal visibility conditions on props update,
     Note this.props are the newProps.
     */
    const isDataSame = this.isFormDataSame(prevProps, this.props);
    this.setState({ isDataSame }, () => {
      !this.state.isDataSame && this.checkFieldUpdateValidation(this.props, prevProps);
    });
  }

  checkFieldUpdateValidation = (newProps, prevProps) => {
    /*
     * Compare field values and determines if modal should be visible or not.
     */
    const {
      componentProps: { formValues: newFormValues },
    } = newProps;
    const {
      componentProps: { formValues: oldFormValues },
    } = prevProps || newProps; // As if its the initial case there are no previous props.

    const meta = newProps.renderData && newProps.renderData.params.meta;
    const newFormData = newProps.componentProps.formValues;
    const { triggerConfig } = meta;
    const { relatedFieldIdsToCheck, isDataSame } = this.state;

    if (
      relatedFieldIdsToCheck &&
      relatedFieldIdsToCheck.length &&
      !isDataSame
    ) {
      relatedFieldIdsToCheck.forEach(fieldId => {
        const validValues = triggerConfig && triggerConfig[fieldId];
        if (
          newFormValues[fieldId] !== oldFormValues[fieldId] && // additional check for particular value field change
          validValues &&
          Array.isArray(validValues) &&
          validValues.indexOf(newFormData[fieldId]) !== -1
        ) {
          this.props.onChangeHandler(true);
        }
      });
    }
  };

  onCancelHandler = () => {
    /*
    * Handles the modal cancellation,
    * creates a reset value object and pass it to parent,
    * to update custom values (like action's 'triggered' values) and modal visibility state.
    * */
    const {
      componentProps: { tabData, updateMultipleField, formValues },
      elmId,
    } = this.props;
    const resetValueSet = {};
    // Making reset values for actions, to update in parent
    this.state.relatedFieldIdsToCheck
      .filter(
        fieldId =>
          tabData[fieldId].params.meta.type === 'action' &&
          formValues[fieldId] === 'triggered',
      )
      .forEach(fieldId => {
        resetValueSet[fieldId] = '';
      });
    // adding modal field to reset values as well.
    updateMultipleField({ ...resetValueSet, [elmId]: false });
  };

  render() {
    const { componentProps, ...otherProps } = this.props;
    return (
      <Modal
        {...otherProps}
        componentProps={{
          ...componentProps,
          visible: !!componentProps.value, // Mapping value to visibility explicitly
        }}
        onCancelHandler={this.onCancelHandler}
      />
    );
  }
}

export default ModalForm;
