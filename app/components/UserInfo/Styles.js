import styled from 'styled-components';
import { sizes, colors, fonts } from 'configs/styleVars';
export const DefaultStyle = styled.div`
  display: flex;
  align-items: center;
  padding: 0 ${sizes.paddingS};
`;
