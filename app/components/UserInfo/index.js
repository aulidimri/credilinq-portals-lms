import React, { PureComponent } from 'react';
import Dropdown from 'components/Dropdown';
import Icon from 'components/Icon';
import { logout } from 'containers/App/actions';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { DefaultStyle } from './Styles';

/**
 * UserInfo Component that displays user information and handles logic for logout
 */
class UserInfo extends PureComponent {
  static propTypes = {
    componentProps: PropTypes.object,
    children: PropTypes.node,
    userInfo: PropTypes.object,
  };

  onClickHandler = (e, args) => {
    console.log("UserInfo", args);
    switch (args) {
      case "logout":
        this.props.dispatch(logout());
        break;
    }
  };



  render() {
    const { userInfo = {}, className } = this.props

    const userMenuData = [
      {
        label: "Logout",
        onClickParams: ["logout",]
      },
    ];

    return (
      <div className={className}>
        <Dropdown
          onClickHandler={this.onClickHandler}
          componentProps={{
            data: userMenuData,
            placement: 'bottomLeft',
            mode: 'vertical',
            trigger: ['click'],
          }}
        >
          <Icon componentProps={{ label: userInfo.username, type: 'caret-down', prefix: true }} />
        </Dropdown>
      </div>
    );
  }
}

export default connect()(UserInfo);
