import React, { PureComponent } from 'react';
import { List as AntList } from 'antd';
import PropTypes from 'prop-types';
import { DefaultStyle } from './Styles';

const { Item: AntListItem } = AntList;

/**
 * ListItem Component, can only be inside a List Component
 */
class ListItem extends PureComponent {
  static propTypes = {
    componentProps: PropTypes.object,
    children: PropTypes.node,
  };

  render() {
    const { componentProps } = this.props;
    return (
      <DefaultStyle>
        <AntListItem {...componentProps}>{this.props.children}</AntListItem>
      </DefaultStyle>
    );
  }
}

export default ListItem;
