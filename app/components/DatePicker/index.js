import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { DatePicker as AntDatePicker } from 'antd';
import { DefaultStyle } from './Styles';
import moment from 'moment';

class DatePicker extends PureComponent {

  static propTypes = {
    onChangeHandler: PropTypes.func,
  }

  static defaultProps = {
    componentProps: {},
  }

  handleChange = (date, dateString) => {
    const { onChangeHandler } = this.props;
    onChangeHandler && onChangeHandler(dateString);
  }

  static convertToMoment(dateString) {
    return moment(dateString, 'DD-MM-YYYY')
  }

  disableThisDate = (dateToCheck) => {
    const { componentProps } = this.props;
    const { maxDate, minDate } = componentProps;

    const minDateMoment = DatePicker.convertToMoment(minDate);
    const maxDateMoment = DatePicker.convertToMoment(maxDate);
    let minDateCheck = !!minDate && dateToCheck.isBefore(minDateMoment, 'day');
    let maxDateCheck = !!maxDate && dateToCheck.isAfter(maxDateMoment, 'day');

    return minDateCheck || maxDateCheck;
  }

  render() {
    const { componentProps, onChangeHandler, elmId, renderData } = this.props;
    const { maxDate, minDate, value, ...baseComponentProps } = componentProps;

    const dateValue = value ? DatePicker.convertToMoment(value) : undefined;
    return (
      <DefaultStyle>
        <AntDatePicker
          {...baseComponentProps}
          value={dateValue}
          format="DD-MM-YYYY"
          disabledDate={this.disableThisDate}
          onChange={this.handleChange}
        />
      </DefaultStyle>
    )
  }
}

export default DatePicker;

////  <DatePicker componentProps={{maxDate: moment('2019-05-15', 'YYYY-MM-DD'), minDate: moment('2019-05-01', 'YYYY-MM-DD'),}} />
