import React, { PureComponent } from 'react';
import Select from 'components/Select';
import PropTypes from 'prop-types';
import { DefaultStyle } from './Styles';
import uuid from 'uuid/v4';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { networkAction } from 'containers/App/actions';
import dottie from 'dottie';
/**
 * RestSelect Component
 */
class RestSelect extends PureComponent {
  // eslint-disable-line react/prefer-stateless-function
  state = {
    options: []
  }
  static propTypes = {
    componentProps: PropTypes.object,
    children: PropTypes.node,
  };

  handleChange = (value) => {
    this.props.onChangeHandler && this.props.onChangeHandler(value)
  }

  componentDidMount() {
    const { renderData: { params: {meta}, id: elmId}, componentProps : { keys } } = this.props;
    const { api : { method, subUrl, valuePath, requestPayload, defaultComment, successActions = [], errorActions = [] } } = meta;

    let data = {};
    Object.keys(requestPayload).forEach(key => {
      let nestedObj = {};
      Object.keys(requestPayload[key]).length && Object.keys(requestPayload[key]).forEach(i => {
        nestedObj[i] = requestPayload[key][i];
      })
      data[key] = nestedObj;
    })

    const formData = {
      api: 'configApi',
      method: 'POST',
      params: { subUrl },
      queryParams: keys,
      data,
      successCallback: (res) => {
        //let mapping = dottie.get(meta.api["options-mapping"], `${elmId}.id`.toString());
        let mapping = meta.api["options-mapping"];

        console.log('values back in rest', res, valuePath, dottie.get(res.data, valuePath), mapping, `${elmId}.id`);

        let dataOptions = dottie.get(res.data, valuePath);
        let options = dataOptions.map(option => {
          return {
            value: option[mapping[`${elmId}.id`]] || option,
            label: option[mapping[`${elmId}.label`]] || option,
          }
        });
        console.log('rest-select response', res, options);
        this.setState({ options })
      },
      failCallback: (err) => {
      }
    };
    this.props.networkAction(formData);

  }


  render() {
    const { componentProps, onChangeHandler, elmId, renderData, ...antProps } = this.props;
    const { keys, ...baseComponentProps } = componentProps;
    const { options } = this.state;
    console.log('in RestSelect', this.props);
    if (!options || !options.length)
      return null

    return (
      <DefaultStyle>
        <Select
          componentProps = {{options: options, ...baseComponentProps}}
          onChangeHandler={onChangeHandler}
          renderData={renderData}></Select>
      </DefaultStyle>
    );
  }
}

const mapStateToProps = createStructuredSelector({
});

function mapDispatchToProps(dispatch) {
  return {
    networkAction: (networkData) => dispatch(networkAction(networkData)),
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);


export default withConnect(RestSelect);

