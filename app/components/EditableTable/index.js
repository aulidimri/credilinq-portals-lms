/**
 * EditableTable Component
 */

import React, { PureComponent } from 'react';
import { Table as AntTable, Popconfirm, Icon } from 'antd';
import propsMap from 'configs/propsMapByFormType';
import componentMap from 'configs/componentMapByFormType';
import _ from 'lodash';
import Button from '../Button';
import { DefaultStyle, AddBtnWrapper, disabledIconStyle , activeIconStyle } from './Styles';
import FormItem from '../FormItem';
import { layoutConfig } from '../../utils/config';
import { getMutatedFormType } from '../../utils/apiUtility';

const EditableContext = React.createContext();

const EditableRow = props => <tr {...props} />;

class EditableCell extends PureComponent {
  /*
    Handles the rendering of each cell be it editable, or action cell.
  */
  save = (event, elmId, value) => {
    /*
     Capture the value form the unitComponent and pass it on
     to update the row and hence table dataSource
     */
    const { record, handleSave } = this.props;
    // validation logic can be added here.
    handleSave(event, { ...record, [elmId]: value });
  };

  getExtraProps = (elmId, renderData, formType) => {
    /*
    * Todo to be added as a util
    * */
    const meta = renderData && renderData.params.meta;
    let extraProps = null;
    switch (formType) {
      case 'dropdown':
      case 'multiselect-dropdown':
        const options =
          (Array.isArray(renderData.enumValues) &&
            renderData.enumValues.map(({ id, name }) => ({
              id,
              label: name,
            }))) ||
          [];
        extraProps = {
          options,
        };
        break;
      case 'string':
        extraProps = {
          className: 'ant-form-text',
        };
        break;
      default:
        extraProps = {};
    }
    return {
      ...extraProps,
    };
  };

  render() {
    const {
      editable,
      dataIndex,
      title,
      record,
      fieldRenderData,
      handleSave,
      isReadMode,
      renderData,
      value,
      ...restProps
    } = this.props;
    let dataCell;
    if (!editable) {
      dataCell = <td {...restProps}>{restProps.children}</td>;
    } else {
      const {
        id: elmId,
        params: { meta },
        ...fieldData
      } = fieldRenderData;
      const formType =
        getMutatedFormType(meta.form_type, isReadMode()) ||
        (meta && meta.form_type);
      const extraProps = this.getExtraProps(elmId, fieldRenderData, formType);
      const compProps =
        propsMap[formType] &&
        propsMap[formType](
          fieldRenderData,
          elmId,
          record[elmId] || '',
          isReadMode(),
        );
      const UnitComponent = componentMap[formType];
      if (!UnitComponent) {
        return null;
      }
      const unitComponent = (
        <UnitComponent
          componentProps={{ ...compProps, ...extraProps, value: record[elmId] }}
          key={elmId}
          renderData={ fieldRenderData }
          onChangeHandler={(val) => {
            this.save(null, elmId, val);
          }}
        />
      );
      const formItemLayout = {
        wrapperCol: { span: 24 },
      };
      dataCell = (
        <td {...restProps}>
          <EditableContext.Consumer>
            {form => {
              this.form = form;
              return (
                <FormItem
                  componentProps={{...formItemLayout}}
                >
                  {unitComponent}
                </FormItem>
              );
            }}
          </EditableContext.Consumer>
        </td>
      );
    }

    return dataCell;
  }
}

class EditableTable extends PureComponent {
  state = {
    columns: [],
    dataSource: [],
    count: 0,
  };

  handleDelete = key => {
    /*
    * Handles the deletion of a row based on 'key' received, hence updating the table's dataSource.
    * */
    const dataSource = [...this.state.dataSource];
    this.setState(
      { dataSource: dataSource.filter(item => item.key !== key) },
      () => this.handleChange(null, this.state.dataSource),
    );
  };

  handleAdd = () => {
    /*
   * Handles the addition of a row, hence updating the table's dataSource.
   * */
    const { count, dataSource, emptyRowData } = this.state;
    const newData = {
      key: count,
      ...emptyRowData,
    };
    this.setState({
      dataSource: [...dataSource, newData],
      count: count + 1,
    });
  };

  handleSave = (event, row) => {
    /*
    * Gets called on each cell's change/save,
    * Updates the table's data,
    * */
    const newData = _.cloneDeep(this.state.dataSource);
    const index = newData.findIndex(item => row.key === item.key);
    const item = newData[index];
    newData.splice(index, 1, {
      ...item,
      ...row,
    });
    this.setState({ dataSource: newData });
    this.handleChange(event, newData);
  };

  handleChange = (e, value) => {
    /*
    * Send teh updated dataSource to parent.
    * */
    this.props.onChangeHandler &&
    this.props.onChangeHandler(JSON.stringify(value));
  };

  componentDidMount() {
    this.buildInstanceRenderData();
  }

  buildInstanceRenderData = () => {
    /*
    * Initial function call, sets up everything in state :
    * dataSource, columns,
    * adds Actions
    * */
    const {
      renderData: {
        id,
        name,
        value: tableValue,
        params: {
          meta: { fields, maxCount, allowAdd, allowDelete },
          meta: tableMeta,
        },
      },
      renderData,
      componentProps: { tabData },
    } = this.props;

    const fieldsArray = fields.map(({ fieldId }) => tabData[fieldId]);
    const columns = fieldsArray.map(field => {
      const {
        name: title,
        value,
        default: defaultValue,
        id: key,
        params: {
          meta: { colWidth },
        },
      } = field;
      return {
        title,
        dataIndex: key,
        key,
        value: value || defaultValue || null,
        width: colWidth * layoutConfig.tableWidthUnit,
        onCell: record => ({
          record,
          fieldRenderData: field,
          editable: true,
          dataIndex: key,
          handleSave: this.handleSave,
          isReadMode: this.isReadMode,
          value: value || defaultValue || null,
        }),
      };
    });

    allowDelete &&
    columns.push({
      title: 'Actions',
      dataIndex: 'operation',
      render: (text, record) =>
        this.isDeleteAllowed() ? (
          <Popconfirm
            title="Sure to delete?"
            onConfirm={() => this.handleDelete(record.key)}
          >
            <Icon type="delete" style={activeIconStyle} />
          </Popconfirm>
        ) : (<Icon type="delete" style={disabledIconStyle} />),
    });

    let emptyRowData = {};
    // const dataSource = [];
    fieldsArray.forEach(field => {
      const { id: elmId, value } = field;
      emptyRowData = {
        ...emptyRowData,
        [elmId]: value,
      };
    });

    // console.group(id, name);
    // console.log('EditableTableProps', this.props);
    // console.log('TableRenderData', renderData);
    // console.log('TableMetaData', tableMeta);
    // console.log('TableFields', fields);
    // console.log('TabData', tabData);
    // console.log('FieldArray', fieldsArray);
    // console.log('Columns', columns);
    // console.log('EmptyRowData', emptyRowData);
    // console.log(
    //   'MaxCount, AllowAdd, AllowDelete',
    //   maxCount,
    //   allowAdd,
    //   allowDelete,
    // );
    // console.groupEnd();

    this.setState(
      { columns, emptyRowData, maxCount, allowAdd, allowDelete },
      () => {
        this.setTableData(tableValue);
      },
    );
  };

  setTableData(value) {
    /*
    * Updates's the initial tableSource, based on the 'allowAdd' flag, updates the count
    * as the initial value to be used for further row IDS.
    * */
    const tableValue =
      value && typeof value === 'string' ? JSON.parse(value) : value;
    if (!tableValue || !tableValue.length || tableValue === '') {
      this.state.allowAdd && this.handleAdd();
    } else {
      this.setState({
        dataSource: tableValue,
        count: 1 + Math.max(...tableValue.map(row => +row.key)),
      });
    }
  }

  isDeleteAllowed = () => {
    const { dataSource, allowDelete } = this.state;
    return !this.isReadMode() && allowDelete && dataSource.length > 1;
  };

  isAddAllowed = () => {
    const { dataSource, maxCount, allowAdd } = this.state;
    return !this.isReadMode() && allowAdd
      ? maxCount
        ? dataSource.length < maxCount
        : true
      : false;
  };

  isReadMode = () => {
    const {
      componentProps: { readMode },
    } = this.props;
    return readMode;
  };

  render() {
    /*
    * Configures cell and row component for the table
    * Renders Table and addRow button too */
    const components = {
      body: {
        row: EditableRow,
        cell: EditableCell,
      },
    };
    const { dataSource, columns } = this.state;
    return (
      <DefaultStyle>
        <AntTable
          dataSource={dataSource}
          columns={columns}
          components={components}
          rowClassName={() => 'editable-row'}
          bordered
        />
        {this.isAddAllowed() ? (
          <AddBtnWrapper>
            <Button
              onClick={this.handleAdd}
              type="primary"
            >
              Add a row
            </Button>
          </AddBtnWrapper>
        ) : null}
      </DefaultStyle>
    );
  }
}

export default EditableTable;
