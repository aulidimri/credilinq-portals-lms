import styled from 'styled-components';
import { colors, sizes, globalColors, fonts } from 'configs/styleVars';

export const DefaultStyle = styled.div`
  display: flex;
  flex-direction: column;
  
  .ant-table-thead > tr > th {
    background-color: ${colors.primaryComponentBG};
    color: ${colors.primaryComponentColor};
    padding: ${sizes.paddingS}
  }

  .ant-table-tbody > tr > td {
    padding: ${sizes.paddingS};
  }

  .ant-pagination {
    float: none;
    display: none;
    justify-content: start;
  }

  .ant-pagination li {
    order: 2;
  }

  .ant-pagination li:last-child {
    order: 1;
    margin-right: auto;
    margin-left: 0;
  }

  .tableActions {
    display: flex;
  }

  .bulkActionsBar {
    display: flex;
  }

  .bulkActionsBar > * {
    margin: ${sizes.marginS};
    margin-left: 0;
  }


  .tableActionsBar {
    display: flex;
    justify-content: space-between;
  }
  
  .editable-cell {
    position: relative;
  }
  
  .editable-cell-value-wrap {
    padding: 5px 12px;
    cursor: pointer;
  }
  
  .editable-row:hover .editable-cell-value-wrap {
    border: 1px solid #d9d9d9;
    border-radius: 4px;
    padding: 4px 11px;
  }
  
  td .ant-form-item{
    margin-bottom: 0px;
  }
  
`;

export const AddBtnWrapper = styled.div`
  text-align: right;
  padding: 16px 10px;
`;

export const disabledIconStyle = {
  color: globalColors.lightGrey,
  fontSize: fonts.fontSizeS,
};

export const activeIconStyle = {
  color: globalColors.primaryColor,
  fontSize: fonts.fontSizeS,
};
