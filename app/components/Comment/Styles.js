import styled from 'styled-components';
import { globalColors, sizes, colors, fonts } from 'configs/styleVars'

export const DefaultStyle = styled.div`
  position: relative;
  .ant-comment-inner {
    padding-top: 0;
  }

  .comment-tab-tag-container {
    display: flex;
    justify-content: flex-end;
    position: absolute;
    right: 0;
    top: 0.5rem;
  }

  .comment-tab-tag {
    padding: 0 ${sizes.paddingS};
    font-size: ${fonts.fontSizeXS};
    background-color: ${colors.primaryComponentBG + "77"};
    color: ${globalColors.white};
  }
`;
