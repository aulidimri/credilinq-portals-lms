import React, { PureComponent } from 'react';
import { Comment as AntComment } from 'antd';
import { DefaultStyle } from './Styles';
import { Avatar } from 'antd';
import moment from 'moment';
import PropTypes from 'prop-types';

class Comment extends PureComponent {

  static propTypes = {
    componentProps: PropTypes.shape({
      comment: PropTypes.shape({
        avatar: PropTypes.string,
        message: PropTypes.string,
        time: PropTypes.string,
        tab: PropTypes.string,
      }),
    }),
  }

  render() {
    const { componentProps } = this.props;
    const { comment = {}, ...baseComponentProps } = componentProps;
    const { author, avatar, content, tab, time } = comment;
    const datetime = time && moment(time).format("D MMM YYYY, h:mm a");

    return (
      <DefaultStyle>
        {
          tab && (
          <div className="comment-tab-tag-container">
            <span className="comment-tab-tag">
              {tab}
            </span>
          </div>)
        }
        <AntComment
          author={(<b>{author}</b>)}
          avatar={(<Avatar size="small" icon={avatar || "user"} />)}
          content={content}
          datetime={datetime}
          {...baseComponentProps}
        >
          {this.props.children}
        </AntComment>
      </DefaultStyle>
    )
  }
}

export default Comment;
