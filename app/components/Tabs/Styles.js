import styled from 'styled-components';
import {sizes} from 'configs/styleVars'

export const DefaultStyle = styled.div`
  .ant-tabs-bar {
    margin: 1px 0;
  }

  .ant-tabs-ink-bar {
    height: 3px;
  }

  .ant-tabs-nav .ant-tabs-tab-active {
    font-weight: 600;
  }
`;
