import React, { Component } from 'react';
import { Tabs as AntTabs } from 'antd';
import PropTypes from 'prop-types';
import { DefaultStyle } from './Styles';

const AntTabPane = AntTabs.TabPane;

class Tabs extends Component {

  onTabChange = (key) => {
    //console.log(key);
    const { callBack, callBackParams } = this.props;
    if (callBack) callBack(key, callBackParams && callBackParams.length && [...callBackParams]);
  }

  render() {
    const { componentProps, children, tabs } = this.props;
    if (!tabs.length) return null;
    return (
      <DefaultStyle>
        <AntTabs {...componentProps} onChange={this.onTabChange}>
          {
            tabs.map((tab) => {
              return <AntTabPane tab={tab.heading} key={tab.key}>{tab.children}</AntTabPane>
            })
          }
        </AntTabs>
      </DefaultStyle>
    );
  }
}

Tabs.propTypes = {
  callBack: PropTypes.func,
  //TODO add other proptypes
};

export default Tabs;
