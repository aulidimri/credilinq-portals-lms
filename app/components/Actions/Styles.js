import styled from 'styled-components';
import { colors, fonts, sizes } from 'configs/styleVars';

export const DefaultStyle = styled.div`
  display: flex;
  align-items: center;
  font-size: ${fonts.fontSizeM};
  > * {
    margin: ${sizes.marginS};
    margin-left: 0;
  }

  .ant-btn {
    padding: 0 ${sizes.paddingS};
  }
`;
