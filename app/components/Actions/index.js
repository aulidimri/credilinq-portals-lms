import React, { PureComponent } from 'react';
import Button from 'components/Button';
import Icon from 'components/Icon';
import Dropdown from 'components/Dropdown';
import PropTypes from 'prop-types';
import { DefaultStyle } from './Styles';

import uuid from 'uuid';

class Actions extends PureComponent {
  static propTypes = {
    componentProps: PropTypes.object,
    children: PropTypes.node,
  };

  renderActions = (actions, actionsToDisplay) => {
    const maxActionsToDisplay = 3;

    actionsToDisplay = Math.min(actionsToDisplay || actions.length, maxActionsToDisplay);
    if (actions.length >= maxActionsToDisplay)
      actionsToDisplay = maxActionsToDisplay - 1;
    const actionsToDisplayAsButtons = actions.slice(0, actionsToDisplay);
    const actionsToDisplayAsDropdown = actions.slice(actionsToDisplay);

    //Render as actions
    const res = actionsToDisplayAsButtons.map(action => {
      if (action.data && !action.data.length || (action.renderData && action.keys)) {
        return (
          <Dropdown
            key={uuid()}
            renderData={action.renderData}
            elmId={action.elmId}
            onClickHandler={action.onClickHandler}
            componentProps={{
              data: action.data,
              keys: action.keys,
              placement: 'bottomRight',
              trigger: ['click'],
            }}
          >
            <Button componentProps={{label: action.label}} />
          </Dropdown>
        )
      }

      return (
        <Button
          key={uuid()}
          onClickHandler={this.props.onClickHandler}
          onClickParams={action.onClickParams}
          componentProps={{ label: action.label, type: "primary", disabled: action.disabled }}
        />
      )
    });
    //Render the rest as dropdown options
    if (actionsToDisplayAsDropdown.length) {
      res.push(
        <Dropdown
          key={uuid()}
          onClickHandler={this.props.onClickHandler}
          componentProps={{
            data: actionsToDisplayAsDropdown,
            placement: 'bottomRight',
            trigger: ['click'],
          }}
        >
          <Button>
            <Icon
              componentProps={{
                type: 'more',
              }}
            />
          </Button>
        </Dropdown>
      );
    }

    return res;
  }

  render() {
    const { componentProps, onClickHandler, data } = this.props;
    const { actionsToDisplay, ...baseComponentProps } = componentProps;

    return (
      <DefaultStyle>
        {this.renderActions(data, actionsToDisplay)}
      </DefaultStyle>
    );
  }
}

export default Actions;
