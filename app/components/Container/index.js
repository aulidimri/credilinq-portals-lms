import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { DefaultStyle } from './Styles';

/**
 * Acts as the container for the whole page if you want to center whatever nodes that are inside it.
 */
class Container extends PureComponent {
  static propTypes = {
    children: PropTypes.node,
  };

  render() {
    return <DefaultStyle>{this.props.children};</DefaultStyle>;
  }
}

export default Container;
