import styled from 'styled-components';
import { colors } from 'configs/styleVars';

export const DefaultStyle = styled.div`
  background-color: ${colors.white};
  display: flex;
  min-height: 100vh;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;
