import React, { PureComponent } from 'react';
import { Select as AntSelect } from 'antd';
import PropTypes from 'prop-types';
import { DefaultStyle } from './Styles';
import uuid from 'uuid';

const Option = AntSelect.Option;
/**
 * Select Component
 */
class Select extends PureComponent {
  // eslint-disable-line react/prefer-stateless-function

  static propTypes = {
    componentProps: PropTypes.object,
    children: PropTypes.node,
  };

  handleChange = (value) => {
    this.props.onChangeHandler && this.props.onChangeHandler(value)
  }


  render() {
    const { componentProps, onChangeHandler, elmId, renderData, ...antProps } = this.props;
    const { options, ...baseComponentProps } = componentProps;

    if (!options || !options.length)
      return null

    return (
      <DefaultStyle>
        <AntSelect {...antProps} {...baseComponentProps} onChange={this.handleChange}>
          {
            options.map(option => <Option key={uuid()} value={option.value || option.id}>{option.label}</Option>)                // TODO Destructure the options object
          }
        </AntSelect>
      </DefaultStyle>
    );
  }
}

export default Select;
