import React from 'react';
import { Spin as AntSpin } from 'antd'
import { DefaultStyle } from './Styles';

const LoadingIndicator = () => (
  <DefaultStyle>
    <AntSpin className="loader-icon" size="large" />
  </DefaultStyle>
);

export default LoadingIndicator;
