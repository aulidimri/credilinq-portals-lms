import styled from 'styled-components';

export const DefaultStyle = styled.div`
  position: fixed;
  z-index: 2;
  height: 100%;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: rgba(255, 255, 255, 0.7);
`;
