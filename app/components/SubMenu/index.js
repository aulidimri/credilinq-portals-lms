import React, { PureComponent } from 'react';
import { Menu as AntMenu } from 'antd';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { networkAction } from 'containers/App/actions';
import PropTypes from 'prop-types';
import { DefaultStyle } from './Styles';
import MenuItem from 'components/MenuItem';
import uuid from 'uuid';
import dottie from 'dottie';





/**
 * Menu Component
 */
const { SubMenu: AntSubMenu } = AntMenu;
class SubMenu extends PureComponent {
  state = {
    data: []
  }

  static propTypes = {
    componentProps: PropTypes.object,
    children: PropTypes.node,
  };

  componentDidMount() {
    const { renderData, elmId, componentProps = {} } = this.props;
    const { keys } = componentProps;
    const meta = dottie.get(renderData, 'params.meta');
    if (!meta)
      return;

    const { api: { method, subUrl, requestPayload, defaultComment, successActions = [], errorActions = [] } } = meta;
    if (!subUrl)
      return;

    let data = {};
    Object.keys(requestPayload).forEach(key => {
      let nestedObj = {};
      Object.keys(requestPayload[key]).length && Object.keys(requestPayload[key]).forEach(i => {
        nestedObj[i] = requestPayload[key][i];
      })
      data[key] = nestedObj;
    })

    //Discuss whether to move this logic to the container or not since it required onClickHandler as prop
    const formData = {
      api: 'configApi',
      method: 'POST',
      params: { subUrl },
      queryParams: keys,
      data,
      successCallback: (res) => {
        let mapping = meta.api["options-mapping"][elmId];
        const createOptions = option => {
          return {
            label: option,
            onClickParams: [renderData, option],
          }
        }
        let options = dottie.get(res, `data.response.${mapping}`);
        options = options && options.map(createOptions);
        console.log('rest-submenu response', res, options);
        this.setState({ data: options })
      },
      failCallback: (err) => {
      }
    };
    this.props.networkAction(formData);
  }

  render() {
    // antProps contain the unidentified props that are passed by the Ant Design Framework by parent components like Dropdown, Select, etc.
    const { componentProps, onClickHandler, renderData, elmId, dispatch, networkAction, ...antProps } = this.props;
    const { data, label, disabled, keys, ...baseComponentProps } = componentProps;

    let options = data;
    if (!data.length) {
      options = this.state.data;
    }
    options = options.map(option => ({
      ...option,
      onClickHandler,
    }));

    return (
      <DefaultStyle>
        <AntSubMenu title={label} {...antProps} {...baseComponentProps} >
          {
            options.map(option => {
              if (option.data && !option.data.length || (option.renderData && option.keys)) {
                return (
                  <SubMenu
                    renderData={option.renderData}
                    elmId={option.elmId}
                    key={uuid()}
                    onClickHandler={option.onClickHandler}
                    componentProps={
                      {
                        label: option.label,
                        disabled: option.disabled,
                        data: option.data,
                        keys: option.keys,
                      }
                    }
                  />
                )
              }
              else {
                return (
                  <MenuItem
                    key={uuid()}
                    onClickHandler={onClickHandler}
                    onClickParams={option.onClickParams}
                    componentProps={{
                      disabled: option.disabled,
                    }}
                  >
                    {option.label}
                  </MenuItem>
                )
              }
            })
          }
        </AntSubMenu>
      </DefaultStyle>
    );
  }
}



const mapStateToProps = createStructuredSelector({
});

function mapDispatchToProps(dispatch) {
  return {
    networkAction: (networkData) => dispatch(networkAction(networkData)),
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);


export default withConnect(SubMenu);
