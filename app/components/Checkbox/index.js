import React, { PureComponent } from 'react';
import { DefaultStyle } from './Styles';
import PropTypes from 'prop-types';
import { Checkbox as AntCheckbox } from 'antd';

class Checkbox extends PureComponent {

  static propTypes = {
    componentProps: PropTypes.object,
  };

  static defaultProps = {
    componentProps: {},
  }

  handleChange = (e) => {
    this.props.onChangeHandler && this.props.onChangeHandler(e.target.checked);
  }

  render() {
    const { componentProps, onChangeHandler, elmId, renderData } = this.props;
    const { label, value, prefix, ...baseComponentProps } = componentProps;

    //TODO: checkbox does not accommodate for prefix/suffix yet
    return (
      <DefaultStyle isPrefix={prefix}>
        <AntCheckbox {...baseComponentProps} value={value} onChange={this.handleChange}><span className="checkbox-component-label">{label}</span></AntCheckbox>
      </DefaultStyle>
    );
  }
}

export default Checkbox;
