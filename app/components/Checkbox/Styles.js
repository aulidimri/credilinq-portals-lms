import styled from 'styled-components';
import { colors } from 'configs/styleVars';

export const DefaultStyle = styled.div`

  .ant-checkbox-wrapper {
    // display: flex;
    // align-items: center;
    // margin: 0;
  }
  .ant-checkbox {
    order: ${props => (props.isPrefix ? 1 : -1)};
  }
`;
