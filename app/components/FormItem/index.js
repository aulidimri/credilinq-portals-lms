import React, { PureComponent } from 'react';
import { Form as AntForm, Tooltip as AntTooltip } from 'antd';
import Icon from 'components/Icon'
import { fonts } from 'configs/styleVars';
import { DefaultStyle } from './Styles';

/**
 * FormItem Component for building the Form Component
 *
 * @props  {function} getFieldDecorator
 *
 * @return {object}    An action object with a type of EDIT_EMPLOYEE_DATA
 */
class FormItem extends PureComponent {
  // eslint-disable-line react/prefer-stateless-function

  render() {
    const {
      componentProps,
      label,
      tip,
    } = this.props;

    const { ...baseComponentProps } = componentProps;

    let formItemLabel = label && (
      <div className="formLabel">
        <span>{label}</span>
        &nbsp;
        {tip && ( //TODO: Figure out a better way to style overlay components, they can easily be styled using ant provided less variables
          <AntTooltip overlayStyle={{fontSize: `${fonts.fontSizeXS}`}} title={tip}>
            <Icon componentProps={{type: "question-circle-o"}} />
          </AntTooltip>
        )}
      </div>
    );

    return (
      <DefaultStyle>
        <AntForm.Item {...baseComponentProps} label={formItemLabel}>
          {this.props.children}
        </AntForm.Item>
      </DefaultStyle>
    );
  }
}

export default FormItem;
