import styled from 'styled-components';
import { sizes, fonts } from 'configs/styleVars';

export const DefaultStyle = styled.div`
  .formLabel {
    display: flex;
    align-items: center;
    width: calc(100% - 15px);
    span {
      text-overflow: ellipsis;
      overflow: hidden;
      white-space: nowrap;
      display: block;
     }
  }

  // .ant-form-item {
  //   display: flex;
  //   align-items: start;
  // }

  .ant-form-item-label, .ant-form-item-control {
    line-height: inherit;
  }

  .ant-form-item-label > label {
    text-align: left;
    display: inline-flex;
    justify-content: space-between;
    width: 100%;
    &::after {
      margin-left: auto;
    }
    &::before {
      line-height: inherit;
    }
  }

  .ant-checkbox-wrapper {
    margin-left: ${sizes.marginS};
  }

  .ant-form-explain {
    font-size: ${fonts.fontSizeXS};
  }
`;
