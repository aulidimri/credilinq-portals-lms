import React, { PureComponent } from 'react';
import { Modal as AntModal } from 'antd';
import { DefaultStyle } from './Styles';

class Modal extends PureComponent {

  onOkHandler = () => {
    this.props.onOkHandler && this.props.onOkHandler();
  }

  onCancelHandler = () => {
    this.props.onCancelHandler && this.props.onCancelHandler();
  }

  render() {
    //Need to specify onOkHandler and onCancelHandler if you want to close modal on mask click
    const { componentProps, onOkHandler, onCancelHandler } = this.props;
    const { ...baseComponentProps } = componentProps;
    return (
      <DefaultStyle>
        <AntModal
          onOk={this.onOkHandler}
          onCancel={this.onCancelHandler}
          {...baseComponentProps}
        >
          {this.props.children}
        </AntModal>
      </DefaultStyle>
    )
  }
}

export default Modal;
