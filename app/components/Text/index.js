import React, { PureComponent } from 'react';
import { DefaultStyle } from './Styles';

class Text extends PureComponent {

  render() {
    const { componentProps } = this.props;
    const { label, className, isCurrency, ...baseComponentProps } = componentProps;

    let labelToDisplay = label;
    if (isCurrency) {
      //TODO: Move to a constants file during app internationalization
      const formatter = new Intl.NumberFormat('en-IN', {
        style: 'currency',
        currency: 'INR',
        currencyDisplay: 'symbol',
        minimumFractionDigits: 0,
        maximumFractionDigits: 2,
      })
      labelToDisplay = formatter.format(label)
    }

    return (
      <DefaultStyle>
        <span className={className}>{labelToDisplay}</span>
      </DefaultStyle>
    );
  }
}

export default Text;
