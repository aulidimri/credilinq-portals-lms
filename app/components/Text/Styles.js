import styled from 'styled-components';

export const DefaultStyle = styled.div`
  //Fixed because ant uses this padding for input
  .ant-form-text {
    padding: 0 11px;
  }
  word-break: break-word;
  hyphens: auto;
`;
