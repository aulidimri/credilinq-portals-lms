import React, { PureComponent } from 'react';
import { Dropdown as AntDropdown } from 'antd';
import Menu from 'components/Menu';
import Button from 'components/Button';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { networkAction } from 'containers/App/actions';
import PropTypes from 'prop-types';
import { DefaultStyle } from './Styles';

import dottie from 'dottie';
import uuid from 'uuid';

/**
 * Use this if you want a dropdown menu to perform some operations
 */
class Dropdown extends PureComponent {

  state = {
    data: []
  }

  static propTypes = {
    componentProps: PropTypes.object,
    children: PropTypes.node,
  };

  componentDidMount() {
    const { renderData, elmId, componentProps = {} } = this.props;
    const { keys } = componentProps;
    const meta = dottie.get(renderData, 'params.meta');
    if (!meta)
      return;

    const { api: { method, subUrl, requestPayload, defaultComment, successActions = [], errorActions = [] } } = meta;
    if (!subUrl)
      return;

    let data = {};
    Object.keys(requestPayload).forEach(key => {
      let nestedObj = {};
      Object.keys(requestPayload[key]).length && Object.keys(requestPayload[key]).forEach(i => {
        nestedObj[i] = requestPayload[key][i];
      })
      data[key] = nestedObj;
    })

    //API RestDropdown will always will single level dropdown
    //Discuss whether to move this logic to the container or not since it required onClickHandler as prop
    const formData = {
      api: 'configApi',
      method: 'POST',
      params: { subUrl },
      queryParams: keys,
      data,
      successCallback: (res) => {
        let mapping = meta.api["options-mapping"][elmId];
        const createOptions = option => {
          return {
            label: option,
            onClickParams: [renderData, option],
          }
        }
        const options = res.data.response[mapping].map(createOptions);
        this.setState({ data: options })
      },
      failCallback: (err) => {
      }
    };
    this.props.networkAction(formData);
  }

  render() {
    const { componentProps, onClickHandler, elmId, renderData, dispatch, networkAction } = this.props;
    const { label, data = [], keys, renderChildren, ...baseComponentProps } = componentProps;

    const options = data.length > 0 ? data : this.state.data;
    if (!options.length) {
      return null;
    }

    const optionsMenu = (
      <Menu
        onClick={e => {
          //To stop bubbling, also to prevent disruption of menu functionality
          e.domEvent.stopPropagation();
        }}
        onClickHandler={onClickHandler}
        data={options}
      />
    );

    let children = this.props.children || renderChildren


    return (
      <DefaultStyle>
        <AntDropdown overlay={optionsMenu} {...baseComponentProps}>{children}</AntDropdown>
      </DefaultStyle>
    );
  }
}


const mapStateToProps = createStructuredSelector({
});

function mapDispatchToProps(dispatch) {
  return {
    networkAction: (networkData) => dispatch(networkAction(networkData)),
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);


export default withConnect(Dropdown);
