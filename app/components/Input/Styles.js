import styled from 'styled-components';

export const DefaultStyle = styled.span`
  display: ${(props) => props.type === 'hidden' ? 'none' : 'block'};
  .ant-input-number {
    width: 100%;
  }
`
