import React, { PureComponent } from 'react';
import { Input as AntInput, InputNumber as AntInputNumber } from 'antd';
import { DefaultStyle } from './Styles';
import Meta from 'antd/lib/card/Meta';

class Input extends PureComponent {
  // eslint-disable-line react/prefer-stateless-function

  static defaultProps = {
    componentProps: {},
  }

  inputValue = 0;
  constructor(props) {
    super(props);
    this.inputValue = props.componentProps.value;
  }

  handleChange = (e) => {
    const { componentProps } = this.props;
    const { type, isCurrency } = componentProps;
    const value = type === "number" ? e : e.target.value;
    //console.log("On Change: ", value);
    this.inputValue = value;
    this.props.onChangeHandler && this.props.onChangeHandler(value);
  }

  handleBlur = (e) => {
    //console.log("On Blur: ", this.inputValue);
    this.props.onBlurHandler && this.props.onBlurHandler(this.inputValue);
  }

  handleEnter = (e) => {
    const { componentProps } = this.props;
    const { type } = componentProps;
    const value = type === "number" ? e : e.target.value;
    this.props.onEnterHandler && this.props.onEnterHandler(value);
  }

  handleSearch = (value) => {
    this.props.onSearchHandler && this.props.onSearchHandler(value);
  }

  render() {
    //Component props contain the props that have use only inside the render method of the component
    //Base Component props are the library specific props
    //Ant Props are the props passed down by certain container ant components
    //The remaining props are props that have meaning only in the container page of the component
    const { componentProps, onChangeHandler, onEnterHandler, onBlurHandler, elmId, renderData } = this.props;
    const { type, isCurrency, ...baseComponentProps } = componentProps;
    let InputComponent;
    let inputTypeProps = {};
    switch (type) {
      case "textarea":
        InputComponent = AntInput.TextArea;
        break;
      case "number":
        inputTypeProps = {
          parser: value => {
            //console.log("PARSER: ", value);
            value = value.replace(/[^0-9.]/g, "");
            //console.log("PARSER: ", value);
            value = value.match(/^([0-9]*[.]?)[0-9]{0,2}/g)[0];
            //console.log("PARSER: ", value);
            return value;
          }
        }
        //value.replace(/(\$\s?)|([\Wa-zA-Z\_]*)/g, "") }
        //TODO: Move to a constants file during app internationalization
        if (isCurrency) {
          const formatter = new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            currencyDisplay: 'symbol',
            minimumFractionDigits: 0,
            maximumFractionDigits: 2,
          })
          inputTypeProps = {
            ...inputTypeProps,
            formatter: value => {
              //console.log("FORMATTER: ", value);
              let res = formatter.format(value);
              //console.log("FORMATTER: ", res);
              //TODO: Find a better way?
              //TODO: Update the container state only onblur, cuz if you enter .222 it shows .22 but actually has .222 in container
              if (value[value.length - 1] === ".") {
                res = res + ".";
              }
              return res;
            }
          }
        }
        InputComponent = AntInputNumber;
        break;
      case "search":
        inputTypeProps = { enterButton: true, onSearch: this.handleSearch, placeholder: "Search" };
        InputComponent = AntInput.Search;
        break;
      default:
        InputComponent = AntInput
    }

    return (
      <DefaultStyle type={type}>
        <InputComponent {...inputTypeProps} {...baseComponentProps} onChange={this.handleChange} onPressEnter={this.handleEnter} onBlur={this.handleBlur} />
      </DefaultStyle>
    );
  }
}

export default Input;
