import React, { PureComponent } from 'react';
import { Menu as AntMenu } from 'antd';
import PropTypes from 'prop-types';
import { DefaultStyle } from './Styles';

const { Item: AntMenuItem } = AntMenu;
/**
 * MenuItem Component, can only be inside a Menu Component
 */
class MenuItem extends PureComponent {
  static propTypes = {
    componentProps: PropTypes.object,
    children: PropTypes.node,
  };

  static defaultProps = {
    componentProps: {},
  };

  onClickHandler = (e) => {
    const { onClickHandler, onClickParams, onClick: antOnClick } = this.props;
    if (onClickHandler) {
      if (onClickParams && onClickParams.length)
        onClickHandler(e, ...onClickParams)
      else
        onClickHandler(e);
    }
    if( antOnClick ) {
      antOnClick(e);
    }
  };

  render() {

    // antProps contain the unidentified props that are passed by the Ant Design Framework by parent components like Dropdown, Select, etc.
    const { componentProps, onClickHandler, onClickParams, onClick: antOnClick, ...antProps } = this.props;
    const { ...baseComponentProps } = componentProps;
    return (
      <DefaultStyle>
        <AntMenuItem  {...antProps} {...baseComponentProps} onClick={this.onClickHandler}>
          {this.props.children}
        </AntMenuItem>
      </DefaultStyle>
    );
  }
}

export default MenuItem;
