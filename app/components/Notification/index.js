import { Button, notification } from 'antd';

export const openNotification = (props = {}) => {
  const { message, description, type = 'open', duration = 0, onClose } = props;
  notification[type]({
    message,
    description,
    duration,
    onClick: () => {
      console.log('Notification Clicked!');
    },
    onClose: () => {
      onClose && onClose();
    }
  });
};
