import styled from 'styled-components';
import { colors } from 'configs/styleVars';

export const DefaultStyle = styled.div`
  background-color: ${colors.primaryBodyBG};
  display: flex;
  height: 100%;
  justify-content: center;
  align-items: center;
`;
