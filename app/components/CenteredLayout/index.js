import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { DefaultStyle } from './Styles';

/**
 * Container that centers whatever node that is inside it.
 */
class CenteredLayout extends PureComponent {
  static propTypes = {
    children: PropTypes.node,
  };

  render() {
    return <DefaultStyle>{this.props.children};</DefaultStyle>;
  }
}

CenteredLayout.propTypes = {
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
};

export default CenteredLayout;
