import React, { PureComponent } from 'react';
import { Button as AntButton } from 'antd';
import PropTypes from 'prop-types';
import { DefaultStyle } from './Styles';

/**
 * Button that accepts a callBack function ( and any params passed to it ) that is called when the button is clicked.
 */
class Button extends PureComponent {
  static propTypes = {
    /** function to be called when the button is clicked */
    onClickHandler: PropTypes.func,
    /** arguments to be passed to the callBack function */
    onClickParams: PropTypes.array,
    componentProps: PropTypes.object,
    children: PropTypes.node,
  };

  static defaultProps = {
    componentProps: {},
  }

  onClickHandler = (e) => {
    const { onClickHandler, onClickParams, onClick: antOnClick } = this.props;
    if (onClickHandler) {
      if (onClickParams && onClickParams.length)
        onClickHandler(e, ...onClickParams)
      else
        onClickHandler(e);
    }
    if (antOnClick)
      antOnClick(e);
  };

  render() {                                                            //added onChangeHandler to be consistent with other formtypes
                                                                        //TODO figure out a better way to do things in ApplicationDetailsPage
    const { componentProps, onClickHandler, onBlurHandler, onClickParams, onClick: antOnClick, elmId, renderData, onChangeHandler, ...antProps } = this.props;
    const { label, ...baseComponentProps } = componentProps;
    return (
      <DefaultStyle>
        <AntButton  {...antProps} {...baseComponentProps} onClick={this.onClickHandler}>
          {label || this.props.children}
        </AntButton>
      </DefaultStyle>
    );
  }
}

export default Button;
