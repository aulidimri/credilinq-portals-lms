// This mapping of components based on their form type
// This does not include the prebuilt/static components at the moment
// Purely based on form type - unit components

import Input from 'components/Input';
import Select from 'components/Select';
import RestSelect from 'components/RestSelect';
import Dropdown from 'components/Dropdown';
import Icon from 'components/Icon';
import Checkbox from 'components/Checkbox';
import Card from 'components/Card';
import Heading from 'components/Heading';
import Button from 'components/Button';
import Text from 'components/Text';
import DatePicker from 'components/DatePicker';
import EditableTable from 'components/EditableTable';
import Upload from 'components/Upload';
import Table from 'components/Table';
import TabCard from 'components/TabCard';
import ExpressionComponent from 'components/ExpressionComponent';
import ModalForm from '../components/ModalForm';

export default {
  text: Input,
  number: Input,
  long: Input,
  card: Card,
  heading: Heading,
  button: Button,
  string: Text,
  datepicker: DatePicker,
  'multiselect-dropdown': Select,
  'readOnlyTable': Table,
  'tab-actions': TabCard,
  upload: Upload,
  hidden: Input,
  'rest-dropdown': RestSelect,
  'droplist': Dropdown,
  'rest-droplist': Dropdown,
  // readOnly: Input,
  // datepicker: Datepicker,
  dropdown: Select,
  // 'multiselect-dropdown': MultiDropDown,
  // 'rest-dropdown': RestDropdown,
  // autocomplete: AutoComplete,
  // upload: FileUploadComponent,
  editableTable: EditableTable,
  // multiInstance: MultiInstanceComponent,
  //customAction: Button,
  checkbox: Checkbox,
  icon: Icon,
  expression: ExpressionComponent,
  modal: ModalForm,
};
