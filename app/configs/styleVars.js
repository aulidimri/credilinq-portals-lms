// Color Variable:
export const globalColors = {
  white: '#FFF',
  black: '#000',
  disabledBodyColor: `rgba(0, 0, 0, 0.25)`,
  primaryBodyColor: '#4a4a4a',
  secondaryBodyColor: '#b3b3b3',
  primaryColor: '#5174C2',
  errorColor: 'red',
  lightGrey: '#ddd',
};

export const colors = {
  /** Component Schema */
  primaryComponentColor: globalColors.white,
  secondaryComponentColor: '',
  primaryHoverComponentColor: '',
  secondaryHoverComponentColor: '',
  primaryComponentBG: globalColors.primaryColor,
  secondaryComponentBG: '',
  primaryHoverComponentBG: '',
  secondaryHoverComponentBG: '',

  /** Table, Lists, Select Options, etc. Schema */
  primaryRowBG: '',
  secondaryRowBG: '',
  hoverRowColorBG: '',
  primaryRowColor: '',
  secondaryRowColor: '',
  hoverRowColorColor: '',

  /** Body Schema */
  primaryBodyBG: globalColors.white,
  secondaryBodyBG: '',
  primaryBodyColor: globalColors.primaryBodyColor,
  secondaryBodyColor: globalColors.secondaryBodyColor,
  disabledBodyColor: globalColors.disabledBodyColor,
  errorBodyColor: globalColors.errorColor,

  /** Border Schema */
  primaryBorderColor: '',
  secondaryBorderColor: '',
  primaryBorderColor: globalColors.secondaryBodyColor,
  secondaryBorderColor: globalColors.lightGrey,
  primaryHoverBorderColor: globalColors.primaryColor,
  primaryActiveBorderColor: globalColors.primaryColor,

};

// TODO: move fonts to separate files for web and native
export const fonts = {
  primaryFontFamily: 'Open Sans',
  secondaryFontFamily: 'Open Sans',
  fontSize: '10px',

  fontSizeXS: '1.2rem',
  fontSizeS: '1.5rem',
  fontSizeM: '2rem',
  fontSizeL: '2.5rem',
  fontSizeXL: '3rem',

};

// TODO: combine font and lineHeight
export const lineHeight = {
  h1: '3rem',
  h2: '2.5rem',
  h3: '2rem',
  h4: '1.8rem',
  body: '1.5rem',
  description: '1.2rem',
  label: '1rem',
};

export const sizes = {
  paddingXL: '7rem',
  paddingL: '5rem',
  paddingM: '3rem',
  paddingS: '1rem',
  paddingXS: '0.5rem',

  marginXL: '5rem',
  marginL: '3rem',
  marginM: '2rem',
  marginS: '1rem',
  marginXS: '0.2rem',
};

const vars = {
  colors,
  fonts,
  sizes,
};

export default vars;
