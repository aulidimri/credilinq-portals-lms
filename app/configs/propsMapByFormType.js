/*
This service contains the props passed to card's components
based upon their form_type. Will need to change it if we change the UI library.
*/

/*
  This is API Specific
*/


//IGNORE THIS, We can do this in the component files itself
//Component props are the props native to the UI library and custom props are the props defined by us
//TODO: Map each property of component and custom props individually to componentProps and customProps of the component so that we don't need to use spread operator in this file for them
// so that if later on, we change the UI Library, we just need to change the old library components in the components and change the mapping of props here only
// and if it happens that if later on, the feature enabled by the component prop of the old UI library, is no longer enabled in the new UI library, there might be two cases on why the
// feature might not be getting enabled
// CASE 1: the name of the component prop is different to enable the same feature, in this case, we just map the component prop here to that name
// CASE 2: the feature does not exist in the new UI library, in this case, we develop the feature on our own and move that prop from component prop to custom prop

import React from 'react';
import Button from 'components/Button';
import dottie from 'dottie';

const getInputProps = (data, id, value, readMode) => {
  const { meta } = data.params
  const formType = meta.multiline ? "textarea" : meta.form_type;
  const inputTypeMap = {
    text: "text",
    textarea: "textarea",
    number: "number",
    long: "number",
    hidden: "hidden",
  };

  const inputTypePropsMap = {
    text: {},
    textarea: {},
    number: {
      isCurrency: meta.isCurrency,
    },
    long: {
      formatter: meta.isCurrency,
    },
    hidden: {},
  }

  return {
    id: 'ku-input-' + id,
    key: 'ku-input-' + id,
    value: value,
    type: inputTypeMap[formType],
    ...inputTypePropsMap[formType],
    disabled: !!meta.disabled
  }
}

const getCheckboxProps = (data, id, value, readMode) => {
  const { meta: { disabled } } = data.params
  value = !!value ? true : false; //if value is empty string
  return {
    id: 'ku-checkbox-' + id,
    checked: value,
    disabled: readMode || disabled,
  }
}

const getRadioProps = (data, id, value, readMode) => {
  const { meta: { disabled } } = data.params
  value = !!value ? true : false; //if value is empty string
  return {
    id: 'ku-checkbox-' + id,
    checked: value,
    disabled: readMode || disabled,
  }
}

const getCardProps = (data, id, value, readMode) => {

  return {
    id: 'ku-card-' + id,
    title: data.name,
  }
}

const getSelectProps = (data, id, value, readMode) => {
  const { meta: { disabled, form_type } } = data.params

  let mode;
  if (form_type === 'multiselect-dropdown') {
    mode = "multiple";
    if (!value)
      value = [];
  }

  return {
    id: 'ku-dropdown-' + id,
    value: value,
    mode,
    disabled: readMode || disabled
  }
}

const getDropdownProps = (data, id, value, readMode) => {
  const meta = dottie.get(data, 'params.meta');
  return {
    id: 'ku-dropdown-' + id,
    label: data.name,
    renderChildren: <Button componentProps={{ type: meta.type === "action" ? "primary" : undefined, label: data.name }} />,
    placement: 'bottomRight',
    trigger: ['click'],
    disabled: readMode,
  }
}

const getIconProps = (data, id, value, readMode) => {

  //TODO make api to icon map for the ui library

  return {
    id: 'ku-icon-' + id,
    type: value,  //TODO fetch type from data
    label: "",
  }
}

const getHeadingProps = (data, id, value, readMode) => {

  const meta = data.params.meta;
  let label = data.name;
  let note = false;
  if (meta.applicationId) {
    label = undefined;
    value = "#" + value;
  }
  if (meta.note) {
    note = true;
  }

  return {
    id: 'ku-heading-' + id,
    type: data.params.meta.headingType,
    value: value,  //TODO fetch type from data
    label: label,
    note: note,
  }
}

const getButtonProps = (data, id, value, readMode) => {
  const { meta: { disabled } } = data.params

  return {
    id: 'ku-button-' + id,
    type: "primary",
    label: data.name,
    disabled: readMode || disabled,
  }
}



const getTextProps = (data, id, value, readMode) => {

  const meta = data.params.meta;
  if (value && (meta.form_type === "dropdown" || meta.form_type === "multiselect-dropdown")) {
    const mapNameById = {};
    for (let i = 0; i < data.enumValues.length; ++i) {
      const { id, name } = data.enumValues[i];
      mapNameById[id] = name;
    }

    if (Array.isArray(value)) {
      value = value.map(key => mapNameById[key]);
      value = value.join(" | ");
    }
    else {
      value = mapNameById[value];
    }
  }

  return {
    id: 'ku-text-' + id,
    label: value,
    isCurrency: meta.isCurrency,
    readMode,
  }
}

const getDatePickerProps = (data, id, value, readMode) => {
  const { meta: { disabled } } = data.params

  return {
    id: 'ku-date-picker-' + id,
    value: value,
    disabled: readMode || disabled
  }
}

const getUploadProps = (data, id, value, readMode) => {
  const { meta: { disabled } } = data.params

  return {
    id: 'ku-upload-' + id,
    fileList: value,
    meta: data.params.meta,
    disabled: readMode || disabled,
    showUploadList: { showRemoveIcon: !readMode }
  }
}

const getReadOnlyTableProps = (data, id, value, readMode) => {
  return {
    id: 'ku-read-only-table-' + id,
    type: "primary",
    label: data.name,
    disabled: readMode,
    rows: value || [],
  }
}

const getTabCardProps = (data, id, value, readMode) => {

  return {
    id: 'ku-card-' + id,
    title: data.name,
    disabled: false
  }
}

const getEditableTableProps = (data, id, value, readMode) => {
  return {
    readMode,
  }
}

const getModalProps = (data, id, value, readMode) => {

  return {
    id: 'ku-modal-' + id,
    title: data.name,
    width: 700,
    maskClosable: true,
    closable: true,
    footer: null,
  }
}
// const getHiddenFieldProps = (data, id, value) => {
//   const meta = data.params.meta,
//     formType = meta.form_type;
//   const propObj = {
//     id: `ku-hidden-${id}`,
//     value: value,
//     disabled: meta.disabled,
//   };
//   return propObj;
// };


//TODO: Shift this to API specific file
export default {
  text: getInputProps,
  long: getInputProps,
  card: getCardProps,
  heading: getHeadingProps,
  button: getButtonProps,
  string: getTextProps,
  datepicker: getDatePickerProps,
  editableTable: getEditableTableProps,
  upload: getUploadProps,
  // rest: getInputFieldProps,
  number: getInputProps,
  // hidden: getHiddenFieldProps,
  checkbox: getCheckboxProps,
  radio: getRadioProps,
  dropdown: getSelectProps,
  'multiselect-dropdown': getSelectProps,
  'readOnlyTable': getReadOnlyTableProps,
  'tab-actions': getTabCardProps,
  hidden: getInputProps,
  'rest-dropdown': getSelectProps,
  'droplist': getDropdownProps,
  'rest-droplist': getDropdownProps,
  modal: getModalProps,
  // 'multiselect-dropdown': getDropdownProps,
  // 'rest-dropdown': getDropdownProps,
  // slider: getSliderProps,
  // upload: getUploadProps,
  // download: getDownloadProps,
  // autocomplete: getAutoCompleteProps,
  // elasticAutocomplete: getAutoCompleteProps,
  // customAction: getButtonProps,
  // heading: getHeadingProps,
  // accordion: getAccordionProps,
  // radioAccordion: getRadioAccordionProps,
  icon: getIconProps,
}















/*

import { isFieldUpdateTypeButton } from 'utils/constants';
//import RequiredTextLabel from '../RequiredTextLabel';
import React from 'react';
//import * as common from 'utils/commonUtils';

// Render behaviour of components
// This return an object in compliance with Material UI setting of input text
export function getInputBasicRenderObj (renderData, editModeState) {
  let obj = {};
  //obj.floatingLabelText = renderData.floatingText && renderData.name;
  obj.floatingLabelText = renderData.floatingText && renderData.label;
  obj.floatingLabelFixed = renderData.floatingLabelFixed;

  const meta = renderData.params.meta;
  if (meta.required || meta.required === 'true') {
    //obj.floatingLabelText = <RequiredTextLabel heading={obj.floatingLabelText} />;
  }

  if (isFieldUpdateTypeButton && !editModeState) {
    obj.underlineShow = false;
  }

  return obj;
}

// Render behaviour of components
// This return an object in compliance with Material UI setting of input text
export function getDropdownBasicRenderObj (renderData) {
  let obj = {};
  obj.hintText = renderData.hintText && renderData.name;
  obj.floatingLabelFixed = renderData.floatingLabelFixed;
  obj.floatingLabelText = (renderData.floatingText && renderData.name) || ' ';

  const meta = renderData.params.meta;
  if (meta.required || meta.required === 'true') {
    obj.floatingLabelText = '* ' + obj.floatingLabelText;
  }

  return obj;
}

const getInputFieldProps = (data, elmId, defaultValue, editModeState) => {
  // basicRenderObj contains the coniguration for hintText, floatingLabelText etc etc
  // This is as per the app style setting for input texts
  const isReadMode = isComponentReadMode(data.editable, editModeState);
  if (isReadMode) {
    return getReadOnlyParams(data, elmId, defaultValue, editModeState);
  }
  const basicRenderObj = getInputBasicRenderObj(data, editModeState);
  const meta = data.params.meta,
        formType = meta.form_type;
  let propObj = {
    ...basicRenderObj,
    id: 'ku-text-' + elmId,
    value: defaultValue,
    disabled: false,
    type: !!meta.isCurrency ? 'text': formType,
    'data-iscurrency': !!meta.isCurrency,
    multiLine: !!meta.multiLine,
    autoCapitalize: !!meta.autoCapitalize,
  };
  if(formType === 'long') {
    propObj.type = 'number';
  }

  return propObj;
}

const getDatePickerProps = (data, elmId, defaultValue, editModeState) => {
  // basicRenderObj contains the coniguration for hintText, floatingLabelText etc etc
  // This is as per the app style setting for input texts
  const isReadMode = isComponentReadMode(data.editable, editModeState);
  const basicRenderObj = getInputBasicRenderObj(data, editModeState);
  const meta = data.params.meta,
        formType = meta.form_type;
  let propObj = {
    ...basicRenderObj,
    container: 'inline',
    id: 'ku-text-' + elmId,
    defaultDate: new Date(),
    disabled: false,
    type: formType
  }
  return propObj;
}

const isComponentReadMode = (editable, editMode) => {
  let isReadMode = !editable;
  if (isFieldUpdateTypeButton && editable) {
    isReadMode = !editMode;
  }
  return isReadMode;
}

const getHiddenFieldProps = (data, elmId, defaultValue) => {
  // This is as per the app style setting for input texts
  const meta = data.params.meta,
        formType = meta.form_type;
  let propObj = {
    id: 'ku-hidden-' + elmId,
    value: defaultValue,
    disabled: meta.disabled,
  }
  return propObj;
}

const getCheckboxProps = (data, elmId, defaultValue, editModeState) => {
  // TODO: drive the default settings from a config
  const isReadMode = isComponentReadMode(data.editable, editModeState);
  const meta = data.params.meta,
        formType = meta.form_type;
  const basicRenderObj = getInputBasicRenderObj(data, editModeState);
  let propObj = {
    id: 'ku-checkbox-' + elmId,
    checked: !!defaultValue,
    disabled: isReadMode,
    style: { position: 'absolute', top: '38px'},
    label: basicRenderObj.floatingLabelText,
  };
  return propObj;
}

const getRadioGroupProps = (data, elmId, defaultValue) => {
  // This is as per the app style setting for input texts
  // TODO: drive the default settings from a config
  const meta = data.params.meta,
        formType = meta.form_type;
  let propObj = {
    radioGroupProp: {
      id: 'ku-radio-' + elmId,
      valueSelected: defaultValue,
    },
    radioButtonProp: {
      disabled: meta.disabled
    }
  }
  return propObj;
}


const getDropdownProps = (data, elmId, defaultValue, editModeState) => {

  // TODO: drive the default settings from a config

  const isReadMode = isComponentReadMode(data.editable, editModeState);
  if (isReadMode) {
    return getReadOnlyParams(data, elmId, defaultValue, editModeState);
  }
  const meta = data.params.meta,
        formType = meta.form_type;
  const basicRenderObj = getInputBasicRenderObj(data, editModeState);
  let propObj = {
    ...basicRenderObj,
    id: 'ku-dropdown-' + elmId,
    value: defaultValue,
    disabled: false,
  }
  return propObj;
}

const getSliderProps = (data, elmId, defaultValue) => {
  // TODO: drive the default settings from a config
  const meta = data.params.meta,
        formType = meta.form_type;
  let isIrregular = meta.isStepIrregluar, valArr, step, sliderStepIndex;
  if (isIrregular) {
    valArr = meta.valArr;
    step = 1;
    defaultValue = !!defaultValue ? defaultValue : valArr[0];
    sliderStepIndex = valArr.indexOf(defaultValue);
  } else {
    defaultValue = !!defaultValue ? defaultValue : meta.min * meta.step;
  }

  let propObj = {
    id: 'ku-slider-' + elmId,
    value: defaultValue,
    disabled: meta.disabled,
    max: !isIrregular ? meta.max * meta.step : valArr.length -1,
    min: !isIrregular ? meta.min * meta.step : 0,
    step: !isIrregular ? meta.step : 1,
    isCurrency: meta.isCurrency,
    valArr: valArr,
    isStepIrregluar: isIrregular,
    sliderStepIndex: sliderStepIndex,
  }
  return propObj;
}

const getUploadProps = (data, elmId, defaultValue, editModeState) => {
  // TODO: drive the default settings from a config
  const meta = data.params.meta,
  formType = meta.form_type;
  const isReadMode = isComponentReadMode(data.editable, editModeState);
  const basicRenderObj = getInputBasicRenderObj(data, editModeState);
  let propObj = {
    ...basicRenderObj,
    multiple: true,
    disabled: isReadMode,
    value: defaultValue,
    accept: meta.filetypes || '', // TODO: move this to a constant config
    docSource: meta.source ? meta.source : 'fileManager',
    docType: meta.docType
  }
  return propObj;
}

const getAutoCompleteProps = (data, elmId, defaultValue, editModeState) => {
  // basicRenderObj contains the coniguration for hintText, floatingLabelText etc etc
  // This is as per the app style setting for input texts
  const isReadMode = isComponentReadMode(data.editable, editModeState);
  if (isReadMode) {
    return getReadOnlyParams(data, elmId, defaultValue, editModeState);
  }
  const basicRenderObj = getInputBasicRenderObj(data, editModeState);
  const meta = data.params.meta,
        formType = meta.form_type;
  let propObj = {
    ...basicRenderObj,
    id: 'ku-autocomplete-' + elmId,
    value: defaultValue,
    disabled: false,
    type: formType
  }
  return propObj;
}

const getDownloadProps = (data, elmId, defaultValue) => {
  const meta = data.params.meta;

  let propObj = {
    url: defaultValue,
    label: data.name,
    docType: meta.docType
  }
  return propObj;
}

const getHeadingProps = (data, elmId, defaultValue) => {
  const meta = data.params.meta;
  const propObj = {
    label: data.name,
    type: meta.headingType,
    value: defaultValue,
  };

  return propObj;
}

const getAccordionProps = (data, elmId, defaultValue) => {
  const meta = data.params.meta;
  const propObj = {
    label: data.name,
    imageUrl: meta.imageUrl,
  };

  return propObj;
}

const getRadioAccordionProps = (data, elmId, defaultValue) => {
  const meta = data.params.meta;
  const propObj = {
    label: data.name,
    valueHolder: meta.valueHolder,
    imageUrl: meta.imageUrl,
  };

  return propObj;
}

const getReadOnlyParams = (data, elmId, defaultValue, editModeState) => {
  // const meta = data.params.meta;
  // const propObj = {
  //   label: data.name,
  //   valueHolder: meta.valueHolder,
  //   imageUrl: meta.imageUrl,
  // };
  //return data;
  const basicRenderObj = getInputBasicRenderObj(data, editModeState);
  const meta = data.params.meta,
        formType = meta.form_type;
  let extraProps = {};
  if(formType === 'text'){
    extraProps = {
      multiLine: !!meta.multiLine,
      autoCapitalize: !!meta.autoCapitalize
    }
  };
  let propObj = {
    ...basicRenderObj,
    ...extraProps,
    id: 'ku-text-' + elmId,
    value: defaultValue,
    disabled: true,
    type: !!meta.isCurrency ? 'text' : formType,
    'data-iscurrency': !!meta.isCurrency
  };
  return propObj;
}


const getButtonProps = (data, elmId, defaultValue, editModeState) => {
  const meta = data.params.meta,
    formType = meta.form_type;

  const isReadMode = isComponentReadMode(data.editable, editModeState);
    const buttonOptions = [
      {
        label: data.label,
        buttonType: isReadMode ? 'disabled' : (data.type || ''),
        buttonData: data,
        key: data.key,
      }
    ];
  let propObj = {
    id: 'ku-button-' + elmId,
    buttons: buttonOptions,
    type: formType
  }
  return propObj;
}

const getIconProps = (data, elmId, defaultValue, editModeState) => {
  const meta = data.params.meta;
  const isReadMode = isComponentReadMode(data.editable, editModeState);
  const basicRenderObj = getInputBasicRenderObj(data, editModeState);
  const propObj = {
    label: basicRenderObj.floatingLabelText,
    type: meta.type,
    disabled: isReadMode
  };

  return propObj;
}

const getExpressionProps = (data, elmId, defaultValue) => {
  const meta = data.params.meta;
  const propObj = {
    //label: data.name,
    //valueHolder: meta.valueHolder,
    //imageUrl: meta.imageUrl,
  };

  return propObj;
}




*/


