import { createGlobalStyle } from 'styled-components';
import { fonts } from 'configs/styleVars';

const GlobalStyle = createGlobalStyle`
  html,
  body {
    height: 100%;
    width: 100%;
    font-size: ${fonts.fontSize};
  }

  body {
    font-family: ${fonts.primaryFontFamily}, ${fonts.secondaryFontFamily};
  }

  body.fontLoaded {
    font-family: ${fonts.primaryFontFamily}, ${fonts.secondaryFontFamily};
  }

  #app {
    //background-color: #fafafa;
    height: 100%;
  }

  p,
  label {
    font-family: ${fonts.primaryFontFamily}, ${fonts.secondaryFontFamily};
    line-height: 1.5em;
  }

  .display-none {
    display: none !important;
  }

  .display-block {
    display: block !important;
  }

  .display-flex {
    display: flex !important;
  }
`;

export default GlobalStyle;
