## README

**This is the repository for Portals**

The project scaffolding structure is taken from react-boilerplate

*We recommend that you go through the installation steps to successfully run the app*

---

## Installation Steps

1. Clone the repository.
2. cd into the folder.
3. Make sure you have Node ">=8.10.0" and npm ">=5".
4. Run npm install to install all dependencies.
5. Run npm start to run the application dev server on port 3000 or npm start -- --port [PORT_NO] to run in your choice of port


---